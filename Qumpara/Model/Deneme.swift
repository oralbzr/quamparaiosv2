//
//  Deneme.swift
//  Qumpara
//
//  Created by Oral Bozkır on 12/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import Foundation

class Deneme: NSObject {

    enum SerializationError: Error {
        case missing(String)
        case invalid(String, Any)
    }
    
    enum Meal: String {
        case breakfast, lunch, dinner
    }
    
    let name: String
    let meals: Set<Meal>
    
    
    
        init(json: [String: Any]) throws {
            // Extract name
            guard let name = json["name"] as? String else {
                throw SerializationError.missing("name")
            }
            
            // Extract and validate meals
            guard let mealsJSON = json["meals"] as? [String] else {
                throw SerializationError.missing("meals")
            }
            
            var meals: Set<Meal> = []
            for string in mealsJSON {
                guard let meal = Meal(rawValue: string) else {
                    throw SerializationError.invalid("meals", string)
                }
                
                meals.insert(meal)
            }
            
            // Initialize properties
            self.name = name
            self.meals = meals
        }
    
}

