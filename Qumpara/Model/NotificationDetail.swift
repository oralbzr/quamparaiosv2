//
//  NotificationDetail.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/9/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import Foundation

class NotificationDetail:NSObject,NSCoding {
    
    var NotificationDate: String = ""
    var Notification: String = ""
    var NType:Int = 0
    var SubType: Int? = 0
    var ReceiptId: Int? = 0
    var ReceiptImage: String? = ""
    var CouponId: Int? = 0
    var DescriptionDetailHTML: String? = ""
    var ContactAddedDate: String? = ""
    var ContactFormDescription: String? = ""
    var CampaignId: Int? = 0
    
    override init() {}
    
    required init(coder aDecoder:NSCoder) {
        if let NotificationDate = aDecoder.decodeObject(forKey: "NotificationDate") as? String {
            self.NotificationDate = NotificationDate
        }
        if let Notification = aDecoder.decodeObject(forKey: "Notification") as? String {
            self.Notification = Notification
        }
        if let NType = aDecoder.decodeObject(forKey: "NType") as? Int {
            self.NType = NType
        }
        if let SubType = aDecoder.decodeObject(forKey: "SubType") as? Int {
            self.SubType = SubType
        }
        if let ReceiptId = aDecoder.decodeObject(forKey: "ReceiptId") as? Int {
            self.ReceiptId = ReceiptId
        }
        if let ReceiptImage = aDecoder.decodeObject(forKey: "ReceiptImage") as? String {
            self.ReceiptImage = ReceiptImage
        }
        if let CouponId = aDecoder.decodeObject(forKey: "CouponId") as? Int {
            self.CouponId = CouponId
        }
        if let DescriptionDetailHTML = aDecoder.decodeObject(forKey: "DescriptionDetailHTML") as? String {
            self.DescriptionDetailHTML = DescriptionDetailHTML
        }
        if let ContactAddedDate = aDecoder.decodeObject(forKey: "ContactAddedDate") as? String {
            self.ContactAddedDate = ContactAddedDate
        }
        if let ContactFormDescription = aDecoder.decodeObject(forKey: "ContactFormDescription") as? String {
            self.ContactFormDescription = ContactFormDescription
        }
        if let CampaignId = aDecoder.decodeObject(forKey: "CampaignId") as? Int {
            self.CampaignId = CampaignId
        }
    }
    
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(NotificationDate, forKey: "NotificationDate")
        aCoder.encode(Notification,forKey: "Notification")
        aCoder.encode(NotificationDate, forKey: "NType")
        aCoder.encode(Notification,forKey: "SubType")
        aCoder.encode(NotificationDate, forKey: "ReceiptId")
        aCoder.encode(Notification,forKey: "ReceiptImage")
        aCoder.encode(NotificationDate, forKey: "CouponId")
        aCoder.encode(Notification,forKey: "DescriptionDetailHTML")
        aCoder.encode(NotificationDate, forKey: "ContactAddedDate")
        aCoder.encode(Notification,forKey: "ContactFormDescription")
        aCoder.encode(NotificationDate, forKey: "CampaignId")
    }
}

