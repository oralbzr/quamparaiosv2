//
//  Campaigns.swift
//  Qumpara
//
//  Created by Banu on 15.08.2016.
//  Copyright © 2016 Nobium. All rights reserved.
//

import Foundation

class Campaigns:NSObject, NSCoding {

    var CampaignId:String = ""
    var Description:String = ""
    var ResimUrl:String = ""
    var Subtitle:String = ""
    var Title:String = ""
    var CampaignShareLink: String = ""
    var IsAdvertisement: Bool = false
    var IsOpenInApp: Bool = false
    var RemainingDay: String = ""
    var AdvertisementURL: String = ""
    
    override init() {}
    
    required init(coder aDecoder: NSCoder) {
        
        if let CampaignId = aDecoder.decodeObject(forKey: "CampaignId") as? String {
            self.CampaignId = CampaignId
        }
       
        if let Description = aDecoder.decodeObject(forKey: "Description") as? String {
            self.Description = Description
        }
        
        if let ResimUrl = aDecoder.decodeObject(forKey: "ResimUrl") as? String {
            self.ResimUrl = ResimUrl
        }
       
        if let Subtitle = aDecoder.decodeObject(forKey: "Subtitle") as? String {
            self.Subtitle = Subtitle
        }
        if let Title = aDecoder.decodeObject(forKey: "Title") as? String {
            self.Title = Title
        }
        
        if let CampaignShareLink = aDecoder.decodeObject(forKey: "CampaignShareLink") as? String {
            self.CampaignShareLink = CampaignShareLink
        }
        if let IsAdvertisement = aDecoder.decodeObject(forKey: "IsAdvertisement") as? Bool {
            self.IsAdvertisement = IsAdvertisement
        }
        if let IsOpenInApp = aDecoder.decodeObject(forKey: "IsOpenInApp") as? Bool {
            self.IsOpenInApp = IsOpenInApp
        }
        if let RemainingDay = aDecoder.decodeObject(forKey: "RemainingDay") as? String {
            self.RemainingDay = RemainingDay
        }
        if let AdvertisementURL = aDecoder.decodeObject(forKey: "AdvertisementURL") as? String {
            self.AdvertisementURL = AdvertisementURL
        }
        
    }
    
    
    public func encode(with aCoder: NSCoder) {

        aCoder.encode(CampaignId,forKey: "CampaignId")
        aCoder.encode(Description,forKey: "Description")
        aCoder.encode(ResimUrl,forKey: "ResimUrl")
        aCoder.encode(Subtitle,forKey: "Subtitle")
        aCoder.encode(Title,forKey: "Title")
        aCoder.encode(Title,forKey: "CampaignShareLink")
        aCoder.encode(IsAdvertisement,forKey: "IsAdvertisement")
        aCoder.encode(Title,forKey: "IsOpenInApp")
        aCoder.encode(RemainingDay,forKey: "RemainingDay")
        aCoder.encode(AdvertisementURL,forKey: "AdvertisementURL")
       
        
    }

}



