//
//  Survey.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/13/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import Foundation

struct Question {
    var QuestionID: Int = 0
    var Caption: String = ""
    var Options: [String] = []
    var Answer: Int = 0
}

class Survey:NSObject,NSCoding {
    
    var Name: String = ""
    var Caption: String = ""
    var Questions: [Question] = []
    
    override init() {}
    
    required init(coder aDecoder:NSCoder) {
        if let Name = aDecoder.decodeObject(forKey: "Name") as? String {
            self.Name = Name
        }
        if let Caption = aDecoder.decodeObject(forKey: "Caption") as? String {
            self.Caption = Caption
        }
        if let Questions = aDecoder.decodeObject(forKey: "Questions") as? [Question] {
            self.Questions = Questions
        }
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(Name, forKey: "Name")
        aCoder.encode(Caption, forKey: "Caption")
        aCoder.encode(Questions, forKey: "Questions")
    }
}
