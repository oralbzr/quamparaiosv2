//
//  Profile.swift
//  Qumpara
//
//  Created by Banu on 21.08.2016.
//  Copyright © 2016 Nobium. All rights reserved.
//

import Foundation

class Profile:NSObject,NSCoding{
    
    var MemberId: String = ""
    var NameSurname: String = ""
    var Email: String = ""
    var Gender: String = ""
    var BirthDate: String = ""
    var City: String = ""
    var County: String = ""
    var FamilySize: String = ""
    var Income:String = ""
    var Graduation: String = ""
    var Pet: Int = -1
    var Child: String = ""
    var Married: String = ""
    var IsNotification: String = ""
    var PhoneNumber: String = ""
    var MemberImageUrl: String = ""
    
    // eklediklerim
    var Adress: String = ""
    var Plaka: String = ""
    
    
    override init() {}
    
    required init(coder aDecoder:NSCoder){
        if let MemberId = aDecoder.decodeObject(forKey: "MemberId") as? String {
            self.MemberId = MemberId
        }
        if let NameSurname = aDecoder.decodeObject(forKey: "NameSurname") as? String {
            self.NameSurname = NameSurname
        }
        if let Email = aDecoder.decodeObject(forKey: "Email") as? String {
            self.Email = Email
        }
        if let Gender = aDecoder.decodeObject(forKey: "Gender") as? String {
            self.Gender = Gender
        }
        if let BirthDate = aDecoder.decodeObject(forKey: "BirthDate") as? String {
            self.BirthDate = BirthDate
        }
        if let City = aDecoder.decodeObject(forKey: "City") as? String {
            self.City = City
        }
        if let County = aDecoder.decodeObject(forKey: "County") as? String {
            self.County = County
        }
        if let FamilySize = aDecoder.decodeObject(forKey: "FamilySize") as? String {
            self.FamilySize = FamilySize
        }
        if let Income = aDecoder.decodeObject(forKey: "Income") as? String {
            self.Income = Income
        }
        if let Graduation = aDecoder.decodeObject(forKey: "Graduation") as? String {
            self.Graduation = Graduation
        }
        self.Pet = aDecoder.decodeInteger(forKey: "Pet") 
         
        if let Child = aDecoder.decodeObject(forKey: "Child") as? String {
            self.Child = Child
        }
        if let Married = aDecoder.decodeObject(forKey: "Married") as? String {
            self.Married = Married
        }
        if let IsNotification = aDecoder.decodeObject(forKey: "IsNotification") as? String {
            self.IsNotification = IsNotification
        }
        if let PhoneNumber = aDecoder.decodeObject(forKey: "PhoneNumber") as? String {
            self.PhoneNumber = PhoneNumber
        }
        if let MemberImageUrl = aDecoder.decodeObject(forKey: "MemberImageUrl") as? String {
            self.MemberImageUrl = MemberImageUrl
        }
        
        // eklediklerim
        if let _Adress = aDecoder.decodeObject(forKey: "Address") as? String {
            self.Adress = _Adress
        }
        if let _Plaka = aDecoder.decodeObject(forKey: "CarPlate") as? String {
            self.Plaka = _Plaka
        }
        
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(MemberId,forKey: "MemberId")
        aCoder.encode(NameSurname,forKey: "NameSurname")
        aCoder.encode(Email,forKey: "Email")
        aCoder.encode(Gender,forKey: "Gender")
        aCoder.encode(BirthDate,forKey: "BirthDate")
        aCoder.encode(City,forKey: "City")
        aCoder.encode(County,forKey: "County")
        aCoder.encode(FamilySize,forKey: "FamilySize")
        aCoder.encode(Income,forKey: "Income")
        aCoder.encode(Graduation,forKey: "Graduation")
        aCoder.encode(Pet,forKey: "Pet")
        aCoder.encode(Child,forKey: "Child")
        aCoder.encode(Married,forKey: "Married")
        aCoder.encode(IsNotification,forKey: "IsNotification")
        aCoder.encode(PhoneNumber,forKey: "PhoneNumber")
        aCoder.encode(MemberImageUrl, forKey: "MemberImageUrl")
        
        // eklediklerim
        aCoder.encode(Adress, forKey: "Address")
        aCoder.encode(Plaka, forKey: "CarPlate")
    }


}











