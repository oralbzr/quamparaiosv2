//
//  Notifications.swift
//
//
//  Created by Banu on 16.08.2016.
//
//

import Foundation

class Notifications:NSObject,NSCoding {
    
    var PhoneNumber: String = ""
    var Notification: String = ""
    var NotificationType:Int = 0
    var NotificationId:Int = 0
    var NotificationDate: String = ""
    var CampaignId: Int?
    var ReceiptId: Int?
    var ReceiptImage: String?
    var SurveyId: Int?
    
    override init() {}
    
    required init(coder aDecoder:NSCoder) {
        if let PhoneNumber = aDecoder.decodeObject(forKey: "PhoneNumber") as? String {
            self.PhoneNumber = PhoneNumber
        }
        if let Notification = aDecoder.decodeObject(forKey: "Notification") as? String {
            self.Notification = Notification
        }
        if let NotificationType = aDecoder.decodeObject(forKey: "NotificationType") as? Int {
            self.NotificationType = NotificationType
        }
        if let NotificationId = aDecoder.decodeObject(forKey: "NotificationId") as? Int {
            self.NotificationId = NotificationId
        }
        if let NotificationDate = aDecoder.decodeObject(forKey: "NotificationDate") as? String {
            self.NotificationDate = NotificationDate
        }
        if let CampaignId = aDecoder.decodeObject(forKey: "CampaignId") as? Int {
            self.CampaignId = CampaignId
        }
        if let ReceiptId = aDecoder.decodeObject(forKey: "ReceiptId") as? Int {
            self.ReceiptId = ReceiptId
        }
        if let ReceiptImage = aDecoder.decodeObject(forKey: "ReceiptImage") as? String {
            self.ReceiptImage = ReceiptImage
        }
        if let SurveyId = aDecoder.decodeObject(forKey: "SurveyId") as? Int {
            self.SurveyId = SurveyId
        }
    }
    
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(PhoneNumber,forKey:"PhoneNumber")
        aCoder.encode(Notification,forKey: "Notification")
        aCoder.encode(NotificationType, forKey: "NotificationType")
        aCoder.encode(NotificationId, forKey: "NotificationId")
        aCoder.encode(NotificationDate, forKey: "NotificationDate")
        aCoder.encode(CampaignId, forKey:"CampaignId")
        aCoder.encode(ReceiptId, forKey:"ReceiptId")
        aCoder.encode(ReceiptImage, forKey:"ReceiptImage")
        aCoder.encode(SurveyId, forKey:"SurveyId")
    }
}
