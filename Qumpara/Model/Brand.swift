//
//  Brand.swift
//  Qumpara
//
//  Created by apple on 20.02.2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class Brand :NSObject, NSCoding {
    
    var BrandId:NSNumber = 0
    var BrandPicture:String = ""
    var BrandName:String = ""
    var IsCheckBox:Bool = false
   
    override init() {}
    
    required init(coder aDecoder: NSCoder) {
        
        if let BrandId = aDecoder.decodeObject(forKey: "BrandId") as? NSNumber {
            self.BrandId = BrandId
        }
        
        if let BrandPicture = aDecoder.decodeObject(forKey: "BrandPicture") as? String {
            self.BrandPicture = BrandPicture
        }
        
        if let BrandName = aDecoder.decodeObject(forKey: "BrandName") as? String {
            self.BrandName = BrandName
        }
        
        if let IsCheckBox = aDecoder.decodeObject(forKey: "IsCheckBox") as? Bool {
            self.IsCheckBox = IsCheckBox
        }
        
        
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(BrandId,forKey: "BrandId")
        aCoder.encode(BrandPicture,forKey: "BrandPicture")
        aCoder.encode(BrandName,forKey: "BrandName")
        aCoder.encode(BrandName,forKey: "IsCheckBox")
        
    }
    
}


    

