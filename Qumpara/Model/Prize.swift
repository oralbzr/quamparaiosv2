//
//  Prize.swift
//  Qumpara
//
//  Created by Oral Bozkır on 24/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class Prize: NSObject , NSCoding {
    var prizeId:String = ""
    var expirationDate:String = ""
    var prizeTitle:String = ""
    var prizeDescription:String = ""
    var prizeImageUrl:String = ""
    var prizeValue:NSNumber = 0
   
    
    override init() {}
    
    required init(coder aDecoder: NSCoder) {
        
        if let PrizeId = aDecoder.decodeObject(forKey: "CampaignId") as? String {
            self.prizeId = PrizeId
        }
        if let ExpirationDate = aDecoder.decodeObject(forKey: "CouponExpirationDate") as? String {
            self.expirationDate = ExpirationDate
        }
        if let PrizeTitle = aDecoder.decodeObject(forKey: "Title") as? String {
            self.prizeTitle = PrizeTitle
        }
        if let PrizeDescription = aDecoder.decodeObject(forKey: "CouponDescription") as? String {
            self.prizeDescription = PrizeDescription
        }
        if let PrizeImageUrl = aDecoder.decodeObject(forKey: "CouponImageUrl") as? String {
            self.prizeImageUrl = PrizeImageUrl
        }
        if let PrizeValue = aDecoder.decodeObject(forKey: "PrizeValue") as? NSNumber {
            self.prizeValue = PrizeValue
        }
        
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(prizeId,forKey: "CampaignId")
        aCoder.encode(expirationDate,forKey: "CouponExpirationDate")
        aCoder.encode(prizeTitle,forKey: "Title")
        aCoder.encode(prizeDescription,forKey: "CouponDescription")
        aCoder.encode(prizeImageUrl,forKey: "CouponImageUrl")
        aCoder.encode(prizeValue,forKey: "PrizeValue")
       
        
        
    }
}
