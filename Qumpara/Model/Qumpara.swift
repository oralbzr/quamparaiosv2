//
//  Qumpara.swift
//  Qumpara
//
//  Created by Banu on 21.08.2016.
//  Copyright © 2016 Nobium. All rights reserved.
//

import Foundation
//Qumpara servisinden gelen hesap bilgileri ve mali bilgileri tutan modeldir.
class Qumpara:NSObject,NSCoding {
    var AccumulatingMoney:String = ""
    var EarnedMoney:String = ""
    var ExpectedMoney:String = ""
    var IBANNo:String = ""
    var TCKN:String = ""
    
    override init() {}
    
    required init(coder aDecoder:NSCoder) {
        if let AccumulatingMoney = aDecoder.decodeObject(forKey: "AccumulatingMoney") as? String {
            self.AccumulatingMoney = AccumulatingMoney
        }
        if let EarnedMoney = aDecoder.decodeObject(forKey: "EarnedMoney") as? String {
            self.EarnedMoney = EarnedMoney
        }
        if let ExpectedMoney = aDecoder.decodeObject(forKey: "ExpectedMoney") as? String {
            self.ExpectedMoney = ExpectedMoney
        }
        if let IBANNo = aDecoder.decodeObject(forKey: "IBANNo") as? String {
            self.IBANNo = IBANNo
        }
        if let TCKN = aDecoder.decodeObject(forKey: "TCKN") as? String {
            self.TCKN = TCKN
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(AccumulatingMoney,forKey:"AccumulatingMoney")
        aCoder.encode(EarnedMoney, forKey: "EarnedMoney")
        aCoder.encode(ExpectedMoney, forKey: "ExpectedMoney")
        aCoder.encode(IBANNo, forKey: "IBANNo")
        aCoder.encode(TCKN, forKey: "TCKN")
    }

}
