//
//  Coupon.swift
//  Qumpara
//
//  Created by Oral Bozkır on 22/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class Coupon: NSObject , NSCoding {
    var couponId:String = ""
    var wonDate:String = ""
    var couponName:String = ""
    var isUsed:Bool = false
    var couponDescription:String = ""
    var code:String = ""
    var campaignImageUrl:String = ""
    var campaignDescription:String = ""
    var couponType:NSNumber = 0
     var campaignShareLink:String = ""
    
    override init() {}
    
    required init(coder aDecoder: NSCoder) {
        
        if let CouponId = aDecoder.decodeObject(forKey: "CouponId") as? String {
            self.couponId = CouponId
        }
        if let WonDate = aDecoder.decodeObject(forKey: "WonDate") as? String {
            self.wonDate = WonDate
        }
        if let IsUsed = aDecoder.decodeObject(forKey: "IsUsed") as? Bool {
            self.isUsed = IsUsed
        }
        if let CouponName = aDecoder.decodeObject(forKey: "CouponName") as? String {
            self.couponName = CouponName
        }
        if let CouponDescription = aDecoder.decodeObject(forKey: "CouponDescription") as? String {
            self.couponDescription = CouponDescription
        }
        if let Code = aDecoder.decodeObject(forKey: "Code") as? String {
            self.code = Code
        }
        if let CampaignImageUrl = aDecoder.decodeObject(forKey: "CampaignImageUrl") as? String {
            self.campaignImageUrl = CampaignImageUrl
        }
        if let CampaignDescription = aDecoder.decodeObject(forKey: "CampaignDescription") as? String {
            self.campaignDescription = CampaignDescription
        }
        if let CouponType = aDecoder.decodeObject(forKey: "CouponType") as? NSNumber {
            self.couponType = CouponType
        }
        if let CampaignShareLink = aDecoder.decodeObject(forKey: "CampaignShareLink") as? String {
            self.campaignShareLink = CampaignShareLink
        }
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(couponId,forKey: "CouponId")
        aCoder.encode(couponName,forKey: "CouponName")
        aCoder.encode(wonDate,forKey: "WonDate")
        aCoder.encode(isUsed,forKey: "IsUsed")
        aCoder.encode(couponDescription,forKey: "CouponDescription")
        aCoder.encode(campaignImageUrl,forKey: "CampaignImageUrl")
        aCoder.encode(campaignDescription,forKey: "CampaignDescription")
        aCoder.encode(couponType,forKey: "CouponType")
        aCoder.encode(code,forKey: "Code")
        aCoder.encode(campaignShareLink,forKey: "CampaignShareLink")
       
    
    }
}
