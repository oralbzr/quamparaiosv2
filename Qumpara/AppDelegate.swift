//
//  AppDelegate.swift
//  Qumpara
//
//  Created by Oral Bozkır on 21/11/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import UserNotifications
import FBSDKCoreKit
import FirebaseInstanceID
import GoogleSignIn
import GGLCore
import GGLSignIn
import CoreLocation
import FirebaseMessaging
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let defaults = UserDefaults.standard
    var deviceToken = ""
    var refresh = false
    var notReaded = true
    var locationManager:CLLocationManager = CLLocationManager()
    
    var pushNotifications:[[AnyHashable:Any]] = []
    var pushSet:Set<String> = Set([])
    var badgeCount:Int = 0
    
    var backgroundUpdateTask: UIBackgroundTaskIdentifier = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Utility.initializeStaticVariables()
        // window?.rootViewController = rootController
        
        //Setup HHTabBarView
        setupReferenceUITabBarController()
        setupHHTabBarView()
        
        //This is important.
        //Setup Application Window
        
        self.window?.rootViewController = self.referenceUITabBarController
        self.window?.makeKeyAndVisible()
        
        
        
        FIRApp.configure()
        
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        
        
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification(_:)),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        
        
        //FACEBOOK LOGIN
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //GOOGLE LOGIN
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        // assert(configureError == nil, "Error configuring Google services: \((describing: configureError))")
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handleFB:Bool = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        
        let handleGGL:Bool = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return handleFB && handleGGL
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundUpdateTask()
        })
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        // application.applicationIconBadgeNumber = self.badgeCount
    }
    
    func endBackgroundUpdateTask() {
        UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskInvalid
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        self.endBackgroundUpdateTask()
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        application.applicationIconBadgeNumber = self.badgeCount
        self.defaults.removeObject(forKey: "Campaigns")
        self.defaults.removeObject(forKey: "Notifications")
        self.defaults.removeObject(forKey: "Profile")
        self.defaults.removeObject(forKey: "Qumpara")
        self.defaults.removeObject(forKey: "Coupon")
        defaults.synchronize()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    let hhTabBarView = HHTabBarView.shared
    
    //Keeping reference of iOS default UITabBarController.
    let referenceUITabBarController = HHTabBarView.shared.referenceUITabBarController
    
    //2
    func setupReferenceUITabBarController() -> Void {
        
        //Creating a storyboard reference
        let storyboard = UIStoryboard(name: "Views", bundle: Bundle.main)
        // let mainController = storyboard.instantiateViewController(withIdentifier: "MainViewController")
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        let mainNavigationController: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "MainViewController"))
        mainNavigationController.view.isUserInteractionEnabled=true
        //Creating navigation controller for navigation inside the second tab.
        let qumparamNavigationController: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "QumparamViewController"))
        
        //Creating navigation controller for navigation inside the second tab.
        let photoNavigationController: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "PhotoViewController"))
        
        //Creating navigation controller for navigation inside the second tab.
        let notificationNavigationController: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "NotificationViewController"))
        
        //Creating navigation controller for navigation inside the second tab.
        let moreNavigationController: UINavigationController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "MoreViewController"))
        
        //Update referenced TabbarController with your viewcontrollers
        referenceUITabBarController.setViewControllers([mainNavigationController, qumparamNavigationController, photoNavigationController, notificationNavigationController, moreNavigationController], animated: false)
    }
    
    //3
    //Update HHTabBarView reference with the tabs requires.
    func setupHHTabBarView() -> Void {
        
        //Default & Selected Background Color
        let defaultTabColor = UIColor.init(red: 0.97, green: 0.49, blue: 0.43, alpha: 1.0)
        let selectedTabColor = UIColor.init(red: 0.78, green: 0.40, blue: 0.34, alpha: 1.0)
        
        //Create Custom Tabs
        let t1 = HHTabButton.init(tabImage: UIImage.init(named: "tabbar-home")!, index: 0)
        
        t1.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
        t1.setHHTabBackgroundColor(color: selectedTabColor, forState: .selected)
        
        let t2 = HHTabButton.init(tabImage: UIImage.init(named: "qumparalogo")!, index: 1)
        t2.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
        t2.setHHTabBackgroundColor(color: selectedTabColor, forState: .selected)
        
        let t3 = HHTabButton.init(tabImage: UIImage.init(named: "tabbar-camera")!, index: 2)
        t3.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
        t3.setHHTabBackgroundColor(color: selectedTabColor, forState: .selected)
        
        let t4 = HHTabButton.init(tabImage: UIImage.init(named: "tabbar-notification")!, index: 3)
        t4.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
        t4.setHHTabBackgroundColor(color: selectedTabColor, forState: .selected)
        
        let t5 = HHTabButton.init(tabImage: UIImage.init(named: "tabbar-menu")!, index: 4)
        t5.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
        t5.setHHTabBackgroundColor(color: selectedTabColor, forState: .selected)
        
        //Note: As tabs are subclassed of UIButton so you can modify it as much as possible.
        
        //Create Array of Custom Tabs
        var arrayTabs = Array<HHTabButton>()
        arrayTabs.append(t1)
        arrayTabs.append(t2)
        arrayTabs.append(t3)
        arrayTabs.append(t4)
        arrayTabs.append(t5)
        
        //Set Default Index for HHTabBarView.
        hhTabBarView.tabBarTabs = arrayTabs
        
        //You should modify badgeLabel after assigning tabs array.
        //t1.badgeLabel?.backgroundColor = .white
        //t1.badgeLabel?.textColor = selectedTabColor
        //t1.badgeLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
        
        //Handle Tab Change Event
        hhTabBarView.defaultIndex = 0
        
        //Show Animation on Switching Tabs
        hhTabBarView.tabChangeAnimationType = .none
        
        //Handle Tab Changes
        hhTabBarView.onTabTapped = { (tabIndex) in
            print("Selected Tab Index:\(tabIndex)")
        }
    }
    
    func hideTabBar() -> Void {
        hhTabBarView.isHidden = true
    }
    
    func showTabBar() -> Void {
        hhTabBarView.isHidden = false
    }
    
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Fisicek")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod) //TODO - sandbox to prod
    }
    
    @objc func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            
            defaults.set(refreshedToken, forKey: "DeviceToken")
            defaults.synchronize()
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        connectToFcm()
    }
    
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
                
                if self.defaults.object(forKey: "access_token") != nil {
                    if let token = FIRInstanceID.instanceID().token() {
                        let headersRegistration : HTTPHeaders = [
                            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                        ]
                        let parametersRegistration : [String: String] = [
                            "RegistrationId": "\(token)"
                        ]
                        Alamofire.request(APIURL.UpdateRegistration.rawValue, method: .post, parameters: parametersRegistration, encoding: JSONEncoding.default, headers: headersRegistration).responseJSON { response in
                            debugPrint(response)
                            print(parametersRegistration)
                            
                            let result = response.result
                            switch result {
                            case .success:
                                let statusCodeForRegistration = response.response!.statusCode
                                switch statusCodeForRegistration {
                                case 200:
                                    print("success!")
                                    break
                                default: break
                                }
                                break
                            case .failure:
                                break
                            }
                        }
                    }
                }
            }
        }
    }
    
    /* Push notifikasyona tıkladığı zaman buraya düşüyor */
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        // Print message ID.
        print("didReceiveRemoteNot")
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        // Print full message.
        print("%@", userInfo)
        
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        
        print(userInfo["notificationId"]!)
        
        var sid = 0
        var type = 0
        if defaults.object(forKey: "access_token") != nil {
            
        if let notId = userInfo["notificationId"] as? String {
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
            ]
            let parameters: [String: AnyObject] = [
                "NotificationId": notId as AnyObject
            ]
            
            Alamofire.request(APIURL.GetNotificationDetail.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                debugPrint(response)
                let result = response.result
                let httpCode = response.response!.statusCode
                
                switch result {
                case .success:
                    switch httpCode {
                    case 200:
                        if let data = result.value as? Dictionary<String, AnyObject> {
                            if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                if let ntype = responseData["Type"] as? Int {
                                    type = ntype
                                }
                                if let surveyid = responseData["SurveyId"] as? Int {
                                    sid = surveyid
                                }
                                let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?[3] as! UINavigationController
                                let storyboard = UIStoryboard(name: "Views", bundle: nil)
                                print(type)
                                
                                switch type {
                                case 1:     // Message
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationMessageViewController") as? NotificationMessageViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 2:     // Won
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationWonViewController") as? NotificationWonViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 3:     // Lost
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationLostViewController") as? NotificationLostViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 4:     // Questionnaire
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "SurveyViewController") as? SurveyViewController
                                    destinationViewController?.surveyID = Int.init(sid)
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 5:     // Campaign
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationCampaignViewController") as? NotificationCampaignViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                default: break
                                }
                            }
                        }
                    default: break
                    }
                case .failure: break
                }
            }
        }
        }
    }
    
    
    //BURAYA DÜŞÜYOR
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("didReceiveRemoteNotFetch")
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        // Print full message.
        print("%@", userInfo)
        
        print(userInfo["notificationId"]!)
        
        var sid = 0
        var type = 0
        
        if defaults.object(forKey: "access_token") != nil {
        
        if let notId = userInfo["notificationId"] as? String {
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
            ]
            let parameters: [String: AnyObject] = [
                "NotificationId": notId as AnyObject
            ]
            
            Alamofire.request(APIURL.GetNotificationDetail.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                debugPrint(response)
                let result = response.result
                let httpCode = response.response!.statusCode
                
                switch result {
                case .success:
                    switch httpCode {
                    case 200:
                        if let data = result.value as? Dictionary<String, AnyObject> {
                            if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                if let ntype = responseData["Type"] as? Int {
                                    type = ntype
                                }
                                if let surveyid = responseData["SurveyId"] as? Int {
                                    sid = surveyid
                                }
                                let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?[3] as! UINavigationController
                                let storyboard = UIStoryboard(name: "Views", bundle: nil)
                                print(type)
                                
                                switch type {
                                case 1:     // Message
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationMessageViewController") as? NotificationMessageViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 2:     // Won
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationWonViewController") as? NotificationWonViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 3:     // Lost
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationLostViewController") as? NotificationLostViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 4:     // Questionnaire
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "SurveyViewController") as? SurveyViewController
                                    destinationViewController?.surveyID = Int.init(sid)
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 5:     // Campaign
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationCampaignViewController") as? NotificationCampaignViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                default: break
                                }
                            }
                        }
                    default: break
                    }
                case .failure: break
                }
            }
        }
    }
    }
}



@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        print("will present notification")
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        // Print full message.
        print("%@", userInfo)
        
        print(userInfo["notificationId"]!)
        
        var sid = 0
        var type = 0
        
        if defaults.object(forKey: "access_token") != nil {
        if let notId = userInfo["notificationId"] as? String {
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
            ]
            let parameters: [String: AnyObject] = [
                "NotificationId": notId as AnyObject
            ]
            
            Alamofire.request(APIURL.GetNotificationDetail.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                debugPrint(response)
                let result = response.result
                let httpCode = response.response!.statusCode
                
                switch result {
                case .success:
                    switch httpCode {
                    case 200:
                        if let data = result.value as? Dictionary<String, AnyObject> {
                            if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                if let ntype = responseData["Type"] as? Int {
                                    type = ntype
                                }
                                if let surveyid = responseData["SurveyId"] as? Int {
                                    sid = surveyid
                                }
                                let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?[3] as! UINavigationController
                                let storyboard = UIStoryboard(name: "Views", bundle: nil)
                                print(type)
                                
                                switch type {
                                case 1:     // Message
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationMessageViewController") as? NotificationMessageViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 2:     // Won
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationWonViewController") as? NotificationWonViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 3:     // Lost
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationLostViewController") as? NotificationLostViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 4:     // Questionnaire
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "SurveyViewController") as? SurveyViewController
                                    destinationViewController?.surveyID = Int.init(sid)
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                case 5:     // Campaign
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationCampaignViewController") as? NotificationCampaignViewController
                                    destinationViewController?.notificationID = Int.init(notId)!
                                    navInTab.pushViewController(destinationViewController!, animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        HHTabBarView.shared.selectTabAtIndex(withIndex: 3)
                                    }
                                    break
                                default: break
                                }
                            }
                        }
                    default: break
                    }
                case .failure: break
                }
            }
        }
    }
    }
}
extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("%@", remoteMessage.appData)
    }
}
