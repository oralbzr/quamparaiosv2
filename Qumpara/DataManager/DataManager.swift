//
//  DataManager.swift
//  Qumpara
//
//  Created by Oral Bozkır on 08/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit

import Alamofire
import Alamofire_SwiftyJSON
import SwiftyJSON

 @objc protocol DataManagerDelegate: NSObjectProtocol{
   //  func networkCheckVersionCompleted()
   @objc optional func mainPageCampaignCompleted(_ result: [String:Any], withError errorDescription: NSString ,withCampaigns campaigns: [Campaigns])
   @objc optional func promotionsCompleted(_ result: [String:Any], withError errorDescription: NSString ,withPromotions promotions: [Campaigns])
}

class DataManager: NSObject {

   weak var delegate: DataManagerDelegate?
    let defaults = UserDefaults.standard
    class DataManager  {
        static let sharedInstance = DataManager()
    }
    
    override init() {
    super.init()
    }
    
    
    
    func sendRequest(_ postStr : [String: Any], withUrl apiUrlStr: NSString, withMethod methodString: String, withHeaders headers: HTTPHeaders? , withDelegateMethod delegateMethod: Selector , completeonClosure: @escaping ( [String: Any]?) -> ())  {
    
        let urlString : String  = apiUrlStr as String
        let method : HTTPMethod
        if methodString == "post"{
            method = .post as HTTPMethod
        }
        else{
            method  = .get as HTTPMethod
        }
        
        var header: HTTPHeaders? =  headers
        
        if headers == nil {
           header  = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        }
        
        Alamofire.request(urlString, method: method, parameters: nil, encoding: JSONEncoding.default, headers: header).validate(statusCode: 200..<303)
            .validate(contentType: ["application/json"]).responseJSON { dataResponse in
         //   print(dataResponse.response!)
           
            let json = JSON()
            var responseData: [String: Any] = [:]
               
            switch dataResponse.result {
            case .success:
               // print(dataResponse.result.value)
                
               print(JSON(dataResponse.result.value).rawString())
               
                guard let resultValue = dataResponse.result.value else{
                    responseData["jsonData"] = json
                    responseData["error"] = "error"
                    return
                }
                let json = JSON(dataResponse.result.value).rawString()
               let jsonp = JSON(parseJSON: json as! String)
                print(jsonp)
                responseData["jsonData"] = jsonp
                responseData["error"] = ""
                break
            case .failure(let error):
                responseData["jsonData"] = json
                responseData["error"] = "error"
                print(error)
            }
                completeonClosure(responseData)
    }
        
    }
    
    func sendMainPageCampaign() {
        
        self.sendRequest(["":""], withUrl:APIURL.GetCampaignList.rawValue as NSString, withMethod: "get", withHeaders:nil, withDelegateMethod: Selector(("mainPageCampaignCompleted"))){responseData in

            var campaigns:[Campaigns] = []
          
            var response =  responseData!
            
            var errorJsonData = response["error"] as! NSString
          
            let resultJsonData = response["jsonData"] as! JSON
          
            print(resultJsonData)
            
               print(resultJsonData["response"])
            
                    let resultsCampaign = (resultJsonData)["response"].arrayValue
            
                    for jsonCampaigns in resultsCampaign
                    {
                        let camp = Campaigns()
        
                        if let campId = (jsonCampaigns.dictionaryValue)["CampaignId"]{
                            
                            if campId.null != nil {
                                camp.CampaignId = ""
                            } else{
                            
                               camp.CampaignId = campId.stringValue
                            
                                }
                            }
                        
                        if let campDescription = (jsonCampaigns.dictionaryValue)["CampaignDescription"]{
                            
                            if campDescription.null != nil {
                                camp.Description = ""
                            } else{
                                camp.Description = campDescription.stringValue
                            }
                        }
                        if let campResimUrl = (jsonCampaigns.dictionaryValue)["CampaignImageUrl"]{
                            
                            if campResimUrl.null != nil {
                                camp.ResimUrl = ""
                            } else{
                                 camp.ResimUrl = campResimUrl .stringValue
                            }
                        }
                        if let campSubtitle = (jsonCampaigns.dictionaryValue)["Subtitle"]{
                            
                            if campSubtitle.null != nil {
                                camp.Subtitle = ""
                            } else{
                                camp.Subtitle = campSubtitle .stringValue
                            }
                        }
                        if let campTitle = (jsonCampaigns.dictionaryValue)["Title"]{
                            
                            if campTitle.null != nil {
                                camp.Title = ""
                            } else{
                                camp.Title = campTitle .stringValue
                            }
                        }
                        if let campRemainingDay = (jsonCampaigns.dictionaryValue)["RemainingDay"]{
                            
                            if campRemainingDay.null != nil {
                                camp.RemainingDay = ""
                            } else{
                                camp.RemainingDay = campRemainingDay .stringValue
                            }
                        }
                        
                        if let campIsAdvertisement = (jsonCampaigns.dictionaryValue)["IsAdvertisement"]{
                            
                            if campIsAdvertisement.null != nil {
                                camp.IsAdvertisement = false
                            } else{
                                camp.IsAdvertisement = campIsAdvertisement .boolValue
                            }
                        }
                        if let campIsOpenInApp = (jsonCampaigns.dictionaryValue)["IsOpenInApp"]{
                            
                            if campIsOpenInApp.null != nil {
                                camp.IsOpenInApp = false
                            } else{
                                camp.IsOpenInApp = campIsOpenInApp .boolValue
                            }
                        }
                        
                        if let campShareLink = (jsonCampaigns.dictionaryValue)["CampaignShareLink"]{
                            
                            if campShareLink.null != nil {
                                camp.CampaignShareLink = ""
                            } else{
                                camp.CampaignShareLink = campShareLink .stringValue
                            }
                        }
                        if let campAdvertisementURL = (jsonCampaigns.dictionaryValue)["AdvertisementURL"]{
                            
                            if campAdvertisementURL.null != nil {
                                camp.AdvertisementURL = ""
                            } else{
                                camp.AdvertisementURL = campAdvertisementURL .stringValue
                            }
                        }
                        campaigns.append(camp)
                    }
                    

          //  print(errorJsonData)
          //  print(resultJsonData)
            
                 DispatchQueue.main.async(execute: {() -> Void in
                    self.delegate?.mainPageCampaignCompleted!(responseData!, withError: errorJsonData, withCampaigns: campaigns)
                    
            })
        }
 
    }
    

    func getPromotions() {
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        
        self.sendRequest(["":""], withUrl:APIURL.GetPromotionList.rawValue as NSString, withMethod: "get", withHeaders: headers, withDelegateMethod: Selector(("promotionsCompleted"))){responseData in
    
            var campaigns:[Campaigns] = []
            var response =  responseData!
            var errorJsonData = response["error"] as! NSString
            
            let resultJsonData = response["jsonData"] as! JSON
            
            print(resultJsonData)
            
            print(resultJsonData["response"])
            
            let resultsCampaign = (resultJsonData)["response"].arrayValue
            
            for jsonCampaigns in resultsCampaign
            {
                let camp = Campaigns()
                
                if let campId = (jsonCampaigns.dictionaryValue)["CampaignId"]{
                    
                    if campId.null != nil {
                        camp.CampaignId = ""
                    } else{
                        
                        camp.CampaignId = campId.stringValue
                        
                    }
                }
                
                if let campDescription = (jsonCampaigns.dictionaryValue)["CampaignDescription"]{
                    
                    if campDescription.null != nil {
                        camp.Description = ""
                    } else{
                        camp.Description = campDescription.stringValue
                    }
                }
                if let campResimUrl = (jsonCampaigns.dictionaryValue)["CampaignImageUrl"]{
                    
                    if campResimUrl.null != nil {
                        camp.ResimUrl = ""
                    } else{
                        camp.ResimUrl = campResimUrl .stringValue
                    }
                }
                if let campSubtitle = (jsonCampaigns.dictionaryValue)["Subtitle"]{
                    
                    if campSubtitle.null != nil {
                        camp.Subtitle = ""
                    } else{
                        camp.Subtitle = campSubtitle .stringValue
                    }
                }
                if let campTitle = (jsonCampaigns.dictionaryValue)["Title"]{
                    
                    if campTitle.null != nil {
                        camp.Title = ""
                    } else{
                        camp.Title = campTitle .stringValue
                    }
                }
                if let campRemainingDay = (jsonCampaigns.dictionaryValue)["RemainingDay"]{
                    
                    if campRemainingDay.null != nil {
                        camp.RemainingDay = ""
                    } else{
                        camp.RemainingDay = campRemainingDay .stringValue
                    }
                }
                
                if let campIsAdvertisement = (jsonCampaigns.dictionaryValue)["IsAdvertisement"]{
                    
                    if campIsAdvertisement.null != nil {
                        camp.IsAdvertisement = false
                    } else{
                        camp.IsAdvertisement = campIsAdvertisement .boolValue
                    }
                }
                if let campIsOpenInApp = (jsonCampaigns.dictionaryValue)["IsOpenInApp"]{
                    
                    if campIsOpenInApp.null != nil {
                        camp.IsOpenInApp = false
                    } else{
                        camp.IsOpenInApp = campIsOpenInApp .boolValue
                    }
                }
                
                if let campShareLink = (jsonCampaigns.dictionaryValue)["CampaignShareLink"]{
                    
                    if campShareLink.null != nil {
                        camp.CampaignShareLink = ""
                    } else{
                        camp.CampaignShareLink = campShareLink .stringValue
                    }
                }
                campaigns.append(camp)
            }
    
           // print(errorJsonData)
           // print(resultJsonData)
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.delegate?.promotionsCompleted!(responseData!, withError: errorJsonData, withPromotions: campaigns)
                
            })
        }
        
    }
    
    
    
    
}
