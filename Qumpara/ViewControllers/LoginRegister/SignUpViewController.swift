//
//  SignUpViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 10/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKLoginKit
import GoogleSignIn
import MRCountryPicker
import WebKit

class SignUpViewController: UIViewController, MRCountryPickerDelegate, GIDSignInDelegate, GIDSignInUIDelegate, WKUIDelegate, WKNavigationDelegate {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var countryFlagImage: UIImageView!
    @IBOutlet weak var countryPhoneLabel: UILabel!
    @IBOutlet weak var phoneNumberText: UITextField!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var registerByPhoneBtn: UIButton!
    @IBOutlet weak var registerByGoogleBtn: UIButton!
    @IBOutlet weak var registerByFacebookBtn: UIButton!
    
    @IBOutlet weak var rulesCheckBox: UIButton!
    @IBOutlet weak var rulesLbl: UILabel!
    
    @IBOutlet weak var alreadyUser: UILabel!
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var webviewIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tabname: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addToolBar(textField: phoneNumberText)
        indicator.isHidden = true
        
        subView.isHidden = true
        webviewIndicator.isHidden = true
        
        countryPhoneLabel.layer.borderColor = UIColor.lightGray.cgColor
        countryPhoneLabel.layer.borderWidth = 0.5
        
        alreadyUser.layer.borderColor = UIColor(red:0.04, green:0.80, blue:0.76, alpha:1.0).cgColor
        alreadyUser.layer.borderWidth = 1
        alreadyUser.layer.cornerRadius = 10
        
        countryPhoneLabel.text = "+90"
        countryFlagImage.image = #imageLiteral(resourceName: "TR")
        phoneNumberText.keyboardType = UIKeyboardType.numberPad
        registerByPhoneBtn.setDisable()
        registerByGoogleBtn.setDisable()
        registerByFacebookBtn.setDisable()
        
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        
        countryPickerView.isHidden = true
        
        let alreadyUserAction = UITapGestureRecognizer(target: self, action: #selector(self.alreadyUserTapped(_:)))
        alreadyUser.isUserInteractionEnabled = true
        alreadyUser.addGestureRecognizer(alreadyUserAction)
        
        //        let tapAction = UITapGestureRecognizer(target: self, action: #selector(self.actionTapped(_:)))
        //        countryPhoneLabel.isUserInteractionEnabled = true
        //        countryPhoneLabel.addGestureRecognizer(tapAction)
        
        let rulesTapAction = UITapGestureRecognizer(target: self, action: #selector(self.rulesTapped(_:)))
        rulesLbl.isUserInteractionEnabled = true
        rulesLbl.addGestureRecognizer(rulesTapAction)
    }
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.countryPhoneLabel.text = phoneCode
        self.countryFlagImage.image = flag
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
    }
    
    @objc func rulesTapped(_ sender: UITapGestureRecognizer) {
        tappedCheckbox(rulesCheckBox)
    }
    
    @objc func actionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        view.addSubview(countryPickerView)
        countryPickerView.isHidden = false
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setLocale("tr_TR")
        countryPicker.setCountryByName("Türkiye")
    }
    
    @objc func alreadyUserTapped(_ sender: UITapGestureRecognizer) {
        if let signInView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController {
            if let navigator = self.navigationController {
                self.indicator.stopAnimating()
                navigator.show(signInView, sender: nil)
            }
        }
    }
    
    @IBAction func chooseBtnClicked(_ sender: UIButton) {
        self.countryPickerView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func phoneTextChanged(_ sender: UITextField) {
        textField(sender, shouldChangeCharactersIn: NSMakeRange(0, (sender.text?.count)!), replacementString: " ")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneNumberText {
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            var flag = false
            if (length - index) > 3 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@ ", areaCode)
                index += 3
            }
            if length - index > 3 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@ ", prefix)
                index += 3
                flag = true
            }
            if length - index > 2 && flag == true {
                let suffix = decimalString.substring(with: NSMakeRange(index, 2))
                formattedString.appendFormat("%@ ", suffix)
                index += 2
            }
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            
            if textField.text?.count == 13 && rulesCheckBox.isSelected {
                registerByPhoneBtn.setEnable()
            } else {
                registerByPhoneBtn.setDisable()
            }
            
            return false
            
        } else {
            return true
        }
    }
    
    @IBAction func phoneTextDone(_ sender: UITextField) {
    }
    
    @IBAction func signUpViaPhoneNumber(_ sender: UIButton) {
        phoneNumberText.resignFirstResponder()
        addBlurLayer()
        view.addSubview(self.indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let phoneNumber = self.phoneNumberText.text!
        let trimmedPhoneNumber = phoneNumber.replacingOccurrences(of: " ", with: "")
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let parameters: [String: AnyObject] = [
            "PhoneNumber" : trimmedPhoneNumber as AnyObject,
            "TypeId" : 0 as AnyObject
        ]
        
        Alamofire.request(APIURL.MemberNew2.rawValue,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let result = response.result
            
            switch result {
            case .success:
                if let data = result.value as? Dictionary<String, AnyObject> {
                    if let httpCode = response.response?.statusCode {
                        switch httpCode {
                        case 200:
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.removeBlurLayer()
                            if let verificationCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "VerificationCodeViewController") as? VerificationCodeViewController {
                                if let navigator = self.navigationController {
                                    self.indicator.stopAnimating()
                                    navigator.pushViewController(verificationCodeView, animated: true)
                                    self.defaults.set(trimmedPhoneNumber, forKey: "PhoneNumber")
                                    self.defaults.set(0, forKey: "loginType")
                                }
                            }
                            break
                        case 403:
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.removeBlurLayer()
                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                            break
                        case 404:
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.removeBlurLayer()
                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                            break
                        case 406:
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.removeBlurLayer()
                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                            break
                        case 409:
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.removeBlurLayer()
                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                            break
                        case 500:
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.removeBlurLayer()
                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                            break
                        default: break
                        }
                    }
                }
                break
            case .failure:
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                self.removeBlurLayer()
                let alert = UIAlertController(title: "Uyarı", message: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
        
    }
    
    @IBAction func signUpViaGoogle(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        addBlurLayer()
        view.addSubview(self.indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        if error == nil {            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
            let parameters: [String: AnyObject] = [
                "TypeId" : 2 as AnyObject,
                "Id": user.userID as AnyObject,
                "Name": user.profile.givenName as AnyObject,
                "SurName": user.profile.familyName as AnyObject,
                "Email": user.profile.email as AnyObject,
                "PhotoUrl": user.profile.imageURL(withDimension: 100).absoluteString as AnyObject
            ]
            
            // For OTPVerify2
            defaults.set(user.userID, forKey: "GoogleUserID")
            defaults.set(user.profile.givenName, forKey: "GoogleName")
            defaults.set(user.profile.familyName, forKey: "GoogleSurname")
            defaults.set(user.profile.email, forKey: "GoogleEmail")
            defaults.set(user.profile.imageURL(withDimension: 100).absoluteString, forKey: "GooglePhoto")
            
            Alamofire.request(APIURL.MemberNew2.rawValue,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                debugPrint(response)
                
                let result = response.result
                
                switch result {
                case .success:
                    let statusCode = response.response!.statusCode
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        switch statusCode {
                        case 200:
                            if let gSignUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "FBGSignUpViewController") as? FBGSignUpViewController {
                                if let navigator = self.navigationController {
                                    self.indicator.stopAnimating()
                                    gSignUpView.typeID = 2
                                    self.defaults.set(2, forKey: "loginType")
                                    self.indicator.stopAnimating()
                                    self.indicator.isHidden = true
                                    self.removeBlurLayer()
                                    navigator.pushViewController(gSignUpView, animated: true)
                                }
                            }
                            break
                        case 403, 404, 406, 500:
                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                            break
                        case 409:
                            if let signInView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController {
                                if let navigator = self.navigationController {
                                    self.indicator.stopAnimating()
                                    self.indicator.stopAnimating()
                                    self.indicator.isHidden = true
                                    self.removeBlurLayer()
                                    navigator.pushViewController(signInView, animated: true)
                                    //self.defaults.set(accessToken!.tokenString, forKey: "access_token")
                                    //self.defaults.set(1, forKey: "loginType")
                                }
                            }
                            break
                        default: break
                        }
                    }
                    break
                case .failure:
                    self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "toMain")
                    break
                }
            }
        } else {
            print(error)
            self.indicator.stopAnimating()
        }
    }
    
    @IBAction func signUpViaFacebook(_ sender: UIButton) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: [], from: self) { (result, error) -> Void in
            if (error == nil){
                if (result?.isCancelled)!{
                    let alert = UIAlertController(title: "Uyarı", message: "Giriş yapmadınız.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    if let navigator = self.navigationController {
                        HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                        navigator.popToRootViewController(animated: true)
                    }
                    return
                }
                else {
                    let accessToken = FBSDKAccessToken.current()
                    
                    if accessToken != nil {
                        self.addBlurLayer()
                        self.view.addSubview(self.indicator)
                        self.indicator.isHidden = false
                        self.indicator.startAnimating()
                        
                        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, first_name, last_name, picture.type(large), birthday, email, gender, location, relationship_status, significant_other, family, friends, likes"]).start(completionHandler: { (connection, result, error) -> Void in
                            
                            let dictionary = result as! Dictionary<String, Any>
                            print(dictionary)
                            
                            if let name = dictionary["first_name"] as? String {
                                if let surname = dictionary["last_name"] as? String {
                                    if let email = dictionary["email"] as? String {
                                        if let id = dictionary["id"] as? String {
                                            if let gender = dictionary["gender"] as? String {
                                                if let picture = dictionary["picture"] as? Dictionary<String, AnyObject> {
                                                    if let picData = picture["data"] as? Dictionary<String, AnyObject> {
                                                        
                                                        let headers: HTTPHeaders = [
                                                            "Content-Type": "application/json",
                                                            "Accept": "application/json"
                                                        ]
                                                        let parameters: [String: AnyObject] = [
                                                            "TypeId" : 1 as AnyObject,
                                                            "Id": id as AnyObject,
                                                            "Name": name as AnyObject,
                                                            "SurName": surname as AnyObject,
                                                            "Email": email as AnyObject,
                                                            "Gender": gender as AnyObject,
                                                            "PhotoUrl": picData["url"] as AnyObject
                                                        ]
                                                        
                                                        // For OTPVerify2
                                                        self.defaults.set(id, forKey: "FBUserID")
                                                        self.defaults.set(name, forKey: "FBName")
                                                        self.defaults.set(surname, forKey: "FBSurname")
                                                        self.defaults.set(email, forKey: "FBEmail")
                                                        self.defaults.set(picData["url"], forKey: "FBPhoto")
                                                        self.defaults.set(gender, forKey: "FBGender")
                                                        
                                                        Alamofire.request(APIURL.MemberNew2.rawValue,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                                                            debugPrint(response)
                                                            
                                                            let result = response.result
                                                            
                                                            switch result {
                                                            case .success:
                                                                let statusCode = response.response!.statusCode
                                                                if let data = result.value as? Dictionary<String, AnyObject> {
                                                                    switch statusCode {
                                                                    case 200:
                                                                        if let fbSignUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "FBGSignUpViewController") as? FBGSignUpViewController {
                                                                            if let navigator = self.navigationController {
                                                                                self.indicator.stopAnimating()
                                                                                self.indicator.stopAnimating()
                                                                                self.indicator.isHidden = true
                                                                                self.removeBlurLayer()
                                                                                navigator.pushViewController(fbSignUpView, animated: true)
                                                                                //self.defaults.set(accessToken!.tokenString, forKey: "access_token")
                                                                                self.defaults.set(1, forKey: "loginType")
                                                                                fbSignUpView.typeID = 1
                                                                            }
                                                                        }
                                                                        break
                                                                    case 403, 404, 406, 500:
                                                                        self.indicator.isHidden = true
                                                                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                                        break
                                                                    case 409:
                                                                        if let signInView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController {
                                                                            if let navigator = self.navigationController {
                                                                                self.indicator.stopAnimating()
                                                                                self.indicator.stopAnimating()
                                                                                self.indicator.isHidden = true
                                                                                self.removeBlurLayer()
                                                                                navigator.pushViewController(signInView, animated: true)
                                                                                //self.defaults.set(accessToken!.tokenString, forKey: "access_token")
                                                                                //self.defaults.set(1, forKey: "loginType")
                                                                            }
                                                                        }
                                                                        break
                                                                    default: break
                                                                    }
                                                                }
                                                                break
                                                            case .failure:
                                                                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "toMain")
                                                                break
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        )
                    }
                }
            }
        }
    }
    
    @IBAction func tappedCheckbox(_ sender: UIButton) {
        if rulesCheckBox.isSelected {
            rulesCheckBox.isSelected = false
        } else {
            rulesCheckBox.isSelected = true
        }
        if phoneNumberText.text?.count == 13 && rulesCheckBox.isSelected {
            registerByPhoneBtn.setEnable()
            registerByFacebookBtn.setEnable()
            registerByGoogleBtn.setEnable()
        } else if rulesCheckBox.isSelected {
            registerByFacebookBtn.setEnable()
            registerByGoogleBtn.setEnable()
        } else {
            registerByPhoneBtn.setDisable()
            registerByFacebookBtn.setDisable()
            registerByGoogleBtn.setDisable()
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        if let navigator = self.navigationController {
            self.indicator.stopAnimating()
            HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
            navigator.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func userAgreementTapped(_ sender: UIButton) {
        loadWebView(withTitle: "Kullanıcı Sözleşmesi", withURL: "https://www.fisicek.com/web/UserContract.htm")
    }
    
    @IBAction func privacyAgreementTapped(_ sender: UIButton) {
        loadWebView(withTitle: "Gizlilik Sözleşmesi", withURL: "https://www.fisicek.com/web/PrivateContract.htm")
    }
    
    func loadWebView(withTitle title: String, withURL url: String) {
        let myURL = URL(string: url)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        tabname.text = title
        subView.isHidden = false
    }
    
    @IBAction func webviewCloseTapped(_ sender: UIButton) {
        subView.isHidden = true
        let url = URL(string: "about:blank")
        let request = URLRequest(url: url!)
        webView.load(request)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        webviewIndicator.center = webView.center
        webView.addSubview(webviewIndicator)
        webviewIndicator.isHidden = false
        webviewIndicator.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webviewIndicator.stopAnimating()
        webviewIndicator.isHidden = true
    }
    
}

extension UIViewController: UITextFieldDelegate {
    func addToolBar(textField: UITextField){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 253.0/255, green: 106.0/255, blue: 80.0/255.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Tamam", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let cancelButton = UIBarButtonItem(title: "Vazgeç", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    @objc func donePressed(){
        view.endEditing(true)
    }
    @objc func cancelPressed(){
        view.endEditing(true)
    }
}

extension UIViewController: UITextViewDelegate {
    func addToolBar(textField: UITextView){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 253.0/255, green: 106.0/255, blue: 80.0/255.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Tamam", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let cancelButton = UIBarButtonItem(title: "Vazgeç", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    @objc func doneViewPressed(){
        view.endEditing(true)
    }
    @objc func cancelViewPressed(){
        view.endEditing(true)
    }
}

extension UIViewController {
    func addBlurLayer() {
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.tag = 1000
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
    }
    
    func removeBlurLayer() {
        view.viewWithTag(1000)?.removeFromSuperview()
    }
    
    func showAlert(withTitle title: String, withMessage message: String, withAction act: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            if act == "pop" {
                self.navigationController?.popViewController(animated: true)
            } else if act == "toMain" {
                self.navigationController?.popViewController(animated: true)
                HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
            } else if act == "root" {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showError(withError err: Dictionary<String, AnyObject>, withAction act: String) {
        if let error = err["response"]?["ErrorDescription"] as? String {
            let alert = UIAlertController(title: "Hata", message: error, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                if act == "pop" {
                    self.navigationController?.popViewController(animated: true)
                } else if act == "toMain" {
                    self.navigationController?.popViewController(animated: true)
                    HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                } else if act == "root" {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            showAlert(withTitle: "Hata", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: act)
        }
        
    }
    
    func unauthorized(withError err: Dictionary<String, AnyObject>) {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
        if let error = err["response"]?["ErrorDescription"] as? String {
            showAlert(withTitle: "Hata", withMessage: error, withAction: "toMain")
        }
    }
}

extension UIButton {
    func setDisable() {
        self.isEnabled = false
        self.isUserInteractionEnabled = false
        self.alpha = 0.5
    }
    func setEnable() {
        self.isEnabled = true
        self.isUserInteractionEnabled = true
        self.alpha = 1.0
    }
}
