//
//  VerificationCodeViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 1/23/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseInstanceID
import FirebaseMessaging

class VerificationCodeViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var verificationCodeTextfield: UITextField!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var closeButton: UIButton!
    
    var isLogin = false
    
    var timer = Timer()
    var totalTime = 180
    var timeRemaining = 180
    var typeID = 0 // facebook:1 google:2
    var deviceToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if defaults.object(forKey: "DeviceToken") != nil {
            deviceToken = defaults.object(forKey: "DeviceToken")! as! String
        } else {
            if let token = FIRInstanceID.instanceID().token() {
                deviceToken = token
            }
        }
        
        continueButton.setDisable()
        closeButton.isHidden = true
        
        addToolBar(textField: verificationCodeTextfield)
        verificationCodeTextfield.keyboardType = UIKeyboardType.numberPad
        
        indicator.isHidden = true
        resendButton.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
    }
    
    @objc func timerRunning() {
        timeRemaining -= 1
        progressView.setProgress(Float(timeRemaining)/Float(totalTime), animated: false)
        let secondsLeft = Int(timeRemaining)
        timeRemainingLabel.text = "Kalan Süre: \(secondsLeft) sn"
        
        if timeRemaining == 0 {
            timer.invalidate()
            timeRemainingLabel.isHidden = true
            continueButton.setDisable()
            verificationCodeTextfield.text = ""
            resendButton.isHidden = false
            closeButton.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
        timeRemainingLabel.text = "Kalan Süre: \(timeRemaining) sn"
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        verificationCodeTextfield.resignFirstResponder()
        addBlurLayer()
        view.addSubview(self.indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let code = self.verificationCodeTextfield.text
        
        switch typeID {
        case 0:
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
            let parameters: [String: AnyObject] = [
                "PhoneNumber": self.defaults.object(forKey: "PhoneNumber")! as AnyObject,
                "TypeId": typeID as AnyObject,
                "SMSCode": code! as AnyObject
            ]
            
            Alamofire.request(APIURL.OTPVerify2.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                self.addBlurLayer()
                self.view.addSubview(self.indicator)
                self.indicator.isHidden = false
                self.indicator.startAnimating()
                
                debugPrint(response)
                
                print(self.typeID)
                
                if let memberInfoView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "MemberInfoViewController") as? MemberInfoViewController {
                    if let navigator = self.navigationController {
                        
                        let result = response.result
                        
                        switch result {
                        case .success:
                            let statusCode = response.response!.statusCode
                            switch statusCode {
                            case 200:
                                if let data = result.value as? Dictionary<String, AnyObject> {
                                    print(data)
                                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                        self.defaults.set(responseData["PhoneNumber"]!, forKey: "PhoneNumber")
                                        self.defaults.set(responseData["MemberId"]!, forKey: "MemberId")
                                        self.defaults.set(responseData["Password"]!, forKey: "Password")
                                    }
                                    
                                    let heads: HTTPHeaders = [
                                        "Content-Type" : "application/x-www-form-urlencoded",
                                        "Accept" : "application/json"
                                    ]
                                    let params: [String: AnyObject] = [
                                        "grant_type" : "password" as AnyObject,
                                        "username" : self.defaults.object(forKey: "PhoneNumber") as AnyObject,
                                        "password" : self.defaults.object(forKey: "Password") as AnyObject
                                    ]
                                    Alamofire.request(APIURL.getToken.rawValue, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: heads).responseJSON { response in
                                        let result = response.result
                                        switch result {
                                        case .success:
                                            if let data = result.value as? Dictionary<String, AnyObject> {
                                                print(data)
                                                self.defaults.set(data["access_token"]!, forKey: "access_token")
                                                print(data["access_token"]!)
                                                
                                                self.defaults.set(data["token_type"]!, forKey: "token_type")
                                            }
                                            let headersUpdate : HTTPHeaders = [
                                                "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                            ]
                                            let parametersUpdate : [String: String] = [
                                                "OperationSystem": "IOS",
                                                "VersionName": Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
                                            ]
                                            Alamofire.request(APIURL.MemberVersionUpdate.rawValue, method: .post, parameters: parametersUpdate, encoding: JSONEncoding.default, headers: headersUpdate).responseJSON { response in
                                                debugPrint(response)
                                                let result = response.result
                                                switch result {
                                                case .success:
                                                    let statusCode = response.response!.statusCode
                                                    switch statusCode {
                                                    case 200:
                                                        let headersRegistration : HTTPHeaders = [
                                                            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                                        ]
                                                        let parametersRegistration : [String: String] = [
                                                            "RegistrationId": "\(self.deviceToken)"
                                                        ]
                                                        Alamofire.request(APIURL.UpdateRegistration.rawValue, method: .post, parameters: parametersRegistration, encoding: JSONEncoding.default, headers: headersRegistration).responseJSON { response in
                                                            debugPrint(response)
                                                            print(parametersRegistration)
                                                            
                                                            let result = response.result
                                                            switch result {
                                                            case .success:
                                                                let statusCodeForRegistration = response.response!.statusCode
                                                                switch statusCodeForRegistration {
                                                                case 200:
                                                                    print("success!")
                                                                    break
                                                                case 401:
                                                                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                                    break
                                                                case 403, 404, 405, 406, 500:
                                                                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                                    self.indicator.stopAnimating()
                                                                    self.indicator.isHidden = true
                                                                    self.removeBlurLayer()
                                                                    break
                                                                default: break
                                                                }
                                                                break
                                                            case .failure:
                                                                self.showAlert(withTitle: "Hata Kodu: \(statusCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "")
                                                                self.indicator.stopAnimating()
                                                                self.indicator.isHidden = true
                                                                self.removeBlurLayer()
                                                                break
                                                            }
                                                        }
                                                        break
                                                    case 401:
                                                        self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                        break
                                                    case 403, 404, 405, 406, 500:
                                                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                        self.indicator.stopAnimating()
                                                        self.indicator.isHidden = true
                                                        self.removeBlurLayer()
                                                        break
                                                    default: break
                                                    }
                                                    break
                                                case .failure:
                                                    self.showAlert(withTitle: "Hata Kodu: \(statusCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "")
                                                    self.indicator.stopAnimating()
                                                    self.indicator.isHidden = true
                                                    self.removeBlurLayer()
                                                    break
                                                }
                                            }
                                            
                                            self.indicator.stopAnimating()
                                            self.indicator.isHidden = true
                                            self.removeBlurLayer()
                                            switch self.defaults.integer(forKey: "loginType") {
                                            case 0:
                                                if self.isLogin == false {
                                                    navigator.pushViewController(memberInfoView, animated: true)
                                                } else {
                                                    HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                    navigator.popToRootViewController(animated: true)
                                                }
                                                break
                                            case 1:
                                                //HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                if let useFriendsCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "UseFriendsCodeController") as? UseFriendsCodeController {
                                                    if let navigator = self.navigationController {
                                                        navigator.pushViewController(useFriendsCodeView, animated: true)
                                                    }
                                                }
                                                break
                                            case 2:
                                                //HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                if let useFriendsCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "UseFriendsCodeController") as? UseFriendsCodeController {
                                                    if let navigator = self.navigationController {
                                                        navigator.pushViewController(useFriendsCodeView, animated: true)
                                                    }
                                                }
                                                break
                                            default:
                                                break
                                            }
                                            break
                                        case .failure:
                                            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                            break
                                        }
                                    }
                                }
                                break
                                //                            case 401, 403, 404, 406, 500:
                                //                                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                            //                                break
                            default:
                                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                self.indicator.stopAnimating()
                                self.indicator.isHidden = true
                                self.removeBlurLayer()
                                break
                            }
                            break
                        case .failure:
                            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.removeBlurLayer()
                            break
                        }
                    }
                }
            }
            break
        case 1:
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
            let parameters: [String: AnyObject] = [
                "PhoneNumber": self.defaults.object(forKey: "PhoneNumber")! as AnyObject,
                "TypeId" : typeID as AnyObject,
                "SMSCode": code! as AnyObject,
                "Name": self.defaults.object(forKey: "FBName")! as AnyObject,
                "SurName": self.defaults.object(forKey: "FBSurname")! as AnyObject,
                "PhotoUrl": self.defaults.object(forKey: "FBPhoto")! as AnyObject,
                "Email": self.defaults.object(forKey: "FBEmail")! as AnyObject,
                "Id": self.defaults.object(forKey: "FBUserID")! as AnyObject,
                "Gender": self.defaults.object(forKey: "FBGender") as AnyObject
            ]
            Alamofire.request(APIURL.OTPVerify2.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                self.addBlurLayer()
                self.view.addSubview(self.indicator)
                self.indicator.isHidden = false
                self.indicator.startAnimating()
                debugPrint(response)
                print(self.typeID)
                if let memberInfoView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "MemberInfoViewController") as? MemberInfoViewController {
                    if let navigator = self.navigationController {
                        let result = response.result
                        switch result {
                        case .success:
                            let statusCode = response.response!.statusCode
                            switch statusCode {
                            case 200:
                                if let data = result.value as? Dictionary<String, AnyObject> {
                                    print(data)
                                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                        self.defaults.set(responseData["PhoneNumber"]!, forKey: "PhoneNumber")
                                        self.defaults.set(responseData["MemberId"]!, forKey: "MemberId")
                                        self.defaults.set(responseData["Password"]!, forKey: "Password")
                                    }
                                    let heads: HTTPHeaders = [
                                        "Content-Type" : "application/x-www-form-urlencoded",
                                        "Accept" : "application/json"
                                    ]
                                    let params: [String: AnyObject] = [
                                        "grant_type" : "password" as AnyObject,
                                        "username" : self.defaults.object(forKey: "PhoneNumber") as AnyObject,
                                        "password" : self.defaults.object(forKey: "Password") as AnyObject
                                    ]
                                    Alamofire.request(APIURL.getToken.rawValue, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: heads).responseJSON { response in
                                        let result = response.result
                                        switch result {
                                        case .success:
                                            if let data = result.value as? Dictionary<String, AnyObject> {
                                                print(data)
                                                self.defaults.set(data["access_token"]!, forKey: "access_token")
                                                print(data["access_token"]!)
                                                
                                                self.defaults.set(data["token_type"]!, forKey: "token_type")
                                            }
                                            let headersUpdate : HTTPHeaders = [
                                                "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                            ]
                                            let parametersUpdate : [String: String] = [
                                                "OperationSystem": "IOS",
                                                "VersionName": Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
                                            ]
                                            Alamofire.request(APIURL.MemberVersionUpdate.rawValue, method: .post, parameters: parametersUpdate, encoding: JSONEncoding.default, headers: headersUpdate).responseJSON { response in
                                                debugPrint(response)
                                                let result = response.result
                                                switch result {
                                                case .success:
                                                    let statusCode = response.response!.statusCode
                                                    switch statusCode {
                                                    case 200:
                                                        let headersRegistration : HTTPHeaders = [
                                                            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                                        ]
                                                        let parametersRegistration : [String: String] = [
                                                            "RegistrationId": "\(self.deviceToken)"
                                                        ]
                                                        Alamofire.request(APIURL.UpdateRegistration.rawValue, method: .post, parameters: parametersRegistration, encoding: JSONEncoding.default, headers: headersRegistration).responseJSON { response in
                                                            debugPrint(response)
                                                            print(parametersRegistration)
                                                            
                                                            let result = response.result
                                                            switch result {
                                                            case .success:
                                                                let statusCodeForRegistration = response.response!.statusCode
                                                                switch statusCodeForRegistration {
                                                                case 200:
                                                                    print("success!")
                                                                    break
                                                                case 401:
                                                                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                                    break
                                                                case 403, 404, 405, 406, 500:
                                                                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                                    self.indicator.stopAnimating()
                                                                    self.indicator.isHidden = true
                                                                    self.removeBlurLayer()
                                                                    break
                                                                default: break
                                                                }
                                                                break
                                                            case .failure:
                                                                self.showAlert(withTitle: "Hata Kodu: \(statusCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "")
                                                                self.indicator.stopAnimating()
                                                                self.indicator.isHidden = true
                                                                self.removeBlurLayer()
                                                                break
                                                            }
                                                        }
                                                        break
                                                    case 401:
                                                        self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                        break
                                                    case 403, 404, 405, 406, 500:
                                                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                        self.indicator.stopAnimating()
                                                        self.indicator.isHidden = true
                                                        self.removeBlurLayer()
                                                        break
                                                    default: break
                                                    }
                                                    break
                                                case .failure:
                                                    self.showAlert(withTitle: "Hata Kodu: \(statusCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "")
                                                    self.indicator.stopAnimating()
                                                    self.indicator.isHidden = true
                                                    self.removeBlurLayer()
                                                    break
                                                }
                                            }
                                            self.indicator.stopAnimating()
                                            self.indicator.isHidden = true
                                            self.removeBlurLayer()
                                            switch self.defaults.integer(forKey: "loginType") {
                                            case 0:
                                                if self.isLogin == false {
                                                    navigator.pushViewController(memberInfoView, animated: true)
                                                } else {
                                                    HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                    navigator.popToRootViewController(animated: true)
                                                }
                                                break
                                            case 1:
                                                //HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                if let useFriendsCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "UseFriendsCodeController") as? UseFriendsCodeController {
                                                    if let navigator = self.navigationController {
                                                        navigator.pushViewController(useFriendsCodeView, animated: true)
                                                    }
                                                }
                                                break
                                            case 2:
                                                //HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                if let useFriendsCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "UseFriendsCodeController") as? UseFriendsCodeController {
                                                    if let navigator = self.navigationController {
                                                        navigator.pushViewController(useFriendsCodeView, animated: true)
                                                    }
                                                }
                                                break
                                            default:
                                                break
                                            }
                                            
                                            break
                                        case .failure:
                                            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                            break
                                        }
                                    }
                                }
                                break
                            case 401, 403, 404, 406, 500:
                                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                self.indicator.stopAnimating()
                                self.indicator.isHidden = true
                                self.removeBlurLayer()
                                break
                            default: break
                            }
                            break
                        case .failure:
                            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            break
                        }
                    }
                }
            }
            break
        case 2:
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
            let parameters: [String: AnyObject] = [
                "PhoneNumber" : self.defaults.object(forKey: "PhoneNumber")! as AnyObject,
                "TypeId" : typeID as AnyObject,
                "SMSCode": code! as AnyObject,
                "Name": self.defaults.object(forKey: "GoogleName")! as AnyObject,
                "SurName": self.defaults.object(forKey: "GoogleSurname")! as AnyObject,
                "PhotoUrl": self.defaults.object(forKey: "GooglePhoto")! as AnyObject,
                "Email": self.defaults.object(forKey: "GoogleEmail")! as AnyObject,
                "Id": self.defaults.object(forKey: "GoogleUserID")! as AnyObject
            ]
            Alamofire.request(APIURL.OTPVerify2.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                self.addBlurLayer()
                self.view.addSubview(self.indicator)
                self.indicator.isHidden = false
                self.indicator.startAnimating()
                
                debugPrint(response)
                
                print(self.typeID)
                
                if let memberInfoView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "MemberInfoViewController") as? MemberInfoViewController {
                    if let navigator = self.navigationController {
                        
                        let result = response.result
                        
                        switch result {
                        case .success:
                            let statusCode = response.response!.statusCode
                            switch statusCode {
                            case 200:
                                if let data = result.value as? Dictionary<String, AnyObject> {
                                    print(data)
                                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                        self.defaults.set(responseData["PhoneNumber"]!, forKey: "PhoneNumber")
                                        self.defaults.set(responseData["MemberId"]!, forKey: "MemberId")
                                        self.defaults.set(responseData["Password"]!, forKey: "Password")
                                    }
                                    let heads: HTTPHeaders = [
                                        "Content-Type" : "application/x-www-form-urlencoded",
                                        "Accept" : "application/json"
                                    ]
                                    let params: [String: AnyObject] = [
                                        "grant_type" : "password" as AnyObject,
                                        "username" : self.defaults.object(forKey: "PhoneNumber") as AnyObject,
                                        "password" : self.defaults.object(forKey: "Password") as AnyObject
                                    ]
                                    Alamofire.request(APIURL.getToken.rawValue, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: heads).responseJSON { response in
                                        let result = response.result
                                        
                                        switch result {
                                        case .success:
                                            if let data = result.value as? Dictionary<String, AnyObject> {
                                                print(data)
                                                self.defaults.set(data["access_token"]!, forKey: "access_token")
                                                print(data["access_token"]!)
                                                
                                                self.defaults.set(data["token_type"]!, forKey: "token_type")
                                            }
                                            let headersUpdate : HTTPHeaders = [
                                                "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                            ]
                                            let parametersUpdate : [String: String] = [
                                                "OperationSystem": "IOS",
                                                "VersionName": Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
                                            ]
                                            Alamofire.request(APIURL.MemberVersionUpdate.rawValue, method: .post, parameters: parametersUpdate, encoding: JSONEncoding.default, headers: headersUpdate).responseJSON { response in
                                                debugPrint(response)
                                                let result = response.result
                                                switch result {
                                                case .success:
                                                    let statusCode = response.response!.statusCode
                                                    switch statusCode {
                                                    case 200:
                                                        let headersRegistration : HTTPHeaders = [
                                                            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                                        ]
                                                        let parametersRegistration : [String: String] = [
                                                            "RegistrationId": "\(self.deviceToken)"
                                                        ]
                                                        Alamofire.request(APIURL.UpdateRegistration.rawValue, method: .post, parameters: parametersRegistration, encoding: JSONEncoding.default, headers: headersRegistration).responseJSON { response in
                                                            debugPrint(response)
                                                            print(parametersRegistration)
                                                            
                                                            let result = response.result
                                                            switch result {
                                                            case .success:
                                                                let statusCodeForRegistration = response.response!.statusCode
                                                                switch statusCodeForRegistration {
                                                                case 200:
                                                                    print("success!")
                                                                    break
                                                                case 401:
                                                                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                                    break
                                                                case 403, 404, 405, 406, 500:
                                                                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                                    self.indicator.stopAnimating()
                                                                    self.indicator.isHidden = true
                                                                    self.removeBlurLayer()
                                                                    break
                                                                default: break
                                                                }
                                                                break
                                                            case .failure:
                                                                self.showAlert(withTitle: "Hata Kodu: \(statusCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "")
                                                                self.indicator.stopAnimating()
                                                                self.indicator.isHidden = true
                                                                self.removeBlurLayer()
                                                                break
                                                            }
                                                        }
                                                        break
                                                    case 401:
                                                        self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                        break
                                                    case 403, 404, 405, 406, 500:
                                                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                        self.indicator.stopAnimating()
                                                        self.indicator.isHidden = true
                                                        self.removeBlurLayer()
                                                        break
                                                    default: break
                                                    }
                                                    break
                                                case .failure:
                                                    self.showAlert(withTitle: "Hata Kodu: \(statusCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "")
                                                    self.indicator.stopAnimating()
                                                    self.indicator.isHidden = true
                                                    self.removeBlurLayer()
                                                    break
                                                }
                                            }
                                            self.indicator.stopAnimating()
                                            self.indicator.isHidden = true
                                            self.removeBlurLayer()
                                            switch self.defaults.integer(forKey: "loginType") {
                                            case 0:
                                                if self.isLogin == false {
                                                    navigator.pushViewController(memberInfoView, animated: true)
                                                } else {
                                                    HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                    navigator.popToRootViewController(animated: true)
                                                }
                                                break
                                            case 1:
                                                //HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                if let useFriendsCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "UseFriendsCodeController") as? UseFriendsCodeController {
                                                    if let navigator = self.navigationController {
                                                        navigator.pushViewController(useFriendsCodeView, animated: true)
                                                    }
                                                }
                                                break
                                            case 2:
                                                //HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                if let useFriendsCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "UseFriendsCodeController") as? UseFriendsCodeController {
                                                    if let navigator = self.navigationController {
                                                        navigator.pushViewController(useFriendsCodeView, animated: true)
                                                    }
                                                }
                                                break
                                            default:
                                                break
                                            }
                                            break
                                        case .failure:
                                            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                            break
                                        }
                                    }
                                }
                                break
                            case 401, 403, 404, 406, 500:
                                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                self.indicator.stopAnimating()
                                self.indicator.isHidden = true
                                self.removeBlurLayer()
                                break
                            default: break
                            }
                            break
                        case .failure:
                            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            break
                        }
                    }
                }
            }
            break
        default:
            self.showAlert(withTitle: "Hata", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "root")
            break
        }
        self.indicator.stopAnimating()
        self.indicator.isHidden = true
        self.removeBlurLayer()
    }
    
    @IBAction func resendButtonTapped(_ sender: UIButton) {
        timeRemaining = 180
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
        timeRemainingLabel.isHidden = false
        resendButton.isHidden = true
        print("Resend button tapped!")
        let trimmedPhoneNumber = defaults.object(forKey: "PhoneNumber")
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let parameters: [String: AnyObject] = [
            "PhoneNumber" : trimmedPhoneNumber as AnyObject,
            "TypeId" : typeID as AnyObject
        ]
        Alamofire.request("https://www.fisicek.com/Application/api/PhoneVerify",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            
            switch result {
            case .success:
                super.viewDidLoad()
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        textField(sender, shouldChangeCharactersIn: NSMakeRange(0, (sender.text?.count)!), replacementString: "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField.text?.count == 4 {
            continueButton.setEnable()
        } else {
            continueButton.setDisable()
        }
        return newString.length <= maxLength
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        if let navigator = self.navigationController {
            self.indicator.stopAnimating()
            HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
            navigator.popToRootViewController(animated: true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
