//
//  FBGSignUpViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/1/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire
import MRCountryPicker

class FBGSignUpViewController: UIViewController, MRCountryPickerDelegate {
    
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UITextField!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var continueButton: UIButton!
    
    var defaults = UserDefaults.standard
    var typeID = 0 // facebook:1 google:2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addToolBar(textField: phoneNumberLbl)
        
        indicator.isHidden = true
        countryPickerView.isHidden = true
        countryCodeLbl.text = "+90"
        countryFlag.image = #imageLiteral(resourceName: "TR")
        phoneNumberLbl.keyboardType = UIKeyboardType.numberPad
        continueButton.setDisable()
        
//        let tapAction = UITapGestureRecognizer(target: self, action: #selector(self.actionTapped(_:)))
//        countryCodeLbl.isUserInteractionEnabled = true
//        countryCodeLbl.addGestureRecognizer(tapAction)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
    }
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.countryCodeLbl.text = phoneCode
        self.countryFlag.image = flag
    }
    
    @objc func actionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        view.addSubview(countryPickerView)
        countryPickerView.isHidden = false
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setLocale("tr_TR")
        countryPicker.setCountryByName("Türkiye")
    }
    
    @IBAction func chooseCountry(_ sender: UIButton) {
        self.countryPickerView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func loginViaPhone(_ sender: UIButton) {
        phoneNumberLbl.resignFirstResponder()
        addBlurLayer()
        view.addSubview(self.indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let phoneNumber = self.phoneNumberLbl.text!
        let trimmedPhoneNumber = phoneNumber.replacingOccurrences(of: " ", with: "")
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let parameters: [String: AnyObject] = [
            "PhoneNumber" : trimmedPhoneNumber as AnyObject,
            "TypeId" : typeID as AnyObject
        ]
        
        Alamofire.request(APIURL.PhoneVerify.rawValue,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            
            let result = response.result
            print(self.typeID)
            
            switch result {
            case .success:
                let statusCode = response.response!.statusCode
                switch statusCode {
                case 200:
                    if let verificationCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "VerificationCodeViewController") as? VerificationCodeViewController {
                        if let navigator = self.navigationController {
                            self.indicator.stopAnimating()
                            verificationCodeView.typeID = self.typeID
                            self.defaults.set(trimmedPhoneNumber, forKey: "PhoneNumber")
                            navigator.pushViewController(verificationCodeView, animated: true)
                        }
                    }
                    break
                case 403, 406, 409, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "toMain")
                    break
                default: break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "toMain")
                break
            }
        }
    }
    
    @IBAction func phoneEditDidEnd(_ sender: UITextField) {
    }
    
    @IBAction func phoneNumberChanged(_ sender: UITextField) {
        textField(sender, shouldChangeCharactersIn: NSMakeRange(0, (sender.text?.count)!), replacementString: " ")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneNumberLbl {
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            var flag = false
            if (length - index) > 3 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@ ", areaCode)
                index += 3
            }
            if length - index > 3 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@ ", prefix)
                index += 3
                flag = true
            }
            if length - index > 2 && flag == true {
                let suffix = decimalString.substring(with: NSMakeRange(index, 2))
                formattedString.appendFormat("%@ ", suffix)
                index += 2
            }
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            
            if textField.text?.count == 13 {
                continueButton.setEnable()
            } else {
                continueButton.setDisable()
            }
            
            return false
            
        } else {
            return true
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        if let navigator = self.navigationController {
            self.indicator.stopAnimating()
            HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
            navigator.popToRootViewController(animated: true)
        }
    }
}

