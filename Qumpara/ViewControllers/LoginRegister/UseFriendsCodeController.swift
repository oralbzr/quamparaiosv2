//
//  UseFriendsCodeController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 1/29/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

class UseFriendsCodeController: UIViewController {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var codeTextfield: UITextField!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator.center = view.center
        indicator.isHidden = true
        
        continueButton.setDisable()
        addToolBar(textField: codeTextfield)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
    }
    
    @IBAction func useCode(_ sender: UIButton) {
        codeTextfield.resignFirstResponder()
        addBlurLayer()
        view.addSubview(self.indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        
        let invitationCode = codeTextfield.text!
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "IniviationCode": invitationCode as AnyObject
        ]
        Alamofire.request(APIURL.InvitationCodeEntry.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let result = response.result
            
            switch result {
            case .success:
                let httpCode = response.response?.statusCode
                switch httpCode! {
                case 200:
                    self.showAlert(withTitle: "Başarılı", withMessage: "Referans Kampanyasına dahil oldunuz. Kampanyalardan herhangi birine katılımınızda biletiniz yüklenecektir.", withAction: "root")
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 403, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                    break
                default: break
                }
                if let navigator = self.navigationController {
                    HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                    navigator.popToRootViewController(animated: true)
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "")
                break
            }
        }
        self.indicator.stopAnimating()
        self.indicator.isHidden = true
        self.removeBlurLayer()
    }
    

    @IBAction func textChanged(_ sender: UITextField) {
        textField(sender, shouldChangeCharactersIn: NSMakeRange(0, (sender.text?.count)!), replacementString: "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField.text?.count == 10 {
            continueButton.setEnable()
        } else {
            continueButton.setDisable()
        }
        return newString.length <= maxLength
    }
    
    @IBAction func skipThisStep(_ sender: UIButton) {
        if let navigator = self.navigationController {
            HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
            navigator.popToRootViewController(animated: true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
