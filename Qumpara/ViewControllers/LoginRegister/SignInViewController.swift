//
//  SignInViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 10/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKLoginKit
import GoogleSignIn
import MRCountryPicker
import FirebaseInstanceID
import FirebaseMessaging

class SignInViewController: UIViewController, MRCountryPickerDelegate, GIDSignInDelegate, GIDSignInUIDelegate {
    
    var defaults = UserDefaults.standard
    
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UITextField!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var continueButton: UIButton!
    
    var deviceToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
                if let devToken = FIRInstanceID.instanceID().token() {
                    self.deviceToken = devToken
                }
            }
        }
        
        countryCodeLbl.layer.borderColor = UIColor.lightGray.cgColor
        countryCodeLbl.layer.borderWidth = 0.5
        
        countryCodeLbl.text = "+90"
        countryFlag.image = #imageLiteral(resourceName: "TR")
        phoneNumberLbl.keyboardType = UIKeyboardType.numberPad
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        
        addToolBar(textField: phoneNumberLbl)
        
        continueButton.setDisable()
        indicator.isHidden = true
        indicator.center = view.center
        countryPickerView.isHidden = true
        let tapAction = UITapGestureRecognizer(target: self, action: #selector(self.actionTapped(_:)))
        countryCodeLbl.isUserInteractionEnabled = true
        countryCodeLbl.addGestureRecognizer(tapAction)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
    }
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.countryCodeLbl.text = phoneCode
        self.countryFlag.image = flag
    }
    
    @objc func actionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        view.addSubview(countryPickerView)
        countryPickerView.isHidden = false
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setLocale("tr_TR")
        countryPicker.setCountryByName("Türkiye")
    }
    
    @IBAction func chooseCountry(_ sender: UIButton) {
        self.countryPickerView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func loginViaPhone(_ sender: UIButton) {
        phoneNumberLbl.resignFirstResponder()
        addBlurLayer()
        view.addSubview(self.indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let phoneNumber = self.phoneNumberLbl.text!
        let trimmedPhoneNumber = phoneNumber.replacingOccurrences(of: " ", with: "")
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let parameters: [String: AnyObject] = [
            "PhoneNumber" : trimmedPhoneNumber as AnyObject,
            "TypeId" : 0 as AnyObject
        ]
        
        Alamofire.request(APIURL.MemberLogin2.rawValue,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            let result = response.result
            
            switch result {
            case .success:
                
                let httpCode = response.response?.statusCode
                
                switch httpCode! {
                case 200:
                    if let verificationCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "VerificationCodeViewController") as? VerificationCodeViewController {
                        if let navigator = self.navigationController {
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.removeBlurLayer()
                            navigator.pushViewController(verificationCodeView, animated: true)
                            verificationCodeView.isLogin = true
                            self.defaults.set(trimmedPhoneNumber, forKey: "PhoneNumber")
                            self.defaults.set(0, forKey: "loginType")
                        }
                    }
                    break
                case 403, 404, 406, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                    break
                default: break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                break
            }
        }
    }
    
    @IBAction func loginViaGoogle(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            self.addBlurLayer()
            self.indicator.isHidden = false
            self.indicator.startAnimating()
            self.view.addSubview(self.indicator)
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
            
            let parameters: [String: AnyObject] = [
                "TypeId" : 2 as AnyObject,
                "Id": user.userID as AnyObject,
                "Name": user.profile.givenName as AnyObject,
                "SurName": user.profile.familyName as AnyObject,
                "Email": user.profile.email as AnyObject,
                "PhotoUrl": user.profile.imageURL(withDimension: 100).absoluteString as AnyObject
            ]
            
            print(user.userID)
            print(user.profile.givenName)
            print(user.profile.familyName)
            print(user.profile.email)
            print(user.profile.imageURL(withDimension: 100).absoluteString)
            
            Alamofire.request(APIURL.MemberLogin2.rawValue,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                debugPrint(response)
                
                let result = response.result
                
                switch result {
                case .success:
                    let statusCode = response.response?.statusCode
                    switch statusCode! {
                    case 200:
                        if let mainView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "MainViewController") as? MainViewController {
                            if let navigator = self.navigationController {
                                self.defaults.set(2, forKey: "loginType")
                                
                                if let data = result.value as? Dictionary<String, AnyObject> {
                                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                        
                                        let heads: HTTPHeaders = [
                                            "Content-Type" : "application/x-www-form-urlencoded",
                                            "Accept" : "application/json"
                                        ]
                                        let params: [String: AnyObject] = [
                                            "grant_type" : "password" as AnyObject,
                                            "username" : responseData["PhoneNumber"] as AnyObject,
                                            "password" : responseData["Password"] as AnyObject
                                        ]
                                        Alamofire.request(APIURL.getToken.rawValue, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: heads).responseJSON { response in
                                            debugPrint(response)
                                            
                                            let result = response.result
                                            let httpCode = response.response?.statusCode
                                            switch httpCode! {
                                            case 200:
                                                if let data = result.value as? Dictionary<String, AnyObject> {
                                                    self.defaults.set(data["access_token"]!, forKey: "access_token")
                                                    let headersUpdate: HTTPHeaders = [
                                                        "Authorization" : "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                                    ]
                                                    let parametersUpdate: [String: AnyObject] = [
                                                        "OperationSystem" : "IOS" as AnyObject,
                                                        "VersionName" : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
                                                    ]
                                                    Alamofire.request(APIURL.MemberVersionUpdate.rawValue, method: .post, parameters: parametersUpdate, encoding: JSONEncoding.default, headers: headersUpdate).responseJSON { response in
                                                        debugPrint(response)
                                                        
                                                        let result = response.result
                                                        switch result {
                                                        case .success:
                                                            let statusCode = response.response!.statusCode
                                                            switch statusCode {
                                                            case 200:
                                                                let headersRegistration : HTTPHeaders = [
                                                                    "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                                                ]
                                                                let parametersRegistration : [String: String] = [
                                                                    "RegistrationId": "\(self.deviceToken)"
                                                                ]
                                                                Alamofire.request(APIURL.UpdateRegistration.rawValue, method: .post, parameters: parametersRegistration, encoding: JSONEncoding.default, headers: headersRegistration).responseJSON { response in
                                                                    debugPrint(response)
                                                                    
                                                                    let result = response.result
                                                                    switch result {
                                                                    case .success:
                                                                        let statusCodeForRegistration = response.response!.statusCode
                                                                        switch statusCodeForRegistration {
                                                                        case 200:
                                                                            print("success!")
                                                                            let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?.first as! UINavigationController
                                                                            let storyboard = UIStoryboard(name: "Views", bundle: nil)
                                                                            let destinationViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
                                                                            navInTab.pushViewController(destinationViewController!, animated: true)
                                                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                                                                HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                                                navigator.popToRootViewController(animated: false)
                                                                                self.indicator.stopAnimating()
                                                                                self.indicator.isHidden = true
                                                                                self.removeBlurLayer()
                                                                            }
                                                                            break
                                                                        case 401:
                                                                            self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                                            break
                                                                        case 403, 404, 405, 406, 500:
                                                                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                                                                            break
                                                                        default: break
                                                                        }
                                                                        break
                                                                    case .failure:
                                                                        self.showAlert(withTitle: "Hata Kodu: \(statusCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                                                                        break
                                                                    }
                                                                }
                                                                break
                                                            case 401:
                                                                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                                break
                                                            case 403, 404, 405, 406, 500:
                                                                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                                                                break
                                                            default: break
                                                            }
                                                            break
                                                        case .failure:
                                                            self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                                                            break
                                                        }
                                                    }
                                                }
                                                break
                                            case 401:
                                                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                break
                                            case 404, 500:
                                                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                break
                                            default: break
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break
                    case 403, 404, 406, 500:
                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                        break
                    default:break
                    }
                    break
                case .failure:
                    self.showAlert(withTitle: "Uyarı", withMessage: "Şu anda işleminizi gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "toMain")
                    break
                }
            }
        } else {
            print(error)
            
            self.indicator.stopAnimating()
        }
    }
    @IBAction func phoneEditDidEnd(_ sender: UITextField) {
        if sender.text?.count == 13 {
            continueButton.setEnable()
        }
    }
    
    @IBAction func phoneNumberChanged(_ sender: UITextField) {
        textField(sender, shouldChangeCharactersIn: NSMakeRange(0, (sender.text?.count)!), replacementString: " ")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneNumberLbl {
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            var flag = false
            if (length - index) > 3 {
                let areaCode = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@ ", areaCode)
                index += 3
            }
            if length - index > 3 {
                let prefix = decimalString.substring(with: NSMakeRange(index, 3))
                formattedString.appendFormat("%@ ", prefix)
                index += 3
                flag = true
            }
            if length - index > 2 && flag == true {
                let suffix = decimalString.substring(with: NSMakeRange(index, 2))
                formattedString.appendFormat("%@ ", suffix)
                index += 2
            }
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            if textField.text?.count == 13 {
                continueButton.setEnable()
            } else {
                continueButton.setDisable()
            }
            return false
            
        } else {
            return true
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.indicator.stopAnimating()
        self.navigationController?.popViewController(animated: true)
//        let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?.first as! UINavigationController
//        let storyboard = UIStoryboard(name: "Views", bundle: nil)
//        let destinationViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
//        navInTab.pushViewController(destinationViewController!, animated: true)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
//            HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
//        }
    }
    
    @IBAction func loginViaFacebook(_ sender: UIButton) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: [], from: self) { (result, error) -> Void in
            if (error == nil){
                if (result?.isCancelled)!{
                    let alert = UIAlertController(title: "Uyarı", message: "Giriş yapmadınız.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?.first as! UINavigationController
                    let storyboard = UIStoryboard(name: "Views", bundle: nil)
                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
                    navInTab.pushViewController(destinationViewController!, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                        HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                    }
                    return
                } else {
                    let accessToken = FBSDKAccessToken.current()
                    
                    if accessToken != nil {
                        self.indicator.isHidden = false
                        self.indicator.startAnimating()
                        
                        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, first_name, last_name, picture.type(large), birthday, email, gender, location, relationship_status, significant_other, family, friends, likes, education, religion"]).start(completionHandler: { (connection, result, error) -> Void in
                            
                            let dictionary = result as! Dictionary<String, Any>
                            
                            if let name = dictionary["first_name"] as? String {
                                if let surname = dictionary["last_name"] as? String {
                                    if let email = dictionary["email"] as? String {
                                        if let id = dictionary["id"] as? String {
                                            if let gender = dictionary["gender"] as? String {
                                                if let picture = dictionary["picture"] as? Dictionary<String, AnyObject> {
                                                    if let picData = picture["data"] as? Dictionary<String, AnyObject> {
                                                        
                                                        let headers: HTTPHeaders = [
                                                            "Content-Type": "application/json",
                                                            "Accept": "application/json"
                                                        ]
                                                        let parameters: [String: AnyObject] = [
                                                            "TypeId" : 1 as AnyObject,
                                                            "Name": name as AnyObject,
                                                            "SurName": surname as AnyObject,
                                                            "Email": email as AnyObject,
                                                            "Id": id as AnyObject,
                                                            "Gender": gender as AnyObject,
                                                            "PhotoUrl": picData["url"] as AnyObject
                                                        ]
                                                        
                                                        print(parameters)
                                                        
                                                        Alamofire.request(APIURL.MemberLogin2.rawValue,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                                                            debugPrint(response)
                                                            
                                                            let result = response.result
                                                            
                                                            
                                                            switch result {
                                                            case .success:
                                                                let statusCode = response.response!.statusCode
                                                                switch statusCode {
                                                                case 200:
                                                                    if let mainView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "MainViewController") as? MainViewController {
                                                                        if let navigator = self.navigationController {
                                                                            self.indicator.stopAnimating()
                                                                            self.defaults.set(1, forKey: "loginType")
                                                                            
                                                                            if let data = result.value as? Dictionary<String, AnyObject> {
                                                                                if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                                                                    
                                                                                    let heads: HTTPHeaders = [
                                                                                        "Content-Type" : "application/x-www-form-urlencoded",
                                                                                        "Accept" : "application/json"
                                                                                    ]
                                                                                    let params: [String: AnyObject] = [
                                                                                        "grant_type" : "password" as AnyObject,
                                                                                        "username" : responseData["PhoneNumber"] as AnyObject,
                                                                                        "password" : responseData["Password"] as AnyObject
                                                                                    ]
                                                                                    Alamofire.request(APIURL.getToken.rawValue, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: heads).responseJSON { response in
                                                                                        let result = response.result
                                                                                        let httpCode = response.response?.statusCode
                                                                                        switch httpCode! {
                                                                                        case 200:
                                                                                            if let data = result.value as? Dictionary<String, AnyObject> {
                                                                                                self.defaults.set(data["access_token"]!, forKey: "access_token")
                                                                                                let headersUpdate: HTTPHeaders = [
                                                                                                    "Authorization" : "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                                                                                ]
                                                                                                let parametersUpdate: [String: AnyObject] = [
                                                                                                    "OperationSystem" : "IOS" as AnyObject,
                                                                                                    "VersionName" : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
                                                                                                ]
                                                                                                Alamofire.request(APIURL.MemberVersionUpdate.rawValue, method: .post, parameters: parametersUpdate, encoding: JSONEncoding.default, headers: headersUpdate).responseJSON { response in
                                                                                                    debugPrint(response)
                                                                                                    let result = response.result
                                                                                                    switch result {
                                                                                                    case .success:
                                                                                                        let statusCode = response.response!.statusCode
                                                                                                        switch statusCode {
                                                                                                        case 200:
                                                                                                            let headersRegistration : HTTPHeaders = [
                                                                                                                "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                                                                                                            ]
                                                                                                            let parametersRegistration : [String: String] = [
                                                                                                                "RegistrationId": "\(self.deviceToken)"
                                                                                                            ]
                                                                                                            Alamofire.request(APIURL.UpdateRegistration.rawValue, method: .post, parameters: parametersRegistration, encoding: JSONEncoding.default, headers: headersRegistration).responseJSON { response in
                                                                                                                debugPrint(response)
                                                                                                                
                                                                                                                let result = response.result
                                                                                                                switch result {
                                                                                                                case .success:
                                                                                                                    let statusCodeForRegistration = response.response!.statusCode
                                                                                                                    switch statusCodeForRegistration {
                                                                                                                    case 200:
                                                                                                                        print("success!")
                                                                                                                        let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?.first as! UINavigationController
                                                                                                                        let storyboard = UIStoryboard(name: "Views", bundle: nil)
                                                                                                                        let destinationViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
                                                                                                                        navInTab.pushViewController(destinationViewController!, animated: true)
                                                                                                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                                                                                                                            HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                                                                                                                            navigator.popToRootViewController(animated: false)
                                                                                                                        }
                                                                                                                        break
                                                                                                                    case 401:
                                                                                                                        self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                                                                                        break
                                                                                                                    case 403, 404, 405, 406, 500:
                                                                                                                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                                                                                                                        break
                                                                                                                    default: break
                                                                                                                    }
                                                                                                                    break
                                                                                                                case .failure:
                                                                                                                    self.showAlert(withTitle: "Hata Kodu: \(statusCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                                                                                                                    break
                                                                                                                }
                                                                                                            }
                                                                                                            break
                                                                                                        case 401:
                                                                                                            self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                                                                                            break
                                                                                                        case 403, 404, 405, 406, 500:
                                                                                                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                                                                                                            break
                                                                                                        default: break
                                                                                                        }
                                                                                                        break
                                                                                                    case .failure:
                                                                                                        self.showAlert(withTitle: "Hata Kodu: \(statusCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                                                                                                        break
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            break
                                                                                        case 404,500:
                                                                                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                                                            break
                                                                                        default: break
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    break
                                                                case 403, 404, 406, 500:
                                                                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                                                    self.indicator.isHidden = true
                                                                    break
                                                                default: break
                                                                }
                                                                break
                                                            case .failure:
                                                                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "")
                                                                break
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }
    }
}
