//
//  MemberInfoViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 1/24/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

struct City {
    var id: Int = 0
    var name: String = ""
}

struct County {
    var id: Int = 0
    var name: String = ""
}

class MemberInfoViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var surnameTextfield: UITextField!
    @IBOutlet weak var genderMaleBtn: UIButton!
    @IBOutlet weak var genderFemaleBtn: UIButton!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var birthdateLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var countyLbl: UILabel!
    
    // picker views
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var countyView: UIView!
    
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var citypicker: UIPickerView!
    @IBOutlet weak var countypicker: UIPickerView!
    
    @IBOutlet weak var continueButton: UIButton!
    
    var blurEffectView = UIVisualEffectView()
    
    var cityData: [City] = []
    var countyData: [County] = []
    
    var cityId = 0
    var countyId = 0
    var gender = 0
    var genderCount = 0
    
    var tempDict = Dictionary<String, AnyObject>()
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addToolBar(textField: nameTextfield)
        addToolBar(textField: surnameTextfield)
        addToolBar(textField: emailTextfield)
        
        dateView.isHidden = true
        cityView.isHidden = true
        countyView.isHidden = true
        indicator.isHidden = true
        
        // keyboard types
        nameTextfield.keyboardType = UIKeyboardType.namePhonePad
        surnameTextfield.keyboardType = UIKeyboardType.namePhonePad
        emailTextfield.keyboardType = UIKeyboardType.emailAddress
        
        // label borders
        birthdateLbl.layer.borderColor = UIColor.lightGray.cgColor
        birthdateLbl.layer.borderWidth = 0.5
        cityLbl.layer.borderColor = UIColor.lightGray.cgColor
        cityLbl.layer.borderWidth = 0.5
        countyLbl.layer.borderColor = UIColor.lightGray.cgColor
        countyLbl.layer.borderWidth = 0.5
        
        // label tap actions
        let birthdateTapAction = UITapGestureRecognizer(target: self, action: #selector(self.birthdateActionTapped(_:)))
        birthdateLbl.isUserInteractionEnabled = true
        birthdateLbl.addGestureRecognizer(birthdateTapAction)
        
        let cityTapAction = UITapGestureRecognizer(target: self, action: #selector(self.cityActionTapped(_:)))
        cityLbl.isUserInteractionEnabled = true
        cityLbl.addGestureRecognizer(cityTapAction)
        
        let countyTapAction = UITapGestureRecognizer(target: self, action: #selector(self.countyActionTapped(_:)))
        countyLbl.isUserInteractionEnabled = true
        countyLbl.addGestureRecognizer(countyTapAction)
        
        getCities()
        
        citypicker.dataSource = self
        citypicker.delegate = self
        
        countypicker.dataSource = self
        countypicker.delegate = self
        
        continueButton.setDisable()
        // Do any additional setup after loading the view.
    }
    
    func getCities() {
        Alamofire.request(APIURL.GetCities.rawValue).responseJSON { response in
            let result = response.result
            
            switch result {
            case .success:
                if let data = result.value as? Dictionary<String, AnyObject> {
                    if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                        for i in 0...responseData.count - 1 {
                            var city = City()
                            city.id = responseData[i]["CityId"] as! Int
                            city.name = responseData[i]["CityName"] as! String
                            self.cityData.append(city)
                        }
                        self.citypicker.reloadAllComponents()
                    }
                }
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    func getCounties(_ cityId: Int) {
        let heads: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let params: [String: AnyObject] = [
            "CityId" : cityId as AnyObject
        ]
        
        Alamofire.request(APIURL.GetCounties.rawValue, method: .post, parameters: params, encoding: JSONEncoding.default, headers: heads).responseJSON { response in
            let result = response.result
            
            self.countyData = []
            switch result {
            case .success:
                if let data = result.value as? Dictionary<String, AnyObject> {
                    if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                        for i in 0...responseData.count - 1 {
                            var county = County()
                            county.id = responseData[i]["CountyId"] as! Int
                            county.name = responseData[i]["CountyName"] as! String
                            self.countyData.append(county)
                        }
                        self.countypicker.reloadAllComponents()
                    }
                }
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
    }
    
    @IBAction func genderMaleTapped(_ sender: UIButton) {
        if genderCount == 0 {
            sender.isSelected = true
            genderCount = 1
            gender = 1
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                genderCount = 0
                gender = 0
            } else {
                sender.isSelected = true
                genderFemaleBtn.isSelected = false
                genderCount = 1
                gender = 1
            }
        }
    }
    
    @IBAction func genderFemaleTapped(_ sender: UIButton) {
        if genderCount == 0 {
            sender.isSelected = true
            genderCount = 1
            gender = 2
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                genderCount = 0
                gender = 0
            } else {
                sender.isSelected = true
                genderMaleBtn.isSelected = false
                genderCount = 1
                gender = 2
            }
        }
    }
    
    @objc func birthdateActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        dateView.isHidden = false
        view.addSubview(dateView)
    }
    
    @IBAction func chooseBirthdate(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        birthdateLbl.text = dateFormatter.string(from: datepicker.date)
        dateView.isHidden = true
        removeBlurLayer()
        controlFields(continueButton)
    }
    
    @IBAction func cancelBirthdate(_ sender: UIButton) {
        dateView.isHidden = true
        removeBlurLayer()
    }
    
    @objc func cityActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        self.cityView.isHidden = false
        self.view.addSubview(self.cityView)
    }
    
    @IBAction func chooseCity(_ sender: UIButton) {
        countyLbl.text = ""
        cityView.isHidden = true
        removeBlurLayer()
        controlFields(continueButton)
    }
    
    @IBAction func cancelCity(_ sender: UIButton) {
        if let city = tempDict["City"] as? String {
            self.cityLbl.text = city
            if let cityid = tempDict["CityId"] as? Int {
                self.cityId = cityid
                self.getCounties(cityid)
            }
        }
        cityView.isHidden = true
        removeBlurLayer()
    }
    
    @objc func countyActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        self.countyView.isHidden = false
        self.view.addSubview(self.countyView)
    }
    
    @IBAction func chooseCounty(_ sender: UIButton) {
        countyView.isHidden = true
        removeBlurLayer()
        controlFields(continueButton)
    }
    
    @IBAction func cancelCounty(_ sender: UIButton) {
        if let county = tempDict["County"] as? String {
            self.countyLbl.text = county
            if let countyid = tempDict["CountyId"] as? Int {
                self.countyId = countyid
            }
        }
        countyView.isHidden = true
        removeBlurLayer()
    }
    
    func controlFields(_ button: UIButton) {
        if nameTextfield.text != "" && surnameTextfield.text != "" && genderCount == 1 && (emailTextfield.text?.isEmail)! && birthdateLbl.text != "" && cityLbl.text != "" && countyLbl.text != "" {
            button.setEnable()
        }
    }
    
    @IBAction func continueButton(_ sender: Any) {
        nameTextfield.resignFirstResponder()
        surnameTextfield.resignFirstResponder()
        emailTextfield.resignFirstResponder()
        addBlurLayer()
        view.addSubview(self.indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "Name": nameTextfield.text! as AnyObject,
            "Surname": surnameTextfield.text! as AnyObject,
            "email": emailTextfield.text as AnyObject,
            "gender": gender as AnyObject,
            "birthDate": birthdateLbl.text!.dateToStandard as AnyObject,
            "City": cityId as AnyObject,
            "County": countyId as AnyObject
        ]
        
        self.indicator.isHidden = false
        self.indicator.startAnimating()
        
        if let useFriendsCodeView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "UseFriendsCodeController") as? UseFriendsCodeController {
            if let navigator = self.navigationController {
                
                let memberID = self.defaults.object(forKey: "MemberId")!
                Alamofire.request("\(APIURL.MemberNewSave.rawValue)/\(memberID)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    debugPrint(response)
                    
                    let result = response.result
                    
                    switch result {
                    case .success:
                        print("success")
                        
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        navigator.pushViewController(useFriendsCodeView, animated: true)
                        break
                    case .failure:
                        let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        break
                    }
                }
            }
        }
        self.indicator.stopAnimating()
        self.indicator.isHidden = true
        self.removeBlurLayer()
    }
    
    func numberOfComponents(in cityPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == citypicker{
            return cityData.count
        } else {
            return countyData.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == citypicker {
            return cityData[row].name
        } else {
            return countyData[row].name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == citypicker {
            getCounties(cityData[row].id)
            cityId = cityData[row].id
            self.cityLbl.text = cityData[row].name
            tempDict["City"] = cityData[row].name as AnyObject
        } else {
            countyId = countyData[row].id
            self.countyLbl.text = countyData[row].name
            tempDict["County"] = countyData[row].name as AnyObject
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func emailEditDidEnd(_ sender: UITextField) {
        controlFields(continueButton)
    }
    @IBAction func surnameEditDidEnd(_ sender: UITextField) {
        controlFields(continueButton)
    }
    @IBAction func nameEditDidEnd(_ sender: UITextField) {
        controlFields(continueButton)
    }
}



extension String {
    public var isEmail: Bool {
        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let firstMatch = dataDetector?.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSRange(location: 0, length: length))
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    
    public var length: Int {
        return self.characters.count
    }
    
    public var isValidPlate: Bool {
//        let regex = "^[0-9]{2}[A-Z]{1,4}[0-9]{1,6}$"
//        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: self)
        return true
    }
    
    public var dateToStandard: String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/yyyy"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatterGet.date(from: self)!
        return dateFormatter.string(from: date);
    }
    
    public var standardToDate: String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatterGet.date(from: self)!
        return dateFormatter.string(from: date);
    }
    
    public var timeToDate: String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatterGet.date(from: self)!
        return dateFormatter.string(from: date);
    }
}
