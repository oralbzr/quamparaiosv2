//
//  QumparaCardViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/27/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

class QumparaCardViewController: UIViewController {
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    @IBOutlet weak var tcno: UITextField!
    @IBOutlet weak var birthdateLbl: UILabel!
    @IBOutlet weak var maidenName: UITextField!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var qumparacard: UIButton!
    @IBOutlet weak var warningView: UIView!
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var handlingScroolView: UIView!
    
    @IBOutlet weak var webView: UIView!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var headlineLbl: UILabel!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator.center = view.center
        indicator.isHidden = true
        
        webView.isHidden = true
        dateView.isHidden = true
        warningView.isHidden = true
        handlingScroolView.isHidden = true
        
        //borders
        birthdateLbl.layer.borderColor = UIColor.lightGray.cgColor
        birthdateLbl.layer.borderWidth = 0.5
        qumparacard.layer.borderColor = UIColor.lightGray.cgColor
        qumparacard.layer.borderWidth = 0.5
        
        // label tap actions
        let birthdateTapAction = UITapGestureRecognizer(target: self, action: #selector(self.birthdateActionTapped(_:)))
        birthdateLbl.isUserInteractionEnabled = true
        birthdateLbl.addGestureRecognizer(birthdateTapAction)
        
        // toolbars to text fields
        addToolBar(textField: name)
        addToolBar(textField: surname)
        addToolBar(textField: tcno)
        addToolBar(textField: maidenName)
        addToolBar(textField: email)
        
        // keyboard types
        name.keyboardType = .alphabet
        surname.keyboardType = .alphabet
        tcno.keyboardType = .numberPad
        maidenName.keyboardType = .alphabet
        email.keyboardType = .alphabet
        
        getUserInfo()
        maidenName.delegate = self
        email.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //
        HHTabBarView.shared.isHidden = true
    }
    
    func getUserInfo() {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)",
            "Accept": "application/json",
            "Content-Type": "application/json"
        ]
        Alamofire.request(APIURL.GetMember.rawValue, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let result = response.result
            let httpCode = response.response?.statusCode
            switch httpCode! {
            case 200:
                if let data = response.result.value as? Dictionary<String, AnyObject> {
                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                        if let name = responseData["Name"] as? String {
                            self.name.text = name
                        }
                        if let surname = responseData["Surname"] as? String {
                            self.surname.text = surname
                        }
                        if let birthdate = responseData["BirthDate"] as? String {
                            self.birthdateLbl.text = birthdate.standardToDate
                        }
                    }
                }
                break
            case 401:
                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                break
            case 403, 500:
                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                break
            default: break
            }
        }
    }
    
    @objc func birthdateActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        dateView.isHidden = false
        view.addSubview(dateView)
    }
    
    @IBAction func chooseBirthdate(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        birthdateLbl.text = dateFormatter.string(from: datepicker.date)
        dateView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelBirthdate(_ sender: UIButton) {
        dateView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func whatIsQumparaCard(_ sender: UIButton) {
        loadWebView(withTitle: "Qumpara Kart Nedir?", withURL: "https://www.fisicek.com/web/QumparaCard.htm")
    }
    
    @IBAction func saveCard(_ sender: UIButton) {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)",
            "Accept": "application/json",
            "Content-Type": "application/json"
        ]
        let parameters: [String: AnyObject] = [
            "name": self.name.text! as AnyObject,
            "surname": self.surname.text! as AnyObject,
            "email": self.email.text! as AnyObject,
            "tcIdentificationNumber": self.tcno.text! as AnyObject,
            "birthDate": self.birthdateLbl.text!.dateToStandard as AnyObject,
            "motherMaidenName": self.maidenName.text! as AnyObject
        ]
        print("\(self.name.text!)-\(self.surname.text!)-\(self.tcno.text!)-\(self.birthdateLbl.text!)-\(self.maidenName.text!)")
        Alamofire.request(APIURL.QumparamCardMemberSave.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let result = response.result
            let httpCode = response.response?.statusCode
            switch httpCode! {
            case 200:
                if let otpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "QumparamOTPViewController") as? QumparamOTPViewController {
                    if let navigator = self.navigationController {
                        let headers: HTTPHeaders = [
                            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                        ]
                        Alamofire.request(APIURL.CreateUserByQumparamCard.rawValue, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                            let httpCode = response.response?.statusCode
                            let result = response.result
                            switch httpCode! {
                            case 200:
                                if let data = response.result.value as? Dictionary<String, AnyObject> {
                                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                        if let smsToken = responseData["SmsToken"] as? String {
                                            self.defaults.set(smsToken, forKey: "sms_token")
                                        }
                                    }
                                }
                                navigator.pushViewController(otpView, animated: true)
                                break
                            case 401:
                                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                break
                            case 403, 404, 405, 500:
                                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "root")
                                break
                            default: break
                            }
                        }
                    }
                }
                break
            case 401:
                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                break
            case 403, 500:
                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                break
            default: break
            }
        }
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        webView.isHidden = true
    }
    
    @IBAction func editDidBegin(_ sender: UITextField) {
        animateViewMoving(up: true, moveValue: self.handlingScroolView.frame.height)
    }
    
    @IBAction func editDidEnd(_ sender: UITextField) {
        animateViewMoving(up: false, moveValue: self.handlingScroolView.frame.height)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func emailEditBegan(_ sender: UITextField) {
        animateViewMoving(up: true, moveValue: self.handlingScroolView.frame.height)
    }
    
    @IBAction func emailEditEnd(_ sender: UITextField) {
        animateViewMoving(up: false, moveValue: self.handlingScroolView.frame.height)
    }
    
    @IBAction func tcnoChanged(_ sender: UITextField) {
        textField(sender, shouldChangeCharactersIn: NSMakeRange(0, (sender.text?.count)!), replacementString: "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 11
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField.text?.count == 11 {
            saveBtn.setEnable()
        } else {
            saveBtn.setDisable()
        }
        return newString.length <= maxLength
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    func loadWebView(withTitle title: String, withURL url: String) {
        let myURL = URL(string: url)
        let myRequest = URLRequest(url: myURL!)
        webview.loadRequest(myRequest)
        headlineLbl.text = title
        webView.isHidden = false
    }
}


