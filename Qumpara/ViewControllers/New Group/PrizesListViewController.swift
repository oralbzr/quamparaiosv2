//
//  PrizesListViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 24/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
let prizeReuseIdentifier = "PrizeCellIdentifer";

class PrizesListViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var lblUserAccount: UILabel!
    @IBOutlet weak var prizeCollection: UICollectionView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var emptyListMessage: UILabel!
    var prizes:[Prize] = []
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.indicator.center=self.view.center
       
        getPrizeList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HHTabBarView.shared.isHidden = true
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
       
    }
    
    func getPrizeList(){
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
            }
        }else{
            if Reachability.isInternetAvailable() {
                self.indicator.show()
                getPrizeInfo()
                
            }else{
                let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getPrizeInfo(){
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        
        Alamofire.request(APIURL.GetPrizeListInformation.rawValue ,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            self.indicator.hide()
            switch result {
            case .success:
                print(result)
                print(result.value as Any)
                if let data = result.value as? Dictionary<String, AnyObject> {
                    
                    if let remainingMoney = data["RemainingMoney"] as? NSString{
                        
                        self.lblUserAccount.text = String(format: "Bakiyeniz: %@ TL", remainingMoney)
                    }
                    
                    if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                        if responseData.count > 0 {
                            for i in 0...responseData.count - 1 {
                                let prize = Prize()
                               
                                if let PrizeId = responseData[i]["CampaignId"] as? NSNumber {
                                    prize.prizeId = PrizeId.stringValue
                                }
                                if let ExpirationDate = responseData[i]["CouponExpirationDate"] as? String {
                                    prize.expirationDate = ExpirationDate
                                }
                                if let PrizeTitle =  responseData[i]["Title"] as? String {
                                    prize.prizeTitle = PrizeTitle
                                }
                                if let PrizeDescription =  responseData[i]["CouponDescription"] as? String {
                                    prize.prizeDescription = PrizeDescription
                                }
                                if let PrizeImageUrl =  responseData[i]["CouponImageUrl"] as? String {
                                    prize.prizeImageUrl = PrizeImageUrl
                                }
                                if let PrizeValue =  responseData[i]["PrizeValue"] as? NSNumber {
                                    prize.prizeValue = PrizeValue
                                }
                                self.prizes.append(prize)
                                
                            }
                            if (self.prizes.count == 0) {
                                self.emptyListMessage.isHidden = false
                            }
                            self.prizeCollection.delegate=self
                            self.prizeCollection.dataSource=self
                            self.prizeCollection.reloadData()
                        }
                        
                    }
                }
                
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.prizes.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: prizeReuseIdentifier, for: indexPath) as! BrandCollectionViewCell
        
        let prize = self.prizes[indexPath.row] as Prize
        
        cell.brandImage.sd_setImage(with: URL(string: prize.prizeImageUrl), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(),
                                    completed: {(image,error,cacheType,url) in
                                        if error != nil {
                                            print(error!)
                                            cell.brandImage.image = UIImage(named: "")
                                        }
        })
        
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let selectedPrize = self.prizes[indexPath.row] as Prize
        print(selectedPrize.prizeId)
        if let prizeDetailView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "PrizeDetailViewController") as? PrizeDetailViewController {
            if let navigator = self.navigationController {
                prizeDetailView.selectedPrize=selectedPrize
                prizeDetailView.titleText = self.lblUserAccount.text
                navigator.pushViewController(prizeDetailView, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return Utility .size(CGSize(width: 90, height: 90))
        
    }
    @IBAction func closeView(_ sender: AnyObject) {
        if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
