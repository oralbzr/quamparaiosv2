//
//  BrandCollectionViewCell.swift
//  Qumpara
//
//  Created by Oral Bozkır on 21/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class BrandCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var brandImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        brandImage.layer.borderColor = UIColor.darkGray.cgColor
        brandImage.layer.borderWidth = 1.0
        
        frame = Utility.rect(frame)
    }
    
    override func prepareForReuse() {
        self.brandImage.image = UIImage(named: "")
        
    }
}
