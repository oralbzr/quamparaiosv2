//
//  CouponListViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 22/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class CouponListViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
   
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var CouponTable: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var segmentView: UISegmentedControl!
    var unusedCoupons:[Coupon] = []
    var usedCoupons:[Coupon] = []
    var brandId: NSNumber!
    var titleText: String!
    var isCheckBox: Bool!
    let defaults = UserDefaults.standard
    var selectedCoupon : Coupon = Coupon()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.indicator.center=self.view.center
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear( animated)
        self.indicator.center=self.view.center
        getBrandCouponList()
        if !titleText.isEmpty {
            self.lblTitle.text=titleText
        }
         segmentView.isHidden = true
        if (isCheckBox == true){
            segmentView.isHidden = false
            self.CouponTable.frame = CGRect(x: self.CouponTable.frame.origin.x, y: CGFloat(Utility .scaledY(168.0)) , width:  self.CouponTable.frame.size.width, height:CGFloat( Utility .scaledY(400)))
        }else{
          
            self.CouponTable.frame = CGRect(x: self.CouponTable.frame.origin.x, y: CGFloat(Utility .scaledY(112.0)) , width:  self.CouponTable.frame.size.width, height:CGFloat( Utility .scaledY(456.0)))
        }
        self.CouponTable.allowsSelection = true
        // Do any additional setup after loading the view.
    }
    
    
    
    func getBrandCouponList(){
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
                 HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
            }
        }else{
            if Reachability.isInternetAvailable() {
                self.indicator.show()
                self.usedCoupons.removeAll()
                self.unusedCoupons.removeAll()
                getBrandCouponInfo()
                
            }else{
                let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getBrandCouponInfo(){
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        
        let parameters: [String: AnyObject] = [
            "PrizeBrandId" : brandId as AnyObject,
        ]
        
        Alamofire.request(APIURL.BrandCoupon.rawValue ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            self.indicator.hide()
            switch result {
            case .success:
                print(result)
                print(result.value as Any)
                if let data = result.value as? Dictionary<String, AnyObject> {
                    
                    if let description = data["description"] as? String{
                        
                    }
                    
                    if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                        if responseData.count > 0 {
                            for i in 0...responseData.count - 1 {
                                let coupon = Coupon()
                                if let CouponId = responseData[i]["CouponId"] as? NSNumber {
                                    coupon.couponId = CouponId.stringValue
                                }
                                if let WonDate = responseData[i]["WonDate"] as? String {
                                    coupon.wonDate = WonDate
                                }
                                if let CouponName = responseData[i]["CouponName"] as? String {
                                   // let trimmedString = CouponName.trimmingCharacters(in: .whitespaces)
                                    coupon.couponName = CouponName
                                   // let result = trimmedString.split(separator: ":")
                                   // print(result);
                                }
                                if let IsUsed = responseData[i]["IsUsed"] as? Bool {
                                    coupon.isUsed = IsUsed
                                }
                                if coupon.isUsed == true{
                                    self.usedCoupons.append(coupon)
                                }else{
                                    self.unusedCoupons.append(coupon)
                                }
                                if (self.unusedCoupons.count == 0){
                                    self.segmentView.selectedSegmentIndex = 0
                                }else if (self.usedCoupons.count == 0){
                                    self.segmentView.selectedSegmentIndex = 1
                                }else{
                                    self.segmentView.selectedSegmentIndex = 1
                                }
                            }
                            self.CouponTable.isHidden=false
                            self.CouponTable.delegate=self
                            self.CouponTable.dataSource=self
                            self.CouponTable.reloadData()
                        }
                    }
                }
                
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentView.selectedSegmentIndex == 0 {
          return  self.usedCoupons.count
        }
       return self.unusedCoupons.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CouponTable.dequeueReusableCell(withIdentifier: "CouponIdentifier", for: indexPath) as! CouponCell
         if segmentView.selectedSegmentIndex == 0 {
        let coupon = self.usedCoupons[indexPath.row] as Coupon
        cell.configureCell(dateTxt: coupon.wonDate, noTxt: coupon.couponName, noPass: "")
         }else{
            let coupon = self.unusedCoupons[indexPath.row] as Coupon
            cell.configureCell(dateTxt: coupon.wonDate, noTxt: coupon.couponName, noPass: "")
        }
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         if segmentView.selectedSegmentIndex == 0 {
            if self.usedCoupons.count > 0{
            selectedCoupon = self.usedCoupons[indexPath.row] as Coupon
            }
         }else{
            if self.unusedCoupons.count > 0{
              selectedCoupon = self.unusedCoupons[indexPath.row] as Coupon
            }
        }
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
            }
             HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
        }else{
                if let CouponDetail = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "CouponDetailViewController") as? CouponDetailViewController {
                    if let navigator = self.navigationController {
                        CouponDetail.couponId=selectedCoupon.couponId
                        if !titleText.isEmpty {
                            CouponDetail.titleLblTxt = titleText
                        }
                        CouponDetail.isCheckBox = isCheckBox
                        navigator.pushViewController(CouponDetail, animated: true)
                    }
                }
            }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return CGFloat(Utility.scaledHeight(70))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HHTabBarView.shared.isHidden = true
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func closeView(_ sender: AnyObject) {
        if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    @IBAction func segmentedControlValueChanged(segment: UISegmentedControl) {
        self.CouponTable.reloadData()
    
    }

  

}
