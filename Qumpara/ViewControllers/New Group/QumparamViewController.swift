//
//  QumparamViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 10/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
let reuseIdentifier = "CellIdentifer";

class QumparamViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var convertToAward: UIButton!
    @IBOutlet weak var sendToCard: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAccount: UILabel!
    @IBOutlet weak var couponCollection: UICollectionView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet var userImage: UIImageView!
    
    var coupons:[Brand] = []
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        self.indicator.center=self.view.center
        convertToAward.layer.borderColor = UIColor.white.cgColor
        sendToCard.layer.borderColor = UIColor.white.cgColor
        self.view.isUserInteractionEnabled=true
        userImage.layer.borderWidth = 0.5
        userImage.layer.masksToBounds = false
        userImage.layer.borderColor = UIColor.lightGray.cgColor
        userImage.layer.cornerRadius = userImage.frame.height/2
        userImage.clipsToBounds = true
        
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HHTabBarView.shared.isHidden = false
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = false
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }else{
            getCouponList()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getCouponList(){
        if Reachability.isInternetAvailable() {
            self.indicator.show()
            self.coupons.removeAll()
            getQumparamInfo()
            
        }else{
            let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func getQumparamInfo(){
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        
        Alamofire.request(APIURL.Qumparam.rawValue ,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            self.indicator.hide()
            switch result {
            case .success:
                print(result)
                print(result.value as Any)
                if let data = result.value as? Dictionary<String, AnyObject> {
                    
                    if let remainingMoney = data["RemainingMoney"] as? String {
                        self.lblUserAccount.text = String(format: "%@ TL", (remainingMoney))
                    }
                    if let name = data["Name"] as? String {
                        self.lblUserName.text = String(format: "%@", name)
                        if let surname = data["Surname"] as? String{
                            self.lblUserName.text = String(format: "%@ %@", name,surname)
                        }
                    }
                    if let imageUrl = data["ProfilImage"] as? String {
                        self.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(),
                                                   completed: {(image,error,cacheType,url) in
                                                    if error != nil {
                                                        print(error!)
                                                        self.userImage.image = #imageLiteral(resourceName: "user")
                                                    }
                        })
                    } else {
                        self.userImage.image = #imageLiteral(resourceName: "user")
                    }
                    
                    if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                        if responseData.count > 0 {
                            for i in 0...responseData.count - 1 {
                                let coupon = Brand()
                                if let brandId = responseData[i]["BrandId"] as? NSNumber {
                                    coupon.BrandId = brandId
                                }
                                if let brandPicture = responseData[i]["BrandPicture"] as? String {
                                    coupon.BrandPicture = brandPicture
                                }
                                if let brandName = responseData[i]["BrandName"] as? String {
                                    coupon.BrandName = brandName
                                }
                                if let isCheckBox = responseData[i]["IsCheckBox"] as? Bool {
                                    coupon.IsCheckBox = isCheckBox
                                }
                                
                                self.coupons.append(coupon)
                                self.couponCollection.delegate=self
                                self.couponCollection.dataSource=self
                                self.couponCollection.reloadData()
                            }
                        }
                    }
                }
                
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.coupons.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! BrandCollectionViewCell
        
        let coupon = self.coupons[indexPath.row] as Brand
        
        cell.brandImage.sd_setImage(with: URL(string: coupon.BrandPicture), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(),
                                    completed: {(image,error,cacheType,url) in
                                        if error != nil {
                                            print(error!)
                                            cell.brandImage.image = UIImage(named: "")
                                        }
        })
        
        print(self.coupons.count)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let coupon = self.coupons[indexPath.row] as Brand
        print(coupon.BrandName)
        if let couponListView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "CouponListViewController") as? CouponListViewController {
            if let navigator = self.navigationController {
                couponListView.brandId=coupon.BrandId
                couponListView.titleText=coupon.BrandName
                couponListView.isCheckBox = coupon.IsCheckBox
                navigator.pushViewController(couponListView, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return Utility .size(CGSize(width: 90, height: 90))
        
    }
    
    @IBAction func sendToCardData(_ sender: AnyObject) {
        self.addBlurLayer()
        self.view.addSubview(self.indicator)
        self.indicator.isHidden = false
        self.indicator.startAnimating()
        if let createCardView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "QumparaCardViewController") as? QumparaCardViewController {
            if let navigator = self.navigationController {
                let headers: HTTPHeaders = [
                    "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                ]
                Alamofire.request(APIURL.CreateUserByQumparamCard.rawValue, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    debugPrint(response)
                    let result = response.result
                    
                    let httpCode = response.response?.statusCode
                    switch httpCode! {
                    case 200:
                        if let data = response.result.value as? Dictionary<String, AnyObject> {
                            if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                if let smsToken = responseData["SmsToken"] as? String {
                                    self.defaults.set(smsToken, forKey: "sms_token")
                                }
                            }
                        }
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        self.removeBlurLayer()
                        let transferMoneyView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "QumparamTransferViewController") as? QumparamTransferViewController
                        navigator.pushViewController(transferMoneyView!, animated: true)
                        break
                    case 401:
                        self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        self.removeBlurLayer()
                        break
                    case 404:
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        self.removeBlurLayer()
                        navigator.pushViewController(createCardView, animated: true)
                    case 403, 405, 500:
                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        self.removeBlurLayer()
                        break
                    default: break
                    }
                }
            }
        }
    }
    
    @IBAction func sendToPrize(_ sender: AnyObject) {
        
        if let prizeListView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "PrizesListViewController") as? PrizesListViewController {
            if let navigator = self.navigationController {
                navigator.pushViewController(prizeListView, animated: true)
            }
        }
        
    }
    
}
