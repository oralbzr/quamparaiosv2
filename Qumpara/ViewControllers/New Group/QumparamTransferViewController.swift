//
//  QumparamTransferViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/27/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

class QumparamTransferViewController: UIViewController {
    
    @IBOutlet weak var accountMoney: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var money: UITextField!
    @IBOutlet weak var sendToCardBtn: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var cardDate: UILabel!
    @IBOutlet weak var cardCvv: UILabel!
    
    @IBOutlet weak var handlingScroolView: UIView!
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addToolBar(textField: money)
        sendToCardBtn.setDisable()
        
        money.keyboardType = .numberPad
        
        indicator.center = view.center
        indicator.isHidden = true
        getCard()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getCard() {
        addBlurLayer()
        view.addSubview(indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        Alamofire.request(APIURL.QumparamGetCard.rawValue, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            print(self.defaults.object(forKey: "access_token")!)
            let result = response.result
            let httpCode = response.response?.statusCode
            switch httpCode! {
            case 200:
                if let data = response.result.value as? Dictionary<String, AnyObject> {
                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                        if let code = responseData["Code"] as? String {
                            if let desc = responseData["Description"] as? String {
                                let alert = UIAlertController(title: "\(code)", message: "\(desc)", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        if let cardimage = responseData["QumparamCardImage"] as? String {
                            let url = URL(string:cardimage)
                            if let dataImage = try? Data(contentsOf: url!)
                            {
                                self.cardImage.image = UIImage(data: dataImage)!
                            }
                        }
                        if let money = responseData["remainingMoney"] as? String {
                            self.accountMoney.text = "\(money) TL"
                        }
                        if let name = responseData["name"] as? String {
                            if let surname = responseData["surname"] as? String {
                                self.cardName.text = "\(name) \(surname)"
                            }
                        }
                        if let expdate = responseData["expDate"] as? String {
                            self.cardDate.text = expdate
                        }
                        if let cvv = responseData["cvv"] as? String {
                            self.cardCvv.text = "CVC \(cvv)"
                        }
                        if let cardno = responseData["cardNumber"] as? String {
                            self.cardNumber.text = cardno
                        }
                    }
                }
                break
            case 401:
                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                break
            case 403, 404, 405, 500:
                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                break
            default: break
            }
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            self.removeBlurLayer()
        }
    }
    
    @IBAction func sendToCard(_ sender: UIButton) {
        addBlurLayer()
        view.addSubview(indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "Money": self.money.text! as AnyObject
        ]
        Alamofire.request(APIURL.QumparamCardChargingMoney.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let result = response.result
            let httpCode = response.response?.statusCode
            switch httpCode! {
            case 200:
                self.showAlert(withTitle: "Başarılı İşlem", withMessage: "Para Aktarma işleminiz başarıyla gerçekleşti.", withAction: "pop")
                break
            case 401:
                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                break
            case 403, 404, 405, 500:
                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                break
            default: break
            }
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            self.removeBlurLayer()
        }
        
    }
    
    @IBAction func editDidBegin(_ sender: UITextField) {
        animateViewMoving(up: true, moveValue: self.handlingScroolView.frame.height / 2)
    }
    
    @IBAction func editDidEnd(_ sender: UITextField) {
        animateViewMoving(up: false, moveValue: self.handlingScroolView.frame.height / 2)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        textField(sender, shouldChangeCharactersIn: NSMakeRange(0, (sender.text?.count)!), replacementString: "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 3
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField.text?.count == 3 && textField.text != "0" && textField.text != "" {
            sendToCardBtn.setEnable()
        } else {
            sendToCardBtn.setDisable()
        }
        return newString.length <= maxLength
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
}
