//
//  CouponDetailViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 22/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class CouponDetailViewController: UIViewController {

    @IBOutlet weak var campaignImage: UIImageView!
    @IBOutlet weak var titleLbl:  UILabel!
    @IBOutlet weak var noText: UILabel!
    @IBOutlet weak var descriptionTxt: UITextView!
    @IBOutlet weak var wonDateText: UILabel!
    @IBOutlet weak var passText: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var btnLink: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnCopy: UIButton!
    @IBOutlet weak var btnUsed: UIButton!
    @IBOutlet weak var usedLbl: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var titleLblTxt:  String!
    var couponId: String!
    var isLink:Bool? = nil
    var selectedCoupon:Coupon = Coupon()
    let defaults = UserDefaults.standard
    var isCheckBox: Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
         self.indicator.center=self.view.center
         self.indicator.show()
         btnLink.layer.borderColor = UIColor.white.cgColor
        
        if (isCheckBox == false){
            btnUsed.isHidden = true
            usedLbl.isHidden = true
        }else{
            btnUsed.isHidden = false
            usedLbl.isHidden = false
        }
         getCouponInfo()
        // Do any additional setup after loading the view.
    }
    
    func getCouponInfo(){
        
        let parameters: [String: AnyObject] = [
            "CouponId" : couponId as AnyObject,
            ]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        
        Alamofire.request(APIURL.GetCouponInformation.rawValue ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            self.indicator.hide()
            switch result {
            case .success:
                print(result)
                print(result.value as Any)
                if let data = result.value as? Dictionary<String, AnyObject> {
                
                        if let description = data["description"] as? String{
                            
                        }
                        
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                           
                            let coupon = Coupon()
                            
                            if let CouponId = responseData["CouponId"] as? String {
                                coupon.couponId = CouponId
                            }
                            if let WonDate = responseData ["WonDate"] as? String {
                                coupon.wonDate = WonDate
                            }
                            if let CouponName = responseData ["CouponName"] as? String {
                                coupon.couponName = CouponName
                            }
                            if let CampaignImageUrl = responseData ["CampaignImageUrl"] as? String {
                                coupon.campaignImageUrl = CampaignImageUrl
                            }
                            if let CouponDescription = responseData ["CouponDescription"] as? String {
                                coupon.couponDescription = CouponDescription
                            }
                            if let Code = responseData ["Code"] as? String {
                                coupon.code = Code
                            }
                            if let Description = responseData ["Description"] as? String {
                                coupon.campaignDescription = Description
                            }
                            if let CouponType = responseData ["CouponType"] as? NSNumber {
                                coupon.couponType = CouponType
                            }
                            if let IsUsed = responseData ["IsUsed"] as? Bool {
                                coupon.isUsed = IsUsed
                            }
                            if let CampaignShareLink = responseData["CampaignShareLink"] as? String {
                                coupon.campaignShareLink = CampaignShareLink
                            }
                            
                            self.selectedCoupon=coupon
                            self.setInfo()
                }
                }
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
     func setInfo() {
        if selectedCoupon.couponType == 1{
           btnLink.isHidden=true
           textView.isHidden=false
           
        }else{
            btnLink.isHidden=false
            textView.isHidden=true
            
        }
        
        self.campaignImage.sd_setImage(with: URL(string: self.selectedCoupon.campaignImageUrl), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(),
                                       completed: {(image,error,cacheType,url) in
                                        if error != nil {
                                            print(error ?? "campaign image default error")
                                            
                                            }
                                        })
        
        titleLbl.text = titleLblTxt
        noText.text = selectedCoupon.couponName
        btnUsed.isSelected = selectedCoupon.isUsed
        wonDateText.text = Utility .convertFormat(selectedCoupon.wonDate)
        descriptionTxt.text = selectedCoupon.couponDescription.html2String
        let size = descriptionTxt.sizeThatFits(self.descriptionTxt.bounds.size)
        var frame = self.descriptionTxt.frame
        frame.size.height = size.height
        descriptionTxt.frame = frame
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height:descriptionTxt.frame.origin.y + descriptionTxt.frame.height+10)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HHTabBarView.shared.isHidden = true
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    @IBAction func shareCampaign(_ sender: AnyObject) {
         if selectedCoupon.couponType == 1{
            let activityVC = UIActivityViewController(activityItems: ["Qumpara’dan kazandığım kupon adı:\(titleLblTxt) \(selectedCoupon.couponName)"], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
            
         }else{
            let activityVC = UIActivityViewController(activityItems: [(selectedCoupon.campaignShareLink)], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func usedCheckUncheck(_ sender: UIButton) {
        
        self.indicator.show()
        self.setUsedInfo()
    }
    
    func setUsedInfo(){
        
        let parameters: [String: AnyObject] = [
            "IsUsed" : !btnUsed.isSelected as AnyObject,
            ]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        print(parameters)
         print("\(APIURL.UpdateUsedCoupon.rawValue)/\(couponId)")
        let couponID = self.couponId
        Alamofire.request("\(APIURL.UpdateUsedCoupon.rawValue)/\(couponID ?? "")" ,method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            self.indicator.hide()
            switch result {
            case .success:
                print(result)
                print(result.value as Any)
                if let data = result.value as? Dictionary<String, AnyObject> {
                    
                    if let description = data["description"] as? String{
                        print(description)
                    }
                    
                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                        if let Code = responseData ["Code"] as? String {
                            if !Code.isEmpty {
                                
                                if self.btnUsed.isSelected == true {
                                    self.btnUsed.isSelected = false
                                }else{
                                    self.btnUsed.isSelected = true
                                }
                        /*
                            if let Description = responseData ["Description"] as? String {
                                let alert = UIAlertController(title: "Bilgi", message:Description, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                        */
                        }
                        }
                        if let error = responseData ["ErrorCode"] as? String {
                            if !error.isEmpty {
                            if let Description = responseData ["ErrorDescription"] as? String {
                                let alert = UIAlertController(title: "", message:Description, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        }
                    }
                }
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    
    
    @IBAction func gotoCouponLink(_ sender: UIButton) {
        
        if let linkDetailView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "WebInfoViewController") as? WebInfoViewController {
            if let navigator = self.navigationController {
                linkDetailView.url=self.selectedCoupon.couponName
                navigator.pushViewController(linkDetailView, animated: true)
            }
        }
        
    }

    @IBAction func copyToClipboard(_ sender: UIButton) {
        
        if selectedCoupon.couponType == 1{
            UIPasteboard.general.string = selectedCoupon.couponName
            let alert = UIAlertController(title: "Bilgi", message: "Kod kopyalandı.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            UIPasteboard.general.string = selectedCoupon.couponName
            let alert = UIAlertController(title: "Bilgi", message: "Link kopyalandı.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    

}
