//
//  QumparamOTPViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/27/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

class QumparamOTPViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var verificationCodeTextfield: UITextField!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var closeButton: UIButton!
    
    var timer = Timer()
    var totalTime = 180
    var timeRemaining = 180
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        continueButton.setDisable()
        closeButton.isHidden = true
        indicator.center = view.center
        
        addToolBar(textField: verificationCodeTextfield)
        verificationCodeTextfield.keyboardType = UIKeyboardType.numberPad
        
        indicator.isHidden = true
        resendButton.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
    }
    
    @objc func timerRunning() {
        timeRemaining -= 1
        progressView.setProgress(Float(timeRemaining)/Float(totalTime), animated: false)
        let secondsLeft = Int(timeRemaining)
        timeRemainingLabel.text = "Kalan Süre: \(secondsLeft) sn"
        
        if timeRemaining == 0 {
            timer.invalidate()
            timeRemainingLabel.isHidden = true
            continueButton.setDisable()
            verificationCodeTextfield.text = ""
            resendButton.isHidden = false
            closeButton.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            // TODO - 500 Internal Server Error
            "gsmNumber": self.defaults.object(forKey: "PhoneNumber")! as AnyObject,
            "otp": verificationCodeTextfield.text! as AnyObject,
            "token": "\(self.defaults.object(forKey: "sms_token")!)" as AnyObject
        ]
        Alamofire.request(APIURL.QumparamCardVerificationSMS.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let result = response.result
            
            let httpCode = response.response?.statusCode
            switch httpCode! {
            case 200:
                if let transferView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "QumparamTransferViewController") as? QumparamTransferViewController {
                    if let navigator = self.navigationController {
                        let headers: HTTPHeaders = [
                            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                        ]
                        Alamofire.request(APIURL.CreateUserByQumparamCard.rawValue, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                            let httpCode = response.response?.statusCode
                            let result = response.result
                            switch httpCode! {
                            case 200:
                                if let data = response.result.value as? Dictionary<String, AnyObject> {
                                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                        if let smsToken = responseData["SmsToken"] as? String {
                                            self.defaults.set(smsToken, forKey: "sms_token")
                                        }
                                    }
                                }
                                navigator.pushViewController(transferView, animated: true)
                                break
                            case 401:
                                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                                break
                            case 403, 404, 405, 500:
                                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                                break
                            default: break
                            }
                        }
                    }
                }
                break
            case 401:
                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                break
            case 403, 405, 500:
                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                break
            default: break
            }
        }
    }
    
    @IBAction func resendButtonTapped(_ sender: UIButton) {
        super.viewDidLoad()
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        textField(sender, shouldChangeCharactersIn: NSMakeRange(0, (sender.text?.count)!), replacementString: "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField.text?.count == 4 {
            continueButton.setEnable()
        } else {
            continueButton.setDisable()
        }
        return newString.length <= maxLength
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        if let navigator = self.navigationController {
            self.indicator.stopAnimating()
            navigator.popToRootViewController(animated: true)
        }
    }
}
