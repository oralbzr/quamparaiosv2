//
//  PrizeDetailViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 24/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class PrizeDetailViewController: UIViewController {
    
    @IBOutlet weak var campaignImage: UIImageView!
    @IBOutlet weak var titleLbl:  UILabel!
    @IBOutlet weak var prizeText: UILabel!
    @IBOutlet weak var userAccountText: UILabel!
    @IBOutlet weak var descriptionTxt: UITextView!
    @IBOutlet weak var expirationDateText: UILabel!
   
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnPrize: UIButton!
   
  
    @IBOutlet weak var indicator: UIActivityIndicatorView!
   
    let defaults = UserDefaults.standard
     var titleText: String!
    
   var selectedPrize:Prize = Prize()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.indicator.center=self.view.center
         btnPrize.layer.borderColor = UIColor.white.cgColor
        if !titleText.isEmpty {
            userAccountText.text=titleText
        }
        self.setInfo()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setInfo() {
       
        self.campaignImage.sd_setImage(with: URL(string: self.selectedPrize.prizeImageUrl), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(),
                                       completed: {(image,error,cacheType,url) in
                                        if error != nil {
                                            print(error ?? "campaign image default error")
                                            
                                        }
        })
        
        titleLbl.text = selectedPrize.prizeTitle
        prizeText.text = String(format: "%@ TL", selectedPrize.prizeValue.stringValue)
        expirationDateText.text = Utility .convertFormat(selectedPrize.expirationDate)
        descriptionTxt.text = selectedPrize.prizeDescription.html2String
        let size = descriptionTxt.sizeThatFits(self.descriptionTxt.bounds.size)
        var frame = self.descriptionTxt.frame
        frame.size.height = size.height
        descriptionTxt.frame = frame
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height:descriptionTxt.frame.origin.y + descriptionTxt.frame.height+10)
    }
    
    @IBAction func convertPrize(_ sender: UIButton) {
        
        self.indicator.show()
        let parameters: [String: AnyObject] = [
            "CampaignId" : selectedPrize.prizeId as AnyObject,
            ]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        print(parameters)
       
        Alamofire.request(APIURL.ConvertToPrize.rawValue ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            self.indicator.hide()
            switch result {
            case .success:
                print(result)
                print(result.value as Any)
                if let data = result.value as? Dictionary<String, AnyObject> {
                    
                    if let description = data["description"] as? String{
                        print(description)
                    }
                    
                    if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                        if let Code = responseData ["Code"] as? String {
                            if !Code.isEmpty {
                                
                                if let Description = responseData ["Description"] as? String {
                                    self.showAlert(withTitle: "Bilgi", withMessage: Description, withAction: "root")
                                }
                            }
                        }
                        if let error = responseData ["ErrorCode"] as? String {
                            if !error.isEmpty {
                                if let Description = responseData ["ErrorDescription"] as? String {
                                    self.showAlert(withTitle: "Uyarı", withMessage: Description, withAction: "root")
                                }
                            }
                        }
                    }
                }
                break
            case .failure:
                  self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "root")
                break
            }
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HHTabBarView.shared.isHidden = true
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
        }
    }
    

}
