//
//  CouponCell.swift
//  Qumpara
//
//  Created by Oral Bozkır on 22/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class CouponCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblPass: UILabel!
   
  
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }
    
    func configureCell(dateTxt:String,noTxt:String,noPass:String){
        
        lblDate.text =  Utility .convertFormat(dateTxt)
        lblNo.text = noTxt
      //  lblPass.text = noPass
        
    }
    
    override func prepareForReuse() {
        self.lblDate.text = ""
        self.lblPass.text = ""
      //  self.lblNo.text = ""
    }
   

}
