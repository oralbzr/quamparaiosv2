//
//  NotificationWonViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/9/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

class NotificationWonViewController: UIViewController {
    
    @IBOutlet weak var notificationDate: UILabel!
    @IBOutlet weak var notificationText: UILabel!
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var receiptIdText: UILabel!
    @IBOutlet weak var notificationImage: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var notificationID: Int = 0
    let notDetail = NotificationDetail()
    
    var defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if Reachability.isInternetAvailable() {
            getNotificationDetail(withID: notificationID)
        } else {
            self.showAlert(withTitle: "Uyarı", withMessage: "Lütfen internet bağlantınızı kontrol ediniz.", withAction: "")
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        notificationImage.addGestureRecognizer(tap)
        
        notificationImage.isHidden = true
        indicator.center = view.center
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }
    }
    
    func getNotificationDetail(withID id:Int) {
        addBlurLayer()
        view.addSubview(indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "NotificationId": id as AnyObject
        ]
        Alamofire.request(APIURL.GetNotificationDetail.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let result = response.result
            let httpCode = response.response!.statusCode
            
            switch result {
            case .success:
                switch httpCode {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                            if let notificationdate = responseData["NotificationDate"] as? String {
                                self.notDetail.NotificationDate = notificationdate
                                self.notificationDate.text = self.notDetail.NotificationDate.standardToDate
                            }
                            if let notification = responseData["Notification"] as? String {
                                self.notDetail.Notification = notification
                                self.notificationText.text = self.notDetail.Notification
                            }
                            if let ntype = responseData["Type"] as? Int {
                                self.notDetail.NType = ntype
                            }
                            if let subtype = responseData["SubType"] as? Int {
                                self.notDetail.SubType = subtype
                                switch self.notDetail.SubType! {
                                case 2:
                                    self.navigationButton.setTitle("Ödülü Gör", for: .normal)
                                    break
                                case 1:
                                    self.navigationButton.setTitle("Qumparam'a Git", for: .normal)
                                    break
                                default: break
                                }
                            }
                            if let receiptid = responseData["ReceiptId"] as? Int {
                                self.notDetail.ReceiptId = receiptid
                            }
                            if let receiptimage = responseData["ReceiptImage"] as? String {
                                self.notDetail.ReceiptImage = receiptimage
                                self.notificationImage.isHidden = false
                                if self.notDetail.ReceiptImage != nil {
                                    let url = URL(string:self.notDetail.ReceiptImage!)
                                    if let dataImage = try? Data(contentsOf: url!)
                                    {
                                        self.notificationImage.image = UIImage(data: dataImage)!
                                    }
                                }
                            }
                            if let couponid = responseData["CouponId"] as? Int {
                                self.notDetail.CouponId = couponid
                            }
                            if let descriptiondetailhtml = responseData["DescriptionDetailHTML"] as? String {
                                self.notDetail.DescriptionDetailHTML = descriptiondetailhtml
                            }
                            if let contactaddeddate = responseData["ContactAddedDate"] as? String {
                                self.notDetail.ContactAddedDate = contactaddeddate
                            }
                            if let contactformdescription = responseData["ContactFormDescription"] as? String {
                                self.notDetail.ContactFormDescription = contactformdescription
                            }
                        }
                    }
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                    self.removeBlurLayer()
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 404, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                default: break
                }
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "---", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func navigationButtonTapped(_ sender: UIButton) {
        switch notDetail.SubType! {
        case 2:
            let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?[1] as! UINavigationController
            let storyboard = UIStoryboard(name: "Views", bundle: nil)
            let destinationViewController = storyboard.instantiateViewController(withIdentifier: "CouponDetailViewController") as? CouponDetailViewController
            destinationViewController?.couponId = "\(self.notDetail.CouponId!)"
            navInTab.pushViewController(destinationViewController!, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                HHTabBarView.shared.selectTabAtIndex(withIndex: 1)
            }
            break
        case 1:
            HHTabBarView.shared.selectTabAtIndex(withIndex: 1)
            break
        default: break
        }
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleToFill
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        HHTabBarView.shared.isHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = true
        HHTabBarView.shared.isHidden = true
        sender.view?.removeFromSuperview()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
