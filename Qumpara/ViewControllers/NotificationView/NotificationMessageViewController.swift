//
//  NotificationMessageViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/9/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

class NotificationMessageViewController: UIViewController {
    
    @IBOutlet weak var notificationDate: UILabel!
    @IBOutlet weak var notificationText: UILabel!
    @IBOutlet weak var notificationTextDetail: UILabel!
    
    @IBOutlet weak var customerView: UIView!
    @IBOutlet weak var customerQuestionDate: UILabel!
    @IBOutlet weak var customerQuestionText: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var notificationID: Int = 0
    let notDetail = NotificationDetail()
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isInternetAvailable() {
            getNotificationDetail(withID: notificationID)
        } else {
            self.showAlert(withTitle: "Uyarı", withMessage: "Lütfen internet bağlantınızı kontrol ediniz.", withAction: "")
        }
        
        navigationButton.isHidden = true
        customerView.isHidden = true
        imageView.isHidden = true
        notificationTextDetail.isHidden = true
        
        //customerQuestionDate.isHidden = true
        //customerQuestionText.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        imageView.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
        indicator.center = view.center
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func getNotificationDetail(withID id:Int) {
        addBlurLayer()
        view.addSubview(indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "NotificationId": id as AnyObject
        ]
        Alamofire.request(APIURL.GetNotificationDetail.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            
            let result = response.result
            let httpCode = response.response!.statusCode
            
            switch result {
            case .success:
                switch httpCode {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                            
                            if let notificationdate = responseData["NotificationDate"] as? String {
                                self.notDetail.NotificationDate = notificationdate
                                self.notificationDate.text = self.notDetail.NotificationDate.standardToDate
                            }
                            if let notification = responseData["Notification"] as? String {
                                self.notDetail.Notification = notification
                                self.notificationText.attributedText = self.notDetail.Notification.html2AttributedString
                            }
                            if let type = responseData["Type"] as? Int {
                                self.notDetail.NType = type
                            }
                            if let subtype = responseData["SubType"] as? Int {
                                self.notDetail.SubType = subtype
                            }
                            if let receiptid = responseData["ReceiptId"] as? Int {
                                self.notDetail.ReceiptId = receiptid
                            }
                            if let receiptimage = responseData["ReceiptImage"] as? String {
                                self.notDetail.ReceiptImage = receiptimage
                                self.imageView.isHidden = false
                                let url = URL(string:self.notDetail.ReceiptImage!)
                                if let dataImage = try? Data(contentsOf: url!)
                                {
                                    self.imageView.image = UIImage(data: dataImage)!
                                }
                            }
                            if let couponid = responseData["CouponId"] as? Int {
                                self.notDetail.CouponId = couponid
                            }
                            if let descriptiondetailhtml = responseData["DescriptionDetailHTML"] as? String {
                                self.notDetail.DescriptionDetailHTML = descriptiondetailhtml
                            }
                            if let contactaddeddate = responseData["ContactAddedDate"] as? String {
                                self.notDetail.ContactAddedDate = contactaddeddate
                                self.customerQuestionDate.text = contactaddeddate.timeToDate
                            }
                            if let contactformdescription = responseData["ContactFormDescription"] as? String {
                                self.notDetail.ContactFormDescription = contactformdescription
                                self.customerQuestionText.text = contactformdescription
                            }
                            
                            if let subtype = self.notDetail.SubType {
                                switch subtype {
                                case 4:
                                    self.navigationButton.setTitle("Qumparam'a Devam Et", for: .normal)
                                    self.navigationButton.isHidden = false
                                    break
                                case 5:
                                    self.navigationButton.setTitle("Profilime Git", for: .normal)
                                    self.navigationButton.isHidden = false
                                    break
                                case 6:
                                    self.imageView.isHidden = false
                                    break
                                case 7:
                                    self.navigationButton.setTitle("Promosyonları Görüntüle", for: .normal)
                                    self.navigationButton.isHidden = false
                                    break
                                case 8:
                                    self.customerView.isHidden = false
                                    break
                                default: break
                                }
                            }
                            
                            if self.notDetail.DescriptionDetailHTML != nil {
                                self.notificationTextDetail.isHidden = false
                                self.notificationTextDetail.attributedText = self.notDetail.DescriptionDetailHTML?.html2AttributedString
                            }
                        }
                    }
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 404, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                default: break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "-----", withAction: "pop")
                break
            }
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            self.removeBlurLayer()
        }
    }
    
    @IBAction func navigationButtonTapped(_ sender: UIButton) {
        if let subtype = self.notDetail.SubType {
            switch subtype {
            case 4:
                HHTabBarView.shared.selectTabAtIndex(withIndex: 1)
                break
            case 5:
                let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?.last as! UINavigationController
                let storyboard = UIStoryboard(name: "Views", bundle: nil)
                let destinationViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
                navInTab.pushViewController(destinationViewController!, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    HHTabBarView.shared.selectTabAtIndex(withIndex: 4)
                }
                break
            case 7:
                let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?.last as! UINavigationController
                let storyboard = UIStoryboard(name: "Views", bundle: nil)
                let destinationViewController = storyboard.instantiateViewController(withIdentifier: "PromotionViewController") as? PromotionViewController
                navInTab.pushViewController(destinationViewController!, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    HHTabBarView.shared.selectTabAtIndex(withIndex: 4)
                }
                break
            default: break
            }
        }
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleToFill
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        HHTabBarView.shared.isHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = true
        HHTabBarView.shared.isHidden = true
        sender.view?.removeFromSuperview()
    }
}
