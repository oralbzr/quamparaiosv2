//
//  QuestionnaireViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/9/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

class SurveyViewController: UIViewController {
    
    @IBOutlet weak var surveyCaptionLbl: UILabel!
    @IBOutlet weak var questionCaptionLbl: UILabel!
    
    @IBOutlet weak var option1Lbl: UILabel!
    @IBOutlet weak var option2Lbl: UILabel!
    @IBOutlet weak var option3Lbl: UILabel!
    @IBOutlet weak var option4Lbl: UILabel!
    
    @IBOutlet weak var option1Btn: UIButton!
    @IBOutlet weak var option2Btn: UIButton!
    @IBOutlet weak var option3Btn: UIButton!
    @IBOutlet weak var option4Btn: UIButton!
    
    @IBOutlet weak var pageLbl: UILabel!
    
    @IBOutlet weak var notInterestedBtn: UIButton!
    @IBOutlet weak var sendAnswerBtn: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var forhide: UIView!
    
    
    var selectedOption = 0
    var optionCount = 0
    var numberOfPages = 1
    var currentPage = 1
    
    var surveyID: Int = 0
    var survey = Survey()
    
    var defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isInternetAvailable() {
            getSurvey(withID: surveyID)
        } else {
            self.showAlert(withTitle: "Uyarı", withMessage: "Lütfen internet bağlantınızı kontrol ediniz.", withAction: "")
        }
        // Do any additional setup after loading the view.
        
        notInterestedBtn.layer.cornerRadius = 5
        notInterestedBtn.layer.borderWidth = 0.5
        
        sendAnswerBtn.layer.cornerRadius = 5
        sendAnswerBtn.layer.borderWidth = 0.5
        
        sendAnswerBtn.setDisable()
        
        indicator.center = view.center
        
        forhide.isHidden = true
    }
    
    func getSurvey(withID id: Int) {
        addBlurLayer()
        view.addSubview(indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "SurveyId": id as AnyObject
        ]
        Alamofire.request(APIURL.GetSurveyQuestion.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let result = response.result
            let httpCode = response.response!.statusCode
            
            switch result {
            case .success:
                switch httpCode {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                            self.survey.Name = responseData["Name"] as! String
                            self.survey.Caption = responseData["Caption"] as! String
                            if let questionsData = responseData["QuestionList"] as? [Dictionary<String,AnyObject>] {
                                self.numberOfPages = questionsData.count
                                for i in 0...questionsData.count - 1 {
                                    if let questionData = questionsData[i] as? Dictionary<String, AnyObject> {
                                        var question = Question()
                                        question.QuestionID = questionData["QuestionId"] as! Int
                                        question.Caption = questionData["Caption"] as! String
                                        question.Options.append(questionData["Option1"] as! String)
                                        question.Options.append(questionData["Option2"] as! String)
                                        if let caption3 = questionData["Option3"] as? String{
                                            question.Options.append(caption3)
                                        }
                                        if let caption4 = questionData["Option4"] as? String {
                                            question.Options.append(caption4)
                                        }
                                        self.survey.Questions.append(question)
                                    }
                                }
                                self.showQuestion(withID: (self.currentPage - 1))
                            }
                        }
                    }
                    break
                case 401:
                    self.forhide.isHidden = false
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 404:
                    self.forhide.isHidden = false
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                case 500:
                    self.forhide.isHidden = false
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                default: break
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                self.removeBlurLayer()
                break
            case .failure:
                self.forhide.isHidden = false
                self.showAlert(withTitle: "Uyarı", withMessage: "Bağlantı Hatası", withAction: "pop")
                break
            }
        }
    }
    
    func showQuestion(withID id: Int) {
        if self.currentPage == self.numberOfPages {
            self.sendAnswerBtn.setTitle("Gönder", for: .normal)
        } else if self.currentPage < self.numberOfPages {
            self.sendAnswerBtn.setTitle("Sonraki", for: .normal)
        }
        self.pageLbl.text = "\(self.currentPage)/\(self.numberOfPages)"
        let question = self.survey.Questions[id]
        self.surveyCaptionLbl.text = self.survey.Caption
        self.questionCaptionLbl.text = question.Caption
        self.option1Lbl.text = question.Options[0]
        self.option2Lbl.text = question.Options[1]
        if question.Options.count == 2 {
            self.option3Btn.isHidden = true
            self.option3Lbl.isHidden = true
            self.option4Btn.isHidden = true
            self.option4Lbl.isHidden = true
        }
        if question.Options.count == 3 {
            if self.option3Lbl.isHidden == true {
                self.option3Btn.isHidden = false
                self.option3Lbl.isHidden = false
            }
            self.option3Lbl.text = question.Options[2]
            self.option4Btn.isHidden = true
            self.option4Lbl.isHidden = true
        } else if question.Options.count == 4 {
            if option3Lbl.isHidden == true && option4Lbl.isHidden == true {
                self.option3Btn.isHidden = false
                self.option3Lbl.isHidden = false
                self.option4Btn.isHidden = false
                self.option4Lbl.isHidden = false
            } else if option4Lbl.isHidden == true && option3Lbl.isHidden == false {
                self.option4Btn.isHidden = false
                self.option4Lbl.isHidden = false
            }
            self.option3Lbl.text = question.Options[2]
            self.option4Lbl.text = question.Options[3]
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func surveyDenied(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "SurveyId": surveyID as AnyObject
        ]
        Alamofire.request(APIURL.SurveyDenied.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            let httpCode = response.response!.statusCode
            
            switch result {
            case .success:
                switch httpCode {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                            self.showAlert(withTitle: "Başarılı", withMessage: "\(responseData["Description"]!)", withAction: "pop")
                        }
                    }
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 404:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                case 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                default: break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "Bağlantı hatası", withAction: "pop")
                break
            }
        }
    }
    
    @IBAction func sendSurveyAnswer(_ sender: UIButton) {
        if sendAnswerBtn.titleLabel?.text == "Sonraki" {
            self.currentPage += 1
            showQuestion(withID: (self.currentPage) - 1)
            self.survey.Questions[self.currentPage - 1].Answer = self.selectedOption
            option1Btn.isSelected = false
            option2Btn.isSelected = false
            option3Btn.isSelected = false
            option4Btn.isSelected = false
            optionCount = 0
            sendAnswerBtn.setDisable()
        } else {
            self.navigationController?.popViewController(animated: true)
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
            ]
            var answer: [String:Any] = [:]
            var answers: [[String:Any]] = []
            for item in self.survey.Questions {
                answer = [
                    "QuestionId": item.QuestionID,
                    "Answer": item.Answer
                ]
                answers.append(answer)
                answer = [:]
            }
            let parameters: [String: Any] = [
                "SurveyId": surveyID as AnyObject,
                "QuestionAnswer": answers as AnyObject
            ]

            Alamofire.request(APIURL.SurveyAnswer.rawValue, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                debugPrint(response)
                let result = response.result
                let httpCode = response.response!.statusCode
                switch result {
                case .success:
                    switch httpCode {
                    case 200:
                        if let data = result.value as? Dictionary<String, AnyObject> {
                            if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                self.showAlert(withTitle: "Başarılı", withMessage: "\(responseData["Description"]!)", withAction: "pop")
                            }
                        }
                        break
                    case 401:
                        self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                        break
                    case 404:
                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                        break
                    case 500:
                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                        break
                    default: break
                    }
                    break
                case .failure:
                    self.showAlert(withTitle: "Uyarı", withMessage: "Bağlantı hatası", withAction: "pop")
                    break
                }
            }
        }
    }
    
    @IBAction func option1Selected(_ sender: UIButton) {
        if optionCount == 0 {
            sender.isSelected = true
            optionCount = 1
            selectedOption = 1
            sendAnswerBtn.setEnable()
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                optionCount = 0
                selectedOption = 0
                sendAnswerBtn.setDisable()
            } else {
                sender.isSelected = true
                option2Btn.isSelected = false
                option3Btn.isSelected = false
                option4Btn.isSelected = false
                optionCount = 1
                selectedOption = 1
                sendAnswerBtn.setEnable()
            }
        }
    }
    @IBAction func option2Selected(_ sender: UIButton) {
        if optionCount == 0 {
            sender.isSelected = true
            optionCount = 1
            self.selectedOption = 2
            sendAnswerBtn.setEnable()
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                optionCount = 0
                selectedOption = 0
                sendAnswerBtn.setDisable()
            } else {
                sender.isSelected = true
                option1Btn.isSelected = false
                option3Btn.isSelected = false
                option4Btn.isSelected = false
                optionCount = 1
                selectedOption = 2
                sendAnswerBtn.setEnable()
            }
        }
    }
    @IBAction func option3Selected(_ sender: UIButton) {
        if optionCount == 0 {
            sender.isSelected = true
            optionCount = 1
            self.selectedOption = 3
            sendAnswerBtn.setEnable()
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                optionCount = 0
                selectedOption = 0
                sendAnswerBtn.setDisable()
            } else {
                sender.isSelected = true
                option1Btn.isSelected = false
                option2Btn.isSelected = false
                option4Btn.isSelected = false
                optionCount = 1
                selectedOption = 3
                sendAnswerBtn.setEnable()
            }
        }
    }
    @IBAction func option4Selected(_ sender: UIButton) {
        if optionCount == 0 {
            sender.isSelected = true
            optionCount = 1
            self.selectedOption = 4
            sendAnswerBtn.setEnable()
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                optionCount = 0
                selectedOption = 0
                sendAnswerBtn.setDisable()
            } else {
                sender.isSelected = true
                option1Btn.isSelected = false
                option2Btn.isSelected = false
                option3Btn.isSelected = false
                optionCount = 1
                selectedOption = 4
                sendAnswerBtn.setEnable()
            }
        }
    }
}
