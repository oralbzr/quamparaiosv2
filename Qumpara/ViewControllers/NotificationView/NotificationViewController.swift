//
//  NotificationViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 10/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit
import Alamofire

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var defaults = UserDefaults.standard
    var notifications: [Notifications] = []
    private var currentPage = 1
    private var shouldShowLoadingCell = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        tableView.separatorStyle = .singleLine
//        tableView.separatorColor = UIColor.orange
        tableView.tableFooterView = customView
        indicator.startAnimating()
        
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.lightGray
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        if Reachability.isInternetAvailable() {
            if defaults.object(forKey: "access_token") == nil {
                if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                    if let navigator = self.navigationController {
                        navigator.pushViewController(signUpView, animated: false)
                    }
                }
            } else {
                getNotifications(withPage: currentPage)
            }
        } else {
            self.showAlert(withTitle: "Uyarı", withMessage: "Lütfen internet bağlantınızı kontrol ediniz.", withAction: "")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = false
        
        if Reachability.isInternetAvailable() {
            if defaults.object(forKey: "access_token") == nil {
                if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                    if let navigator = self.navigationController {
                        navigator.pushViewController(signUpView, animated: false)
                    }
                }
            }
            if tableView.visibleCells.isEmpty {
                getNotifications(withPage: 1)
            }
        } else {
            self.showAlert(withTitle: "Uyarı", withMessage: "Lütfen internet bağlantınızı kontrol ediniz.", withAction: "")
        }
    }
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        notifications.removeAll()
        currentPage = 1
        getNotifications(withPage: currentPage)
        self.tableView.reloadData()
        if notifications.isEmpty {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = ""
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            //tableView.separatorStyle  = .none
        }
        delayExecutionByMilliseconds(500){
            refreshControl.endRefreshing()
        }
    }
    
    fileprivate func delayExecutionByMilliseconds(_ delay: Int, for anonFunc: @escaping () -> Void) {
        let when = DispatchTime.now() + .milliseconds(delay)
        DispatchQueue.main.asyncAfter(deadline: when, execute: anonFunc)
    }
    
    func getNotifications(withPage page: Int) {
        tableView.backgroundView = nil
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "PageNumber": page as AnyObject
        ]
        
        Alamofire.request(APIURL.GetNotifications.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            self.indicator.stopAnimating()
            let result = response.result
            switch result {
            case .success:
                if let data = result.value as? Dictionary<String, AnyObject> {
                    if let httpCode = response.response?.statusCode {
                        switch httpCode {
                        case 200:
                            print("success")
                            if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                                if responseData.isEmpty {
                                    self.tableView.tableFooterView = nil
                                    self.tableView.reloadData()
                                    if self.notifications.isEmpty {
                                        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
                                        noDataLabel.text          = "Bildiriminiz bulunmamaktadır."
                                        noDataLabel.textColor     = UIColor.black
                                        noDataLabel.textAlignment = .center
                                        self.tableView.backgroundView  = noDataLabel
                                        self.tableView.separatorStyle  = .none
                                    }
                                    break
                                } else {
                                    for i in 0...responseData.count - 1 {
                                        let not = Notifications()
                                        if let notification = responseData[i]["Notification"] as? String {
                                            not.Notification = notification
                                        }
                                        if let notificationdate = responseData[i]["NotificationDate"] as? String {
                                            not.NotificationDate = notificationdate
                                        }
                                        if let notificationid = responseData[i]["NotificationId"] as? Int {
                                            not.NotificationId = notificationid
                                        }
                                        if let notificationtype = responseData[i]["NotificationType"] as? Int {
                                            not.NotificationType = notificationtype
                                        }
                                        if let surveyid = responseData[i]["SurveyId"] as? Int {
                                            not.SurveyId = surveyid
                                        }
                                        if let campaignid = responseData[i]["CampaignId"] as? Int {
                                            not.CampaignId = campaignid
                                        }
                                        if let receiptid = responseData[i]["ReceiptId"] as? Int {
                                            not.ReceiptId = receiptid
                                        }
                                        if let receiptimage = responseData[i]["ReceiptImage"] as? String {
                                            not.ReceiptImage = receiptimage
                                        }
                                        self.notifications.append(not)
                                    }
                                    self.tableView.tableFooterView = nil
                                    self.tableView.reloadData()
                                    if self.notifications.isEmpty {
                                        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
                                        noDataLabel.text          = ""
                                        noDataLabel.textColor     = UIColor.black
                                        noDataLabel.textAlignment = .center
                                        self.tableView.backgroundView  = noDataLabel
                                        //self.tableView.separatorStyle  = .none
                                    }
                                }
                            }
                            break
                        case 401:
                            self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                            break
                        case 403, 404, 500:
                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "toMain")
                            break
                        default: break
                        }
                    }
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "Bağlantı hatası", withAction: "toMain")
                break
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = notifications.count
        return shouldShowLoadingCell ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath)
        let item = notifications[indexPath.row]
        configureCell(for: cell, with: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notificationItem = notifications[indexPath.row]
        
        switch notificationItem.NotificationType {
        case 1: // message
            if let messageNotificationView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "NotificationMessageViewController") as? NotificationMessageViewController {
                if let navigator = self.navigationController {
                    messageNotificationView.notificationID = notificationItem.NotificationId
                    navigator.pushViewController(messageNotificationView, animated: true)
                }
            }
            break
        case 2: // won
            if let wonNotificationView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "NotificationWonViewController") as? NotificationWonViewController {
                if let navigator = self.navigationController {
                    wonNotificationView.notificationID = notificationItem.NotificationId
                    navigator.pushViewController(wonNotificationView, animated: true)
                }
            }
            break
        case 3: // lost
            if let lostNotificationView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "NotificationLostViewController") as? NotificationLostViewController {
                if let navigator = self.navigationController {
                    lostNotificationView.notificationID = notificationItem.NotificationId
                    navigator.pushViewController(lostNotificationView, animated: true)
                }
            }
            break
        case 4: // questionnaire
            if let surveyView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SurveyViewController") as? SurveyViewController {
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
                ]
                let parameters: [String: AnyObject] = [
                    "NotificationId": notificationItem.NotificationId as AnyObject
                ]
                Alamofire.request(APIURL.GetNotificationDetail.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    debugPrint(response)
                    let result = response.result
                    let httpCode = response.response!.statusCode
                    
                    switch result {
                    case .success:
                        switch httpCode {
                        case 200:
                            if let data = result.value as? Dictionary<String, AnyObject> {
                                if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                    if let navigator = self.navigationController {
                                        if notificationItem.NotificationId != nil {
                                            if let surveyid = responseData["SurveyId"] as? Int {
                                                surveyView.surveyID = surveyid
                                                self.defaults.set(indexPath.row, forKey: "cellForRemove")
                                                navigator.pushViewController(surveyView, animated: true)
                                            }
                                        }
                                    }
                                }
                            }
                            break
                        case 401:
                            self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                            break
                        case 404, 500:
                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                            break
                        default: break
                        }
                        break
                    case .failure:
                        self.showAlert(withTitle: "Uyarı", withMessage: "Bağlantı hatası", withAction: "pop")
                        break
                    }
                }
            }
            break
        case 5: // campaign
            if let campaignView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "NotificationCampaignViewController") as? NotificationCampaignViewController {
                if let navigator = self.navigationController {
                    campaignView.notificationID = notificationItem.NotificationId
                    navigator.pushViewController(campaignView, animated: true)
                }
            }
            break
        default: break
        }
    }
    
    func configureCell(for cell: UITableViewCell, with item: Notifications) {
        let notLbl = cell.viewWithTag(1002) as! UILabel
        notLbl.attributedText = item.Notification.html2AttributedString
        
        let notDate = cell.viewWithTag(1001) as! UILabel
        notDate.text = item.NotificationDate.standardToDate
        let notImage = cell.viewWithTag(1000) as! UIImageView
        switch item.NotificationType {
        case 1:     // Message
            notImage.image = #imageLiteral(resourceName: "notificationInfo")
            break
        case 2:     // Won
            notImage.image = #imageLiteral(resourceName: "notificationWon")
            break
        case 3:     // Lost
            notImage.image = #imageLiteral(resourceName: "notificationLost")
            break
        case 4:     // Questionnaire
            notImage.image = #imageLiteral(resourceName: "notificationQuestionnaire")
            break
        case 5:     // Campaign
            notImage.image = #imageLiteral(resourceName: "notificationCampaign")
            break
        default: break
        }
    }
    
    private func fetchNextPage() {
        currentPage += 1
        getNotifications(withPage: currentPage)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (10 * currentPage) - 1 {
            fetchNextPage()
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
