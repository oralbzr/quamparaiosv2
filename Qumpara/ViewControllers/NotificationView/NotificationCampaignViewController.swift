//
//  NotificationCampaignViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/9/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

class NotificationCampaignViewController: UIViewController {
    
    @IBOutlet weak var notificationDate: UILabel!
    @IBOutlet weak var notificationText: UILabel!
    @IBOutlet weak var notificationTextDetail: UILabel!
    
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var notificationID: Int = 0
    let notDetail = NotificationDetail()
    
    let defaults = UserDefaults.standard
    
    var campaign = Campaigns()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.notificationTextDetail.isHidden = true
        
        if Reachability.isInternetAvailable() {
            getNotificationDetail(withID: notificationID)
        } else {
            self.showAlert(withTitle: "Uyarı", withMessage: "Lütfen internet bağlantınızı kontrol ediniz.", withAction: "")
        }
        // Do any additional setup after loading the view.
        
        indicator.center = view.center
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = true
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func getNotificationDetail(withID id:Int) {
        addBlurLayer()
        view.addSubview(indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "NotificationId": id as AnyObject
        ]
        Alamofire.request(APIURL.GetNotificationDetail.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            let httpCode = response.response!.statusCode
            
            switch result {
            case .success:
                switch httpCode {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                            
                            if let notificationdate = responseData["NotificationDate"] as? String {
                                self.notDetail.NotificationDate = notificationdate
                                self.notificationDate.text = self.notDetail.NotificationDate.standardToDate
                            }
                            if let notification = responseData["Notification"] as? String {
                                self.notDetail.Notification = notification
                                self.notificationText.text = self.notDetail.Notification
                            }
                            if let type = responseData["Type"] as? Int {
                                self.notDetail.NType = type
                            }
                            if let subtype = responseData["SubType"] as? Int {
                                self.notDetail.SubType = subtype
                            }
                            if let receiptid = responseData["ReceiptId"] as? Int {
                                self.notDetail.ReceiptId = receiptid
                            }
                            if let campaignid = responseData["CampaignId"] as? Int {
                                self.notDetail.CampaignId = campaignid
                            }
                            if let receiptimage = responseData["ReceiptImage"] as? String {
                                self.notDetail.ReceiptImage = receiptimage
                            }
                            if let couponid = responseData["CouponId"] as? Int {
                                self.notDetail.CouponId = couponid
                            }
                            if let descriptiondetailhtml = responseData["DescriptionDetailHTML"] as? String {
                                self.notDetail.DescriptionDetailHTML = descriptiondetailhtml
                                self.notificationTextDetail.isHidden = false
                                self.notificationTextDetail.text = descriptiondetailhtml
                            }
                            if let contactaddeddate = responseData["ContactAddedDate"] as? String {
                                self.notDetail.ContactAddedDate = contactaddeddate
                            }
                            if let contactformdescription = responseData["ContactFormDescription"] as? String {
                                self.notDetail.ContactFormDescription = contactformdescription
                            }
                        }
                    }
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                    self.removeBlurLayer()
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 404, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                default: break
                }
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "---", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    @IBAction func navigationButtonTapped(_ sender: UIButton) {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        let parameters: [String: AnyObject] = [
            "CampaignId": self.notDetail.CampaignId! as AnyObject
        ]
        Alamofire.request(APIURL.GetCampaignDetail.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            
            let result = response.result
            let statusCode = response.response!.statusCode
            
            switch result {
            case .success:
                switch statusCode {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                            if let description = responseData["CampaignDescription"] as? String {
                                self.campaign.Description = description
                            }
                            if let image = responseData["CampaignImageUrl"] as? String {
                                self.campaign.ResimUrl = image
                            }
                            if let title = responseData["Title"] as? String {
                                self.campaign.Title = title
                            }
                            if let subtitle = responseData["Subtitle"] as? String {
                                self.campaign.Subtitle = subtitle
                            }
                            if let remainingDay = responseData["RemainingDay"] as? String {
                                self.campaign.RemainingDay = remainingDay
                            }
                            if let link = responseData["CampaignShareLink"] as? String {
                                self.campaign.CampaignShareLink = link
                            }
                        }
                        let navInTab: UINavigationController = HHTabBarView.shared.referenceUITabBarController.viewControllers?.first as! UINavigationController
                        let storyboard = UIStoryboard(name: "Views", bundle: nil)
                        let destinationViewController = storyboard.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                        destinationViewController?.selectedCampaign = self.campaign
                        navInTab.pushViewController(destinationViewController!, animated: true)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
                        }
                    }
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 403, 404, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                default: break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Hata", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                break
            }
        }
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
