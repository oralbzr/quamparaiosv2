//
//  MainPageCell.swift

import UIKit

class MainPageCell: UITableViewCell {
    
    @IBOutlet weak var CampaignImage: UIImageView!
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var SubtitleLbl: UILabel!
    @IBOutlet weak var remainingDayLbl: UILabel!
    @IBOutlet weak var remainingView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        remainingView.layer.borderColor = UIColor.lightGray.cgColor
        remainingView.layer.borderWidth = 1.0
        remainingView.layer.cornerRadius = 14
    }
    
    func configureCell(titleTxt:String, subTitleTxt:String,remainingDayTxt:String){
        TitleLbl.text = titleTxt
        SubtitleLbl.text = subTitleTxt
        remainingDayLbl.text = remainingDayTxt
    }
    
    override func prepareForReuse() {
        self.CampaignImage.image = UIImage(named: "yükleniyor")
        self.TitleLbl.text = ""
        self.SubtitleLbl.text = ""
        self.remainingDayLbl.text = ""
    }
    
}
