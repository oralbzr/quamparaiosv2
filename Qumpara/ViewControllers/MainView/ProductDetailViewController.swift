//
//  ProductDetailVC.swift
//  Qumpara
//
//  Created by Banu on 8.08.2016.
//  Copyright © 2016 Nobium. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire


class ProductDetailViewController: UIViewController {
    
    @IBOutlet weak var campaignImage: UIImageView!
    @IBOutlet weak var titleLbl: UITextView!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var descriptionTxt: UITextView!
    @IBOutlet weak var remainingDayLbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var remainingView: UIView!
    @IBOutlet weak var promotionView: UIView!
    @IBOutlet weak var promotionTextfield: UITextField!
    @IBOutlet weak var btnPromotion: UIButton!
     @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isPromotion:Bool? = nil
    var selectedCampaign:Campaigns = Campaigns()
    let defaults = UserDefaults.standard
   
    override func viewDidLoad() {
        super.viewDidLoad()
       
        btnPromotion.setDisable()
        promotionView.isHidden = true
        if isPromotion == true{
            promotionView.isHidden = false
            var rect : CGRect = scrollView.frame
            rect.origin.y = rect.origin.y + promotionView.frame.height
            rect = CGRect(x: rect.origin.x, y: rect.origin.y, width: rect.width, height: rect.height - promotionView.frame.height)
            scrollView.frame = rect
            addToolBar(textField: promotionTextfield)
        }
        
    
        remainingView.layer.borderColor = UIColor.lightGray.cgColor
        remainingView.layer.borderWidth = 1.0
        remainingView.layer.cornerRadius = 14
        remainingDayLbl.text=selectedCampaign.RemainingDay
        
        titleLbl.text = selectedCampaign.Title
        let size = titleLbl.sizeThatFits(self.titleLbl.bounds.size)
        var frame = self.titleLbl.frame
        frame.size.height = size.height
        self.titleLbl.frame = frame
     
    
        subTitleLbl.text =  selectedCampaign.Subtitle
        
        descriptionTxt.attributedText =  selectedCampaign.Description.html2AttributedString
        let size3 = descriptionTxt.sizeThatFits(self.descriptionTxt.bounds.size)
        var frame3 = self.descriptionTxt.frame
        frame3.size.height = size3.height
        self.descriptionTxt.frame = frame3
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height:descriptionTxt.frame.origin.y + descriptionTxt.frame.height)

        setAllInfo()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    func setAllInfo() {
        DispatchQueue.main.async {
            
            self.campaignImage.sd_setImage(with: URL(string: self.selectedCampaign.ResimUrl), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(),
                                           completed: {(image,error,cacheType,url) in
                                            if error != nil {
                                                print(error ?? "campaign image default error")
                                                self.campaignImage.sd_setImage(with: URL(string: self.selectedCampaign.ResimUrl), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(), completed: {
                                                    (image,error,cacheType,url) in
                                                    if error != nil {
                                                        self.campaignImage.image = UIImage(named: "")
                                                    }
                                                })
                                            }
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    @IBAction func shareCampaign(_ sender: AnyObject) {
        
        let activityVC = UIActivityViewController(activityItems: ["Alışveriş fişlerimi Qumpara'da kazanca çeviriyorum. Sen de hemen bu kampanya ile kazanmaya başlayabilirsin:\(selectedCampaign.CampaignShareLink)"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
       
        if let code = promotionTextfield.text , code.count > 0 {
            btnPromotion.setEnable()
        }
      }
    
    @IBAction func textBeginEditing(_ sender: UITextField) {
        let code = promotionTextfield.text!
        if code == "Promosyon kodu"{
            promotionTextfield.text=""
        }
        
        if promotionTextfield.text != "" {
            btnPromotion.setEnable()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 10
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func sendPromotionCode() {
        self.indicator.isHidden=false
        self.indicator.startAnimating()
        promotionTextfield.resignFirstResponder()
        if let code = promotionTextfield.text , code.count > 0 {
            if Reachability.isInternetAvailable() {
               
                    let headers: HTTPHeaders = [
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
                    ]
                    let parameters: [String: AnyObject] = [
                        "PromotionCode": code as AnyObject,
                        "CampaignId": selectedCampaign.CampaignId as AnyObject
                    ]
                    Alamofire.request(APIURL.ApplyPromotionCode.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                        debugPrint(response)
                        let result = response.result
                        self.indicator.stopAnimating()
                        switch result {
                        case .success:
                            // TODO
                            if let data = result.value as? Dictionary<String, AnyObject> {
                                print(data["response"]!["ErrorCode"])
                                if let error  = data["response"]!["ErrorCode"] , error != nil{
                                    
                                    if let errorMessage  = data["response"]!["ErrorDescription"] ,errorMessage != nil{
                                        let alert = UIAlertController(title: "Uyarı", message:errorMessage as? String, preferredStyle: UIAlertControllerStyle.alert)
                                       alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                                       self.present(alert, animated: true, completion: nil)
                                    }
                                }else  if let message  = data["response"]!["Description"] ,message != nil{
                                    let alert = UIAlertController(title: "Kupon Kullanımı", message:message as? String, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                            print("success")
                            break
                        case .failure:
                            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            break
                        }
                    }
                }
            }else{
                let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }

    
    
        
}


extension NSAttributedString
{
    func getHTMLString() -> String
    {
        var htmlText = "";
        let documentAttributes = [NSAttributedString.DocumentAttributeKey.documentType: NSAttributedString.DocumentType.html]
        do {
            let htmlData = try self.data(from: NSMakeRange(0, self.length), documentAttributes:documentAttributes)
            if let htmlString = String(data:htmlData, encoding:String.Encoding.utf8) {
                htmlText = htmlString
            }
            return htmlText
        }
        catch {
            print("error creating HTML from Attributed String")
            return ""
        }
    }
}
extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}


