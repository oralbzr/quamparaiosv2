//
//  AdCellTableViewCell.swift
//  Qumpara
//
//  Created by Oral Bozkır on 10/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class AdCellTableViewCell: UITableViewCell {
    @IBOutlet weak var AdImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        AdImage.layer.borderColor = UIColor.black.cgColor
        AdImage.layer.borderWidth = 1.0
    }

    override func prepareForReuse() {
        self.AdImage.image = UIImage(named: "yükleniyor")
        
    }
   

}
