//
//  WebInfoViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 18/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class WebInfoViewController: UIViewController , UIWebViewDelegate{
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var url: String?
   
    override func viewDidLoad(){
        super.viewDidLoad()
        self.indicator.center=self.webView.center
        self.webView.delegate = self
        
         if let urlStr = self.url , urlStr.count > 0 {
            self.webView.loadRequest(URLRequest(url: URL(string: urlStr)!))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.indicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.indicator.stopAnimating()
    }
    
    
    @IBAction func closeView(_ sender: Any) {
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func handleSwipe(_ sender: Any) {
        if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
}
