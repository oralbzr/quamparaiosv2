//
//  MainViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 09/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import Alamofire
import FirebaseInstanceID

class MainViewController: UIViewController ,DataManagerDelegate,UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    
    var campaigns:[Campaigns] = []
    var filteredCampaigns:[Campaigns] = []
    var notification:[Notifications] = []
    var selectedCampaign:Campaigns = Campaigns()
    var indexPath: IndexPath?
    
    let defaults = UserDefaults.standard
    
    var tableCells:[MainPageCell] = []
    var tableAdCells:[AdCellTableViewCell] = []
    
    let refreshControl = UIRefreshControl()
    
    var refreshLoadingView : UIView!
    var isRefreshIconsOverlap = false
    var isRefreshAnimating = false
    
    
    var badgeCount = 0
    
    var reload = false
    
    var isNot = true
    
    var qumpara: Qumpara = Qumpara()
    var coupon: [Coupon] = []
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var refreshIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var dataManager = DataManager()
    
    override func viewDidLoad() {
        
        updateVersionIfNeeded()
        updateRegistration()
        
        // self.view.isUserInteractionEnabled=true
        HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
        dataManager.delegate = self
        self.indicator.center=self.view.center
        self.loadCampaigns()
        self.searchBar.sizeToFit()
        self.searchBar.textField?.textColor = UIColor.white
        //self.searchBar.textField?.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.00)
        self.tableView.allowsSelection = true
        
        self.refreshControl.tintColor = UIColor.gray
        
        self.refreshControl.backgroundColor = UIColor.clear
        self.refreshControl.tintColor = UIColor.clear
        self.refreshControl.addSubview(UIView(frame: self.refreshControl.bounds))
        
        self.refreshControl.addTarget(self, action:#selector(MainViewController.refreshTable), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl)
        
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    func updateRegistration() {
        
        
        if defaults.object(forKey: "DeviceToken") != nil {
            
            if self.defaults.object(forKey: "access_token") != nil {
                let headersRegistration : HTTPHeaders = [
                    "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                ]
                let parametersRegistration : [String: String] = [
                    "RegistrationId": "\(self.defaults.object(forKey: "DeviceToken")!)"
                ]
                Alamofire.request(APIURL.UpdateRegistration.rawValue, method: .post, parameters: parametersRegistration, encoding: JSONEncoding.default, headers: headersRegistration).responseJSON { response in
                    debugPrint(response)
                    print(parametersRegistration)
                    
                    let result = response.result
                    switch result {
                    case .success:
                        let statusCodeForRegistration = response.response!.statusCode
                        switch statusCodeForRegistration {
                        case 200:
                            print("success!")
                            break
                        default: break
                        }
                        break
                    case .failure:
                        break
                    }
                }
            }
        } else {
            if self.defaults.object(forKey: "access_token") != nil {
                if let token = FIRInstanceID.instanceID().token() {
                    let headersRegistration : HTTPHeaders = [
                        "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
                    ]
                    let parametersRegistration : [String: String] = [
                        "RegistrationId": "\(token)"
                    ]
                    Alamofire.request(APIURL.UpdateRegistration.rawValue, method: .post, parameters: parametersRegistration, encoding: JSONEncoding.default, headers: headersRegistration).responseJSON { response in
                        debugPrint(response)
                        print(parametersRegistration)
                        
                        let result = response.result
                        switch result {
                        case .success:
                            let statusCodeForRegistration = response.response!.statusCode
                            switch statusCodeForRegistration {
                            case 200:
                                print("success!")
                                print(parametersRegistration)
                                break
                            default: break
                            }
                            break
                        case .failure:
                            break
                        }
                    }
                }
            }
        }
    }
    
    func updateVersionIfNeeded() {
        if defaults.object(forKey: "access_token") != nil {
            let headersUpdate: HTTPHeaders = [
                "Authorization" : "Bearer \(self.defaults.object(forKey: "access_token")!)"
            ]
            let parametersUpdate: [String: AnyObject] = [
                "OperationSystem" : "IOS" as AnyObject,
                "VersionName" : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
            ]
            
            Alamofire.request(APIURL.MemberVersionUpdate.rawValue, method: .post, parameters: parametersUpdate, encoding: JSONEncoding.default, headers: headersUpdate).responseJSON { response in
                debugPrint(response)
                
                let result = response.result
                switch result {
                case .success:
                    let statusCode = response.response!.statusCode
                    switch statusCode {
                    case 200:
                        print("success")
                        break
                    case 401, 500:
                        self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                        break
                    case 403, 404, 405, 406:
                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                        break
                    default: break
                    }
                    break
                case .failure:
                    self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "")
                    break
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // self.navigationItem.rightBarButtonItem?.badgeValue = "0"
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let noidNot = defaults.value(forKey:"noidNot") as? Int
        if let data = defaults.object(forKey: "ReadNotifications") as? Data {
            self.badgeCount = self.notification.count - (NSKeyedUnarchiver.unarchiveObject(with: data) as! Set<String>).count - noidNot!
        }
        self.tableView.flashScrollIndicators()
        
        let newNot = delegate.pushNotifications
        var badgeNumber = 0
        if newNot.count + self.badgeCount > 0 {
            badgeNumber = newNot.count + self.badgeCount
        }
        delegate.badgeCount = badgeNumber
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func mainPageCampaignCompleted(_ result: [String:Any], withError errorDescription: NSString ,withCampaigns campaigns: [Campaigns])
    {
        self.indicator.stopAnimating()
        self.refreshIndicator.stopAnimating()
        print(errorDescription)
        if  errorDescription != "" {
            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin" , preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.tableView.allowsSelection = true
            self.refreshControl.endRefreshing()
        }else{
            self.tableView.isHidden=false
            self.campaigns = campaigns
            self.filteredCampaigns=campaigns
            tableView!.delegate = self
            tableView!.dataSource = self
            self.tableView.allowsSelection = true
            tableView.reloadData();
            self.refreshControl.endRefreshing()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HHTabBarView.shared.isHidden = false
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func loadCampaigns() {
        
        if Reachability.isInternetAvailable() {
            dataManager.sendMainPageCampaign()
            if self.reload == false {
                indicator.startAnimating()
            }
        }else{
            let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @objc func refreshTable(){
        if Reachability.isInternetAvailable() {
            self.reload = true
            self.tableView.allowsSelection = false
            refreshIndicator.startAnimating()
            self.loadCampaigns()
        } else {
            let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredCampaigns.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let campaign = self.filteredCampaigns[indexPath.row] as Campaigns
        
        
        
        if campaign.IsAdvertisement == true{
            return CGFloat(Utility.scaledHeight(177))
        }
        return CGFloat(Utility.scaledHeight(230))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let campaign = self.filteredCampaigns[indexPath.row] as Campaigns
        
        if campaign.IsAdvertisement == false{
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "MainPageCell", for: indexPath) as! MainPageCell
            
            cell.configureCell(titleTxt: campaign.Title, subTitleTxt: campaign.Subtitle , remainingDayTxt: campaign.RemainingDay)
            cell.CampaignImage.sd_setImage(with: URL(string: campaign.ResimUrl), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(),
                                           completed: {(image,error,cacheType,url) in
                                            if error != nil {
                                                print(error!)
                                                cell.CampaignImage.image = UIImage(named: "")
                                            }
            })
            return cell
        }else{
            let adCell = self.tableView.dequeueReusableCell(withIdentifier: "AdCellTableViewCell", for: indexPath) as! AdCellTableViewCell
            adCell.AdImage.sd_setImage(with: URL(string: campaign.ResimUrl), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(),
                                       completed: {(image,error,cacheType,url) in
                                        if error != nil {
                                            print(error!)
                                            adCell.AdImage.image = UIImage(named: "")
                                        }
            })
            return adCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexPath = tableView.indexPathForSelectedRow!
        let campaigns = self.filteredCampaigns[indexPath.row] as Campaigns
        searchBar.resignFirstResponder()
        self.searchBar.text = ""
        self.selectedCampaign = campaigns
        self.searchBar.isHidden=true
        self.btnSearch.isHidden=false
        btnCancel.isHidden = true
        self.tableView.deselectRow(at: self.indexPath!, animated: false)
        
            if self.selectedCampaign.IsAdvertisement == true {
                if self.selectedCampaign.IsOpenInApp == true {
                    if let adDetailView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "WebInfoViewController") as? WebInfoViewController {
                        if let navigator = self.navigationController {
                            adDetailView.url=self.selectedCampaign.AdvertisementURL
                            navigator.pushViewController(adDetailView, animated: true)
                        }
                    }
                }else{
                    if let productDetailView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController {
                        if let navigator = self.navigationController {
                            productDetailView.selectedCampaign=self.selectedCampaign
                            productDetailView.isPromotion=false
                            navigator.pushViewController(productDetailView, animated: true)
                        }
                    }
                }
            }else{
                if defaults.object(forKey: "access_token") == nil {
                    if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                        if let navigator = self.navigationController {
                            navigator.pushViewController(signUpView, animated: true)
                        }
                    }
                }else{
                if let productDetailView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController {
                    if let navigator = self.navigationController {
                        productDetailView.selectedCampaign=self.selectedCampaign
                        productDetailView.isPromotion=false
                        navigator.pushViewController(productDetailView, animated: true)
                    }
                }
                }
            }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        self.filteredCampaigns.removeAll()
        if (searchBar.text?.isEmpty)! {
            self.filteredCampaigns=self.campaigns
        }else{
            if self.campaigns.count > 0 {
                for i in 0...self.campaigns.count - 1 {
                    let campaign = self.campaigns[i] as Campaigns
                    if campaign.Title.range(of: searchBar.text!, options: .caseInsensitive) != nil || campaign.Subtitle.range(of: searchBar.text!, options: .caseInsensitive) != nil || campaign.Description.range(of: searchBar.text!, options: .caseInsensitive) != nil {
                        self.filteredCampaigns.append(campaign)
                    }
                }
            }
            if self.filteredCampaigns.count == 0{
                //    self.filteredCampaigns=self.campaigns
            }
        }
        self.searchBar.isHidden=true
        self.btnSearch.isHidden=false
        btnCancel.isHidden = true
        tableView.reloadData();
        tableView.reloadInputViews();
        searchBar.resignFirstResponder()
        self.searchBar.text = ""
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        self.filteredCampaigns.removeAll()
        self.filteredCampaigns=self.campaigns
        tableView.reloadData();
        self.searchBar.isHidden=true
        self.btnSearch.isHidden=false
        btnCancel.isHidden = true
        searchBar.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.filteredCampaigns.removeAll()
        if (searchBar.text?.isEmpty)! {
            self.filteredCampaigns=self.campaigns
        }else{
            if self.campaigns.count > 0 {
                for i in 0...self.campaigns.count - 1 {
                    let campaign = self.campaigns[i] as Campaigns
                    if campaign.Title.range(of: searchBar.text!, options: .caseInsensitive) != nil || campaign.Subtitle.range(of: searchBar.text!, options: .caseInsensitive) != nil || campaign.Description.range(of: searchBar.text!, options: .caseInsensitive) != nil {
                        self.filteredCampaigns.append(campaign)
                    }
                }
            }
            if self.filteredCampaigns.count == 0{
                //     self.filteredCampaigns=self.campaigns
            }
        }
        
        tableView.reloadData();
        tableView.reloadInputViews();
        
    }
    
    @IBAction func showSearchBar(_ sender: AnyObject) {
        self.searchBar.isHidden = false
        self.btnSearch.isHidden=true
        btnCancel.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func closeSearch(_ sender: AnyObject) {
        self.filteredCampaigns.removeAll()
        self.filteredCampaigns=self.campaigns
        tableView.reloadData();
        self.searchBar.isHidden=true
        self.btnSearch.isHidden=false
        btnCancel.isHidden = true
        searchBar.resignFirstResponder()
        self.searchBar.text = ""
    }
    
}



extension UISearchBar {
    /// Return text field inside a search bar
    var textField: UITextField? {
        let subViews = subviews.flatMap { $0.subviews }
        guard let textField = (subViews.filter { $0 is UITextField }).first as? UITextField else { return nil
        }
        return textField
    }
}

