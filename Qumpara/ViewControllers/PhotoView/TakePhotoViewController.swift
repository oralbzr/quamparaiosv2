//
//  TakePhotoViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 03/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Alamofire
import SwiftyJSON
import CoreLocation

class TakePhotoViewController: UIViewController , AVCapturePhotoCaptureDelegate,CLLocationManagerDelegate{

   
    @IBOutlet weak var cameraView: UIView!
     @IBOutlet weak var indicator: UIActivityIndicatorView!
     @IBOutlet weak var controlView: UIView!
     @IBOutlet weak var lblHelpText: UILabel!
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var reTake: UIButton!
    @IBOutlet weak var send: UIButton!
    @IBOutlet weak var photo: UIButton!
    @IBOutlet weak var flashBtn: UIButton!
    @IBOutlet weak var coverView: UIView!
    
    @IBOutlet var swipe: UISwipeGestureRecognizer!
    var locationManager:CLLocationManager = CLLocationManager()
    var latitude = 0.0
    var longitude = 0.0
    
    var cameraV: UIView!
    var session: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var iv: UIImageView!
    var previewImage: UIImage!
    var image:UIImage?
    var oneTime = true
    var denied:Bool? = nil
    var finish = false
    var cam:AVCaptureDevice?
    let defaults = UserDefaults.standard
    var isFlashOn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHelpText.sizeToFit()
        lblHelpText.center=self.view.center
        lblHelpText.frame = CGRect(x: lblHelpText.frame.origin.x, y:30.0, width: lblHelpText.frame.size.width, height: lblHelpText.frame.size.height)
         self.photo.layer.cornerRadius = self.photo.frame.size.width/2.0
        self.photo.layer.borderColor = UIColor.white.cgColor
        coverView.layer.borderWidth=2.0
        coverView.layer.borderColor=UIColor.white.cgColor
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        let frm = CGRect(x: self.view.frame.origin.x , y: self.view.frame.origin.y , width: self.view.frame.width, height: self.view.frame.height - 0)
        self.cameraV = UIView(frame: frm)
        self.view.addSubview(cameraV)
        self.view.sendSubview(toBack: cameraV)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //self.previewLayer!.frame = cameraView.bounds
        checkForMovieCaptureAccess(andThen: self.reallyStart)
         self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        super.viewWillAppear(animated)
        
    }
    
    @available(iOS 10.0, *)
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let buff = photoSampleBuffer {
            if let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buff, previewPhotoSampleBuffer: previewPhotoSampleBuffer) {
                
                self.image = UIImage(data: data)
                if self.cam!.hasTorch {
                    do {
                   //     try cam!.lockForConfiguration()
                     //   cam!.torchMode = AVCaptureDevice.TorchMode.off
                     //   cam!.unlockForConfiguration()
                    }
                    catch let error {
                        print(error)
                    }
                }
                self.allButtonsEnable()
                
                
                 self.showButtons()
           
            }
            
            
        }
        self.allButtonsEnable()
    }
    
    @available(iOS 10.0, *)
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
        
        DispatchQueue.main.async {
            
            if self.session != nil {
                self.session.stopRunning()
                self.session = nil
            }
        }
        
    }
    
    func reallyStart() {
        if self.session != nil && self.session.isRunning {
            self.session.stopRunning()
            self.previewLayer.removeFromSuperlayer()
            self.session = nil
            return
        }
        
        self.session = AVCaptureSession()
        
        do {
            self.session.beginConfiguration()
            
            let preset = AVCaptureSession.Preset.high
            guard self.session.canSetSessionPreset(self.session.sessionPreset) else {return}
            self.session.sessionPreset = preset
            
            if #available(iOS 10.0, *) {
                let output = AVCapturePhotoOutput()
                guard self.session.canAddOutput(output) else {return}
                self.session.addOutput(output)
            } else if #available(iOS 9.0, *) {
                let output = AVCaptureStillImageOutput()
                output.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                guard self.session.canAddOutput(output) else {return}
                self.session.addOutput(output)
            }
            self.session.commitConfiguration()
        }
        self.cam = AVCaptureDevice.default(for: AVMediaType.video)
        if cam!.hasTorch {
            do {
                 try cam!.lockForConfiguration()
                if (isFlashOn == false){
                    cam!.torchMode = AVCaptureDevice.TorchMode.off
                }else{
                    try cam!.lockForConfiguration()
                    cam!.torchMode = AVCaptureDevice.TorchMode.on
                }
                cam!.unlockForConfiguration()
            }
            catch let error {
                print(error)
            }
        }
        guard let input = try? AVCaptureDeviceInput(device:cam!) else {return}
        self.session.addInput(input)
        
        let lay = AVCaptureVideoPreviewLayer(session:self.session)
        self.previewLayer = lay
        
        self.cameraV.layer.addSublayer(self.previewLayer)
        self.previewLayer!.frame = self.cameraV.bounds
        self.previewLayer.videoGravity = AVLayerVideoGravity.resize
        
        
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
        self.session.startRunning()
    }
    
    @IBAction func closeCamera(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func backToMain(_ sender: Any) {
       self.changeTab()
    }
    
    
    @IBAction func CameraToMain(_ sender: AnyObject) {
       self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func swipeHandle(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    @IBAction func takePhoto(_ sender: AnyObject) {
        self.allButtonsDisable()
        guard self.session != nil && self.session.isRunning else {
            self.allButtonsEnable()
            return
        }
        if #available(iOS 10.0, *) {
            let settings = AVCapturePhotoSettings()
            //settings.flashMode = .off
            settings.isAutoStillImageStabilizationEnabled = true
            // let's also ask for a preview image
            let pbpf = settings.availablePreviewPhotoPixelFormatTypes[0]
            let len = max(self.previewLayer.bounds.width, self.previewLayer.bounds.height)
            settings.previewPhotoFormat = [
                kCVPixelBufferPixelFormatTypeKey as String : pbpf,
                kCVPixelBufferWidthKey as String : len,
                kCVPixelBufferHeightKey as String : len
            ]
            
            guard let output = self.session.outputs[0] as? AVCapturePhotoOutput else {
                self.allButtonsEnable()
                return
            }
            // how to deal with orientation; stolen from Apple's AVCam example!
            if let conn = output.connection(with: AVMediaType.video) {
                
                let orientation =  AVCaptureVideoOrientation.portrait
                //UIDevice.current.orientation.videoOrientation!
                conn.videoOrientation = orientation
            }
            output.capturePhoto(with: settings, delegate: self)
        } else {
            guard let stillImageOutput:AVCaptureStillImageOutput = self.session.outputs[0] as? AVCaptureStillImageOutput else {
                self.allButtonsEnable()
                return
            }
            if let videoConnection = stillImageOutput.connection(with: AVMediaType.video){
                videoConnection.videoOrientation = AVCaptureVideoOrientation.portrait
                stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: {
                    (sampleBuffer, error) in
                    
                    if sampleBuffer != nil {
                        
                        let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer!)
                        let dataProvider  = CGDataProvider(data: imageData! as CFData)
                        let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent:   CGColorRenderingIntent.defaultIntent)//CGColorRenderingIntent.RenderingIntentDefault)
                        
                        self.image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.right)
                        /*
                        if self.cam!.hasTorch {
                            do {
                              try self.cam!.lockForConfiguration()
                                self.cam!.torchMode = AVCaptureDevice.TorchMode.off
                                self.cam!.unlockForConfiguration()
                            }
                            catch let error {
                                print(error)
                            }
                        }
                        */
                        DispatchQueue.main.async {
                            if self.session != nil {
                                self.session.stopRunning()
                                self.session = nil
                            }
                        }
                        self.showButtons()
                     
                        
                    }
                    
                    
                })
            }
        }
        self.allButtonsEnable()
        
    }
    
    func allButtonsEnable(){
        self.reTake.isEnabled = true
        self.cancel.isEnabled = true
        self.photo.isEnabled = true
        self.send.isEnabled = true
        self.swipe.isEnabled = true
    }
    
    func allButtonsDisable(){
        self.reTake.isEnabled = true
        self.cancel.isEnabled = true
        self.photo.isEnabled = true
        self.send.isEnabled = true
        self.swipe.isEnabled = false
    }
    
    
    func showButtons(){
        self.reTake.isHidden = false
        self.cancel.isHidden = false
        self.photo.isHidden = true
        self.send.isHidden = false
        self.lblHelpText.isHidden=true
        self.controlView.isHidden=false
        self.flashBtn.isHidden=true
        self.coverView.isHidden=true
    }
    
    func hideButtons(){
        self.reTake.isHidden = true
        self.cancel.isHidden = true
        self.photo.isHidden = false
        self.send.isHidden = true
        self.lblHelpText.isHidden=false
        self.controlView.isHidden=true
        self.flashBtn.isHidden=false
        self.coverView.isHidden=false
    }
    
    
    
    @IBAction func confirm(_ sender: AnyObject) {
        self.uploadPhoto()
    }
    
    @IBAction func reTake(_ sender: AnyObject) {
        self.hideButtons()
        self.image=nil;
        checkForMovieCaptureAccess(andThen: self.reallyStart)
       
    }
    
    
    @IBAction func openFlash(_ sender: UIButton!) {
       
        if sender.tag == 0 {
            isFlashOn = true
            sender.setBackgroundImage(UIImage(named: "flash-white"), for: UIControlState.normal)
            self.toggleTorch(on: true)
            sender.tag = 1
        }else{
            isFlashOn = false
            sender.setBackgroundImage(UIImage(named: "flash-black"), for: UIControlState.normal)
            self.toggleTorch(on: false)
            sender.tag = 0
        }
        
    }
    
     func toggleTorch(on: Bool) {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video)
            else {return}
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if on == true {
                    device.torchMode = .on
                } else {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
    
    func uploadPhoto() {
        self.indicator.isHidden=false;
        self.indicator .startAnimating()
        
        if self.longitude == 0.0 && self.latitude == 0.0 && self.denied != true {
            finish = true
            return
        }
        
        if Reachability.isInternetAvailable() {
            allButtonsDisable()
           
            let imageData = UIImageJPEGRepresentation(self.image!, 0.5)
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer \(defaults.object(forKey: "access_token")!)"
            ]
            
            Alamofire.upload(multipartFormData: {(multipartFormData) in
                
                multipartFormData.append(imageData!, withName: "file", fileName: "imageFile.jpg", mimeType: "image/jpg")
                multipartFormData.append(String(self.latitude).data(using: String.Encoding.utf8)!, withName: "Latitude")
                multipartFormData.append(String(self.longitude).data(using: String.Encoding.utf8)!, withName: "Longitude")
                
                
            }, to: APIURL.UploadImage.rawValue,headers: headers, encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        guard let resultValue = response.result.value else {
                            let alert = UIAlertController(title: "Uyarı", message: "Bilinmeyen bir hata oluştu tekrarı halinde lütfen iletişime geçiniz" , preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.allButtonsEnable()
                            self.indicator.stopAnimating()
                            return
                        }
                       
                        let result = response.result
                        
                        let httpCode = response.response?.statusCode
                        switch httpCode! {
                        case 200:
                            
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                          
                            if let data = result.value as? Dictionary<String, AnyObject> {
                                if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                                    self.showAlert(withTitle: "Başarılı", withMessage: "\(responseData["Description"]!)", withAction: "toMain")
                                }
                            }
                          
                            break
                        case 401:
                            self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.allButtonsEnable()
                            break
                        case 404:
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                             self.allButtonsEnable()
                        
                        case 403, 405, 500:
                            self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                             self.allButtonsEnable()
                            break
                        default: break
                        }
                    }
                case .failure(let encodingError):
                    let alert = UIAlertController(title: "Uyarı", message: "Bilinmeyen bir hata oluştu tekrarı halinde lütfen iletişime geçiniz" , preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.allButtonsEnable()
                    self.indicator.stopAnimating()
                    return
                }
                
            })
            
        } else {
            self.indicator.stopAnimating()
            let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.allButtonsEnable()
            
        }

        
    }
    
    func changeTab() {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
        HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
    }
    
    func checkForMovieCaptureAccess(andThen f:(()->())? = nil) {
        let status = AVCaptureDevice.authorizationStatus(for:AVMediaType.video)
        switch status {
        case .authorized:
            f?()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for:AVMediaType.video) { granted in
                if granted {
                    DispatchQueue.main.async {
                        f?()
                    }
                }
            }
        case .restricted:
            // do nothing
            break
        case .denied:
            let alert = UIAlertController(
                title: "Need Authorization",
                message: "Wouldn't you like to authorize this app " +
                "to use the camera?",
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(
                title: "No", style: .cancel))
            alert.addAction(UIAlertAction(
            title: "OK", style: .default) {
                _ in
                let url = URL(string:UIApplicationOpenSettingsURLString)!
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    // Fallback on earlier versions
                }
            })
            UIApplication.shared.delegate!.window!!.rootViewController!.present(alert, animated:true)
        }
        
       
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        self.latitude = locations[0].coordinate.latitude
        self.longitude = locations[0].coordinate.longitude
        
        if oneTime && finish {
            oneTime = false
            self.uploadPhoto()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
            self.denied = false
            locationManager.startUpdatingLocation()
        } else {
            self.denied = true
            if finish {
                self.uploadPhoto()
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    
}






