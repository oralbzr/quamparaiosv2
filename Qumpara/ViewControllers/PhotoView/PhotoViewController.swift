//
//  PhotoViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 10/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit
import ImageSlideshow

class PhotoViewController: UIViewController {
    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var btnTakePhoto: UIButton!
    //image slideshowda gösterilecek resimler
    let source = [ImageSource(imageString:"PV1")!,ImageSource(imageString:"PV2")!]
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnTakePhoto.layer.borderColor = UIColor(red:0.98, green:0.50, blue:0.43, alpha:1.00).cgColor
        let slideWidth = CGFloat(Utility.scaledWidth(300.0))
        let slideHeight = CGFloat(Utility.scaledHeight(442.0))
        slideShow.frame = CGRect(x: 10, y: 20 , width: slideWidth, height: slideHeight)
        //Image slideshow için gerekli ayarlar
        self.slideShow.circular = false;
        self.slideShow.scrollView.bounces = false
        self.slideShow.scrollView.alwaysBounceVertical = false
        //self.slideShow.backgroundColor = UIColor.white
        self.slideShow.pageControlPosition = PageControlPosition.insideScrollView
        self.slideShow.pageControl.currentPageIndicatorTintColor = UIColor(red: 0.98, green: 0.50, blue: 0.43, alpha: 1.0)
        self.slideShow.pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.slideShow.contentScaleMode = UIViewContentMode.scaleToFill
        self.slideShow.zoomEnabled = false
        self.slideShow.setImageInputs(source)
        self.slideShow.scrollView.isDirectionalLockEnabled = true
        self.slideShow.scrollView.alwaysBounceHorizontal = true
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        HHTabBarView.shared.isHidden = false
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        if let takePhoto = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "TakePhotoViewController") as? TakePhotoViewController {
            self.modalPresentationStyle=UIModalPresentationStyle.fullScreen
            self.present(takePhoto, animated: true, completion: nil)
        }
    }
}
