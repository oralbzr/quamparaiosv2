//
//  PromotionViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 17/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import Alamofire

class PromotionViewController:  UIViewController ,DataManagerDelegate,UITableViewDelegate, UITableViewDataSource {
   
    var promotions:[Campaigns] = []
    var selectedPromotions:Campaigns = Campaigns()
    var indexPath: IndexPath?
    let defaults = UserDefaults.standard
    var tableCells:[MainPageCell] = []
    let refreshControl = UIRefreshControl()
    
    var refreshLoadingView : UIView!
    var isRefreshIconsOverlap = false
    var isRefreshAnimating = false

    var reload = false
    var isNot = true
    
   
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var UILabel: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
     @IBOutlet weak var refreshIndicator: UIActivityIndicatorView!
    
    var dataManager = DataManager()
    
    override func viewDidLoad() {
        dataManager.delegate = self
        self.indicator.center=self.view.center
        self.loadPromotions()
      
        self.tableView.allowsSelection = true
       
        self.refreshControl.tintColor = UIColor.gray
        
        self.refreshControl.backgroundColor = UIColor.clear
        self.refreshControl.tintColor = UIColor.clear
        self.refreshControl.addSubview(UIView(frame: self.refreshControl.bounds))
        
        self.refreshControl.addTarget(self, action:#selector(PromotionViewController.refreshTable), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl)
       
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadCells() {
        tableCells.removeAll()
        for _ in 0..<self.promotions.count {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "MainPageCell") as! MainPageCell
            tableCells.append(cell)
        }
       
        tableView!.reloadData();
    }
    

    func promotionsCompleted(_ result: [String:Any], withError errorDescription: NSString ,withPromotions promotions: [Campaigns])
    {
        self.indicator.stopAnimating()
        self.refreshIndicator.stopAnimating()
        print(errorDescription)
        if  errorDescription != "" {
            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin" , preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.tableView.allowsSelection = true
            self.refreshControl.endRefreshing()
        }else{
            self.tableView.isHidden=false
            self.promotions = promotions
            tableView!.delegate = self
            tableView!.dataSource = self
            self.tableView.allowsSelection = true
            self.loadCells()
             self.refreshControl.endRefreshing()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HHTabBarView.shared.isHidden = true
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
         HHTabBarView.shared.isHidden = false
    }
    
    func loadPromotions() {
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
            }
        }else{
        if Reachability.isInternetAvailable() {
            if !refreshIndicator.isAnimating {
              indicator.startAnimating()
            }
            dataManager.getPromotions()
        }else{
                    let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
            }
            }
    }
    
    @objc func refreshTable(){
        if Reachability.isInternetAvailable() {
            self.reload = true
            self.tableView.allowsSelection = false
             refreshIndicator.startAnimating()
            self.loadPromotions()
        } else {
            let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.promotions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return CGFloat(Utility.scaledHeight(230))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let campaign = self.promotions[indexPath.row] as Campaigns
        
            let cell = tableCells[indexPath.row]
        cell.configureCell(titleTxt: campaign.Title, subTitleTxt: campaign.Subtitle, remainingDayTxt: campaign.RemainingDay)
            cell.CampaignImage.sd_setImage(with: URL(string: campaign.ResimUrl), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(),
                                           completed: {(image,error,cacheType,url) in
                                            if error != nil {
                                                print(error!)
                                                cell.CampaignImage.image = UIImage(named: "")
                                            }
            })
            return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexPath = tableView.indexPathForSelectedRow!
        let campaigns = self.promotions[indexPath.row] as Campaigns
        
        self.selectedPromotions = campaigns
        
        self.tableView.deselectRow(at: self.indexPath!, animated: false)
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
            }
        }else{
            if let productDetailView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController {
                if let navigator = self.navigationController {
                    productDetailView.selectedCampaign=self.selectedPromotions
                    productDetailView.isPromotion=true
                    navigator.pushViewController(productDetailView, animated: true)
                }
            }
        }
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
        }
    }
}
