//
//  SupportViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/15/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import WebKit
import MessageUI
import Alamofire

struct Support {
    var name: String
    var url: String
}

class SupportViewController: UIViewController, WKUIDelegate, MFMailComposeViewControllerDelegate, WKNavigationDelegate {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var webView: UIView!
    @IBOutlet weak var wkwebView: WKWebView!
    @IBOutlet weak var supportTabName: UILabel!
    
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var sssView: UIView!
    @IBOutlet weak var userAgreementView: UIView!
    @IBOutlet weak var privacyPolicyView: UIView!
    
    @IBOutlet weak var descLbl: UILabel!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var contactBtn: UIButton!
    @IBOutlet weak var emailBtn: UIButton!
    
    var supports: [Support] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.isHidden = true
        indicator.center = view.center
        
        getSupports()
        
        webView.isHidden = true
        
        // Layer borders
        contactBtn.layer.borderColor = UIColor.orange.cgColor
        contactBtn.layer.borderWidth = 0.5
        emailBtn.layer.borderColor = UIColor.orange.cgColor
        emailBtn.layer.borderWidth = 0.5
        
        // Tap Gesture Recognizers
        let aboutTapAction = UITapGestureRecognizer(target: self, action: #selector(self.aboutViewTapped(_:)))
        aboutView.isUserInteractionEnabled = true
        aboutView.addGestureRecognizer(aboutTapAction)
        
        let sssTapAction = UITapGestureRecognizer(target: self, action: #selector(self.sssViewTapped(_:)))
        sssView.isUserInteractionEnabled = true
        sssView.addGestureRecognizer(sssTapAction)
        
        let userAgreementTapAction = UITapGestureRecognizer(target: self, action: #selector(self.userAgreementViewTapped(_:)))
        userAgreementView.isUserInteractionEnabled = true
        userAgreementView.addGestureRecognizer(userAgreementTapAction)
        
        let privacyPolicyTapAction = UITapGestureRecognizer(target: self, action: #selector(self.privacyPolicyViewTapped(_:)))
        privacyPolicyView.isUserInteractionEnabled = true
        privacyPolicyView.addGestureRecognizer(privacyPolicyTapAction)
        
        // Do any additional setup after loading the view.
    }
    
    func getSupports() {
        addBlurLayer()
        view.addSubview(indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        Alamofire.request(APIURL.GetSupports.rawValue, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let result = response.result
            
            switch result {
            case .success:
                let httpCode = response.response?.statusCode
                switch httpCode! {
                case 200:
                    if let data = response.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                            for i in 0...3 {
                                var support = Support(name: "",url: "")
                                support.name = responseData[i]["Name"] as! String
                                support.url = responseData [i]["Url"] as! String
                                self.supports.append(support)
                            }
                        }
                    }
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 403:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "toMain")
                    break
                case 404, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                default: break
                }
                break
            case .failure:
                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                break
            }
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            self.removeBlurLayer()
        }
    }
    
    @objc func aboutViewTapped(_ sender: UITapGestureRecognizer) {
        loadWebView(withTitle: supports[0].name, withURL: supports[0].url)
    }
    
    @objc func sssViewTapped(_ sender: UITapGestureRecognizer) {
        loadWebView(withTitle: supports[1].name, withURL: supports[1].url)
    }
    
    @objc func userAgreementViewTapped(_ sender: UITapGestureRecognizer) {
        loadWebView(withTitle: supports[2].name, withURL: supports[2].url)
    }
    
    @objc func privacyPolicyViewTapped(_ sender: UITapGestureRecognizer) {
        loadWebView(withTitle: supports[3].name, withURL: supports[3].url)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        HHTabBarView.shared.isHidden = true
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func contactFormTapped(_ sender: UIButton) {
        if let contactView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "ContactViewController") as? ContactViewController {
            if let navigator = self.navigationController {
                navigator.pushViewController(contactView, animated: true)
            }
        }
    }
    
    @IBAction func sendEmail(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            composeVC.setToRecipients(["info@nobium.com.tr"])
            composeVC.setSubject("Qumpara Uygulaması Hk.")
            let dictionary = Bundle.main.infoDictionary!
            let version = dictionary["CFBundleShortVersionString"] as! String
            let body = "\n\n\n\nİletişim bilgileriniz, sizlere daha hızlı hizmet verebilmek adına mailinizin sonuna otomatik olarak eklenmiştir. Lütfen silmeyiniz. \niOS Version: \(UIDevice.current.systemVersion) \niPhone Model: \(UIDevice.current.modelName) \nApp Version: \(version)."
            composeVC.setMessageBody(body, isHTML: false)
            
            self.present(composeVC, animated: true, completion: nil)
        } else {
            self.showAlert(withTitle: "Hata", withMessage: "Telefonunuza bağlı bir mail hesabı bulunmamaktadır.", withAction: "")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
        
        switch result {
        case .sent:
            self.showAlert(withTitle: "Başarılı", withMessage: "E-mail gönderildi.", withAction: "")
            break
        case .cancelled, .failed:
            //self.showAlert(withTitle: "Başarısız", withMessage: "E-mail gönderilemedi.", withAction: "")
            print("cancelled")
            break
        case .saved:
            self.showAlert(withTitle: "Başarılı", withMessage: "E-mail kaydedildi.", withAction: "")
            break
        }
        
//        let alert = UIAlertController(title: "Başarılı", message: "E-mail gönderildi.", preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
//        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backScreen(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeWebView(_ sender: UIButton) {
        webView.isHidden = true
        let url = URL(string: "about:blank")
        let request = URLRequest(url: url!)
        wkwebView.load(request)
    }
    
    func loadWebView(withTitle title: String, withURL url: String) {
        let myURL = URL(string: url)
        let myRequest = URLRequest(url: myURL!)
        wkwebView.load(myRequest)
        supportTabName.text = title
        webView.isHidden = false
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad7,5", "iPad7,6":                      return "iPad 6"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}
