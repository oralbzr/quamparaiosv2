//
//  MoreViewController.swift
//  Qumpara
//
//  Created by Oral Bozkır on 10/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import Alamofire

class MoreViewController: UIViewController  {
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var promotionCodeView: UIView!
    @IBOutlet weak var inviteFriendView: UIView!
    @IBOutlet weak var supportView: UIView!
    @IBOutlet weak var logoutView: UIView!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var notSwitch: UISwitch!
    
    let defaults = UserDefaults.standard
    
    var memberImageUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Tap Gesture Recognizers
        
        let profileTapAction = UITapGestureRecognizer(target: self, action: #selector(self.profileViewTapped(_:)))
        profileView.isUserInteractionEnabled = true
        profileView.addGestureRecognizer(profileTapAction)
        
        let promotionTapAction = UITapGestureRecognizer(target: self, action: #selector(self.promotionViewTapped(_:)))
        promotionCodeView.isUserInteractionEnabled = true
        promotionCodeView.addGestureRecognizer(promotionTapAction)
        
        let inviteFriendTapAction = UITapGestureRecognizer(target: self, action: #selector(self.inviteFriendViewTapped(_:)))
        inviteFriendView.isUserInteractionEnabled = true
        inviteFriendView.addGestureRecognizer(inviteFriendTapAction)
        
        let supportTapAction = UITapGestureRecognizer(target: self, action: #selector(self.supportViewTapped(_:)))
        supportView.isUserInteractionEnabled = true
        supportView.addGestureRecognizer(supportTapAction)
        
        let logoutTapAction = UITapGestureRecognizer(target: self, action: #selector(self.logoutViewTapped(_:)))
        logoutView.isUserInteractionEnabled = true
        logoutView.addGestureRecognizer(logoutTapAction)
        // Do any additional setup after loading the view.
        
        userImage.layer.borderWidth = 0.5
        userImage.layer.masksToBounds = false
        userImage.layer.borderColor = UIColor.lightGray.cgColor
        userImage.layer.cornerRadius = userImage.frame.height/2
        userImage.clipsToBounds = true
        
        indicator.isHidden = true
        
        if Reachability.isInternetAvailable() {
            if defaults.object(forKey: "access_token") == nil {
                if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                    if let navigator = self.navigationController {
                        navigator.pushViewController(signUpView, animated: false)
                    }
                }
            } else {
                getUserInfo()
            }
        } else {
            self.showAlert(withTitle: "Uyarı", withMessage: "Lütfen internet bağlantınızı kontrol ediniz.", withAction: "")
        }
        
    }
    
    func getUserInfo() {
        view.addSubview(indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)",
            "Accept": "application/json",
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(APIURL.GetMember.rawValue, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            
            let result = response.result
            switch result {
            case .success:
                let httpCode = response.response?.statusCode
                switch httpCode! {
                case 200:
                    if let data = response.result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                            if let isNot = responseData["IsNotification"] as? String {
                                if isNot == "False" {
                                    self.notSwitch.setOn(false, animated: false)
                                } else {
                                    self.notSwitch.setOn(true, animated: false)
                                }
                            }
                            if let name = responseData["Name"] as? String {
                                if let surname = responseData["Surname"] as? String {
                                    self.userName.text = "\(name) \(surname)"
                                }
                            }
                            if let image = responseData["MemberImageUrl"] as? String {
                                self.memberImageUrl = image
                                let url = URL(string:image)
                                if let dataImage = try? Data(contentsOf: url!)
                                {
                                    self.userImage.image = UIImage(data: dataImage)!
                                } else {
                                    self.userImage.image = #imageLiteral(resourceName: "user")
                                }
                            } else {
                                self.userImage.image = #imageLiteral(resourceName: "user")
                            }
                        }
                    }
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 403, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "toMain")
                    break
                default: break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu an gerçekleştiremiyoruz Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                break
            }
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HHTabBarView.shared.isHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if Reachability.isInternetAvailable() {
            if defaults.object(forKey: "access_token") == nil {
                if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                    if let navigator = self.navigationController {
                        navigator.pushViewController(signUpView, animated: false)
                    }
                }
            } else {
                getUserInfo()
            }
        } else {
            self.showAlert(withTitle: "Uyarı", withMessage: "Lütfen internet bağlantınızı kontrol ediniz.", withAction: "")
        }
        // Hide the navigation bar on the this view controller
    }
    
    @objc func profileViewTapped(_ sender: UITapGestureRecognizer) {
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
            }
        } else {
            if let profileView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(profileView, animated: true)
                }
            }
        }
    }
    
    @objc func promotionViewTapped(_ sender: UITapGestureRecognizer) {
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
            }
        } else {
            if let promotionView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "PromotionViewController") as? PromotionViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(promotionView, animated: true)
                }
            }
        }
    }
    
    @objc func inviteFriendViewTapped(_ sender: UITapGestureRecognizer) {
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
            }
        } else {
            if let inviteFriendView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "InviteFriendViewController") as? InviteFriendViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(inviteFriendView, animated: true)
                }
            }
        }
    }
    
    @objc func supportViewTapped(_ sender: UITapGestureRecognizer) {
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
            }
        } else {
            if let supportView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SupportViewController") as? SupportViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(supportView, animated: true)
                }
            }
        }
    }
    
    @objc func logoutViewTapped(_ sender: UITapGestureRecognizer) {
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: true)
                }
            }
        } else {
            let alert = UIAlertController(title: "Uyarı", message: "Çıkış yapmak istediğinizden emin misiniz?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Hayır", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Evet", style: UIAlertActionStyle.default, handler: logout))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func logout(action: UIAlertAction) {
        switch defaults.integer(forKey: "loginType") {
        case 0:
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            UserDefaults.standard.synchronize()
            break
        case 1: //facebook
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            UserDefaults.standard.synchronize()
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            break
        case 2: //google
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            UserDefaults.standard.synchronize()
            GIDSignIn.sharedInstance().signOut()
            break
        default:
            let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            break
        }
        
        HHTabBarView.shared.selectTabAtIndex(withIndex: 0)
    }
    
    @IBAction func switchTapped(_ sender: UISwitch) {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        let parameters = [
            "NotificationChange": sender.isOn as AnyObject
        ]
        Alamofire.request(APIURL.ChangeNotification.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            //self.showAlert(withTitle: "Başarılı", withMessage: "Bildirim ayarlarınız değiştirildi.", withAction: "")
        }
    }
    
}
