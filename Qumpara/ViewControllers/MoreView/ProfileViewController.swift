//
//  ProfileViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/19/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

struct Job {
    var id: Int = 0
    var name: String = ""
}

struct Pet {
    var id: Int = 0
    var name: String = ""
}

struct EducationLevel {
    var id: Int = 0
    var name: String = ""
}

struct Income {
    var id: Int = 0
    var name: String = ""
}

class ProfileViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let defaults = UserDefaults.standard
    
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var scroller: UIScrollView!
    
    @IBOutlet var userImage: UIImageView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var licensePlate: UITextField!
    @IBOutlet weak var address: UITextField!
    
    @IBOutlet weak var birthdateLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var countyLbl: UILabel!
    @IBOutlet weak var jobLbl: UILabel!
    @IBOutlet weak var educationLevelLbl: UILabel!
    @IBOutlet weak var petLbl: UILabel!
    @IBOutlet weak var familySizeLbl: UILabel!
    @IBOutlet weak var incomeLbl: UILabel!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var countyView: UIView!
    @IBOutlet weak var educationView: UIView!
    @IBOutlet weak var petView: UIView!
    @IBOutlet weak var jobView: UIView!
    @IBOutlet weak var incomeView: UIView!
    @IBOutlet weak var familySizeView: UIView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var cityPicker: UIPickerView!
    @IBOutlet weak var countyPicker: UIPickerView!
    @IBOutlet weak var petPicker: UIPickerView!
    @IBOutlet weak var educationPicker: UIPickerView!
    @IBOutlet weak var jobPicker: UIPickerView!
    @IBOutlet weak var familySizePicker: UIPickerView!
    @IBOutlet weak var incomePicker: UIPickerView!
    
    @IBOutlet weak var genderMaleBtn: UIButton!
    @IBOutlet weak var genderFemaleBtn: UIButton!
    
    @IBOutlet weak var maritalSingle: UIButton!
    @IBOutlet weak var maritalMarried: UIButton!
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var blurEffectView = UIVisualEffectView()
    
    var cityData: [City] = []
    var countyData: [County] = []
    var jobData: [Job] = []
    var educationData: [EducationLevel] = []
    var petData: [Pet] = []
    var incomeData: [Income] = []
    var familySizeData: [Int] = [1, 2, 3, 4, 5]
    
    var cityId = 0
    var countyId = 0
    var gender = 0
    var genderCount = 0
    var married = 0
    var mStatusCount = 0  // marital status count
    var jobId = 0
    var educationLevelId = 0
    var petId = 0
    var incomeId = 0
    var familySize = 0
    var memberImageUrl = ""
    
    var tempDict = Dictionary<String, AnyObject>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Toolbars
        addToolBar(textField: name)
        addToolBar(textField: surname)
        addToolBar(textField: email)
        addToolBar(textField: licensePlate)
        addToolBar(textField: address)
        
        dateView.isHidden = true
        cityView.isHidden = true
        countyView.isHidden = true
        educationView.isHidden = true
        petView.isHidden = true
        jobView.isHidden = true
        incomeView.isHidden = true
        familySizeView.isHidden = true
        
        indicator.isHidden = true
        indicator.center = view.center
        
        // Label borders
        addBorder(label: birthdateLbl)
        addBorder(label: phoneLbl)
        addBorder(label: cityLbl)
        addBorder(label: countyLbl)
        addBorder(label: jobLbl)
        addBorder(label: educationLevelLbl)
        addBorder(label: petLbl)
        addBorder(label: familySizeLbl)
        addBorder(label: incomeLbl)
        
        var contentRect = CGRect.zero
        
        for view in scroller.subviews {
            contentRect = contentRect.union(view.frame)
        }
        scroller.contentSize = contentRect.size
        
        // Label Tap Actions
        let birthdateTapAction = UITapGestureRecognizer(target: self, action: #selector(self.birthdateActionTapped(_:)))
        birthdateLbl.isUserInteractionEnabled = true
        birthdateLbl.addGestureRecognizer(birthdateTapAction)
        
        let cityTapAction = UITapGestureRecognizer(target: self, action: #selector(self.cityActionTapped(_:)))
        cityLbl.isUserInteractionEnabled = true
        cityLbl.addGestureRecognizer(cityTapAction)
        
        let countyTapAction = UITapGestureRecognizer(target: self, action: #selector(self.countyActionTapped(_:)))
        countyLbl.isUserInteractionEnabled = true
        countyLbl.addGestureRecognizer(countyTapAction)
        
        let petTapAction = UITapGestureRecognizer(target: self, action: #selector(self.petActionTapped(_:)))
        petLbl.isUserInteractionEnabled = true
        petLbl.addGestureRecognizer(petTapAction)
        
        let educationTapAction = UITapGestureRecognizer(target: self, action: #selector(self.educationActionTapped(_:)))
        educationLevelLbl.isUserInteractionEnabled = true
        educationLevelLbl.addGestureRecognizer(educationTapAction)
        
        let jobTapAction = UITapGestureRecognizer(target: self, action: #selector(self.jobActionTapped(_:)))
        jobLbl.isUserInteractionEnabled = true
        jobLbl.addGestureRecognizer(jobTapAction)
        
        let familySizeTapAction = UITapGestureRecognizer(target: self, action: #selector(self.familySizeActionTapped(_:)))
        familySizeLbl.isUserInteractionEnabled = true
        familySizeLbl.addGestureRecognizer(familySizeTapAction)
        
        let incomeTapAction = UITapGestureRecognizer(target: self, action: #selector(self.incomeActionTapped(_:)))
        incomeLbl.isUserInteractionEnabled = true
        incomeLbl.addGestureRecognizer(incomeTapAction)
        
        userImage.layer.borderWidth = 0.5
        userImage.layer.masksToBounds = false
        userImage.layer.borderColor = UIColor.lightGray.cgColor
        userImage.layer.cornerRadius = userImage.frame.height/2
        userImage.clipsToBounds = true
        
        getUserInfo()
        getCities()
        getJobs()
        getPets()
        getEducationLevels()
        getIncomes()
        
        cityPicker.dataSource = self
        cityPicker.delegate = self
        
        countyPicker.dataSource = self
        countyPicker.delegate = self
        
        petPicker.dataSource = self
        petPicker.delegate = self
        
        educationPicker.dataSource = self
        educationPicker.delegate = self
        
        jobPicker.dataSource = self
        jobPicker.delegate = self
        
        incomePicker.dataSource = self
        incomePicker.delegate = self
        
        familySizePicker.dataSource = self
        familySizePicker.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        HHTabBarView.shared.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }
    }
    
    func getCities() {
        Alamofire.request(APIURL.GetCities.rawValue).responseJSON { response in
            let result = response.result
            
            switch result {
            case .success:
                let httpCode = response.response?.statusCode
                switch httpCode! {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                            for i in 0...responseData.count - 1 {
                                var city = City()
                                city.id = responseData[i]["CityId"] as! Int
                                city.name = responseData[i]["CityName"] as! String
                                self.cityData.append(city)
                            }
                            self.cityPicker.reloadAllComponents()
                        }
                    }
                    break
                default:
                    self.showAlert(withTitle: "Hata Kodu : \(httpCode!)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                    break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                break
            }
        }
    }
    
    func getCounties(_ cityId: Int) {
        let heads: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let params: [String: AnyObject] = [
            "CityId" : cityId as AnyObject
        ]
        
        Alamofire.request(APIURL.GetCounties.rawValue, method: .post, parameters: params, encoding: JSONEncoding.default, headers: heads).responseJSON { response in
            let result = response.result
            
            self.countyData = []
            switch result {
            case .success:
                let httpCode = response.response?.statusCode
                switch httpCode! {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                            for i in 0...responseData.count - 1 {
                                var county = County()
                                county.id = responseData[i]["CountyId"] as! Int
                                county.name = responseData[i]["CountyName"] as! String
                                self.countyData.append(county)
                            }
                            self.countyPicker.reloadAllComponents()
                        }
                    }
                    break
                default:
                    self.showAlert(withTitle: "Hata Kodu : \(httpCode!)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                    break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                break
            }
        }
    }
    func getJobs() {
        Alamofire.request(APIURL.GetJobs.rawValue).responseJSON { response in
            let result = response.result
            
            switch result {
            case .success:
                let httpCode = response.response!.statusCode
                switch httpCode {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                            for i in 0...responseData.count - 1 {
                                var job = Job()
                                job.id = responseData[i]["JobId"] as! Int
                                job.name = responseData[i]["JobName"] as! String
                                self.jobData.append(job)
                            }
                            self.jobPicker.reloadAllComponents()
                        }
                    }
                    break
                default:
                    self.showAlert(withTitle: "Hata Kodu : \(httpCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                    break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                break
            }
        }
    }
    
    func getPets() {
        Alamofire.request(APIURL.GetPets.rawValue).responseJSON { response in
            let result = response.result
            
            switch result {
            case .success:
                let httpCode = response.response!.statusCode
                switch httpCode {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                            for i in 0...responseData.count - 1 {
                                var pet = Pet()
                                pet.id = responseData[i]["PetId"] as! Int
                                pet.name = responseData[i]["PetName"] as! String
                                self.petData.append(pet)
                            }
                            self.petPicker.reloadAllComponents()
                        }
                    }
                    break
                default:
                    self.showAlert(withTitle: "Hata Kodu : \(httpCode)", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                    break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu anda gerçekleştiremiyoruz. Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                break
            }
        }
    }
    
    func getEducationLevels() {
        Alamofire.request(APIURL.GetEducations.rawValue).responseJSON { response in
            let result = response.result
            
            switch result {
            case .success:
                if let data = result.value as? Dictionary<String, AnyObject> {
                    if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                        for i in 0...responseData.count - 1 {
                            var ed = EducationLevel()
                            ed.id = responseData[i]["EducationId"] as! Int
                            ed.name = responseData[i]["EducationName"] as! String
                            self.educationData.append(ed)
                        }
                        self.educationPicker.reloadAllComponents()
                    }
                }
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    func getIncomes() {
        Alamofire.request(APIURL.GetIncomes.rawValue).responseJSON { response in
            let result = response.result
            
            switch result {
            case .success:
                if let data = result.value as? Dictionary<String, AnyObject> {
                    if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                        for i in 0...responseData.count - 1 {
                            var income = Income()
                            income.id = responseData[i]["IncomeId"] as! Int
                            income.name = responseData[i]["IncomeName"] as! String
                            self.incomeData.append(income)
                        }
                        self.incomePicker.reloadAllComponents()
                    }
                }
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    func getUserInfo() {
        addBlurLayer()
        view.addSubview(indicator)
        indicator.isHidden = false
        indicator.startAnimating()
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)",
            "Accept": "application/json",
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(APIURL.GetMember.rawValue, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            
            let result = response.result
            switch result {
            case .success:
                let httpCode = response.response?.statusCode
                switch httpCode! {
                case 200:
                    if let data = response.result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                            self.tempDict = responseData
                            if let name = responseData["Name"] as? String {
                                self.name.text = name
                            }
                            if let surname = responseData["Surname"] as? String {
                                self.surname.text = surname
                            }
                            if let birthdate = responseData["BirthDate"] as? String {
                                self.birthdateLbl.text = birthdate.standardToDate
                            }
                            if let city = responseData["City"] as? String {
                                self.cityLbl.text = city
                                if let cityid = responseData["CityId"] as? Int {
                                    self.cityId = cityid
                                    self.getCounties(cityid)
                                }
                            }
                            if let county = responseData["County"] as? String {
                                self.countyLbl.text = county
                                if let countyid = responseData["CountyId"] as? Int {
                                    self.countyId = countyid
                                }
                            }
                            if let email = responseData["Email"] as? String {
                                self.email.text = email
                            }
                            if let phone = responseData["PhoneNumber"] as? String {
                                self.phoneLbl.text = phone
                            }
                            if let image = responseData["MemberImageUrl"] as? String {
                                self.memberImageUrl = image
                                let url = URL(string:image)
                                if let dataImage = try? Data(contentsOf: url!)
                                {
                                    self.userImage.image = UIImage(data: dataImage)!
                                } else {
                                    self.userImage.image = #imageLiteral(resourceName: "user")
                                }
                            } else {
                                self.userImage.image = #imageLiteral(resourceName: "user")
                            }
                            if let gender = responseData["Gender"] as? String {
                                switch gender {
                                case "ERKEK":
                                    self.genderMaleBtn.isSelected = true
                                    self.genderCount = 1
                                    self.gender = 1
                                    break
                                case "KADIN":
                                    self.genderFemaleBtn.isSelected = true
                                    self.genderCount = 1
                                    self.gender = 2
                                    break
                                default: break
                                }
                            }
                            if let maritalStatus = responseData["Married"] as? Int {
                                switch maritalStatus {
                                case 1:
                                    self.maritalMarried.isSelected = true
                                    self.mStatusCount = 1
                                    self.married = 1
                                    break
                                case 0:
                                    self.maritalSingle.isSelected = true
                                    self.mStatusCount = 1
                                    self.married = 0
                                    break
                                default: break
                                }
                            }
                            if let job = responseData["Job"] as? String {
                                self.jobLbl.text = job
                                if let jobid = responseData["JobId"] as? Int {
                                    self.jobId = jobid
                                }
                            }
                            if let pet = responseData["Pet"] as? String {
                                self.petLbl.text = pet
                                if let petid = responseData["PetId"] as? Int {
                                    self.petId = petid
                                }
                            }
                            if let size = responseData["FamilySize"] as? Int {
                                self.familySizeLbl.text = "\(size)"
                                self.familySize = size
                            }
                            if let income = responseData["Income"] as? String {
                                self.incomeLbl.text = income
                                if let inid = responseData["IncomeId"] as? Int {
                                    self.incomeId = inid
                                }
                            }
                            if let education = responseData["Graduation"] as? String {
                                self.educationLevelLbl.text = education
                                if let edid = responseData["GraduationId"] as? Int {
                                    self.educationLevelId = edid
                                }
                            }
                            if let carplate = responseData["CarPlate"] as? String {
                                self.licensePlate.text = carplate
                            }
                            if let address = responseData["Address"] as? String {
                                self.address.text = address
                            }
                        }
                    }
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 403, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                default: break
                }
                break
            case .failure:
                self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu an gerçekleştiremiyoruz Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                break
            }
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            self.removeBlurLayer()
        }
    }
    
    @IBAction func genderMaleTapped(_ sender: UIButton) {
        if genderCount == 0 {
            sender.isSelected = true
            genderCount = 1
            gender = 1
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                genderCount = 0
                gender = 0
            } else {
                sender.isSelected = true
                genderFemaleBtn.isSelected = false
                genderCount = 1
                gender = 1
            }
        }
    }
    
    @IBAction func genderFemaleTapped(_ sender: UIButton) {
        if genderCount == 0 {
            sender.isSelected = true
            genderCount = 1
            gender = 2
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                genderCount = 0
                gender = 0
            } else {
                sender.isSelected = true
                genderMaleBtn.isSelected = false
                genderCount = 1
                gender = 2
            }
        }
    }
    
    @IBAction func marriedTapped(_ sender: UIButton) {
        if mStatusCount == 0 {
            sender.isSelected = true
            mStatusCount = 1
            married = 1
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                mStatusCount = 0
                married = 0
            } else {
                sender.isSelected = true
                maritalSingle.isSelected = false
                mStatusCount = 1
                married = 1
            }
        }
    }
    
    @IBAction func singleTapped(_ sender: UIButton) {
        if mStatusCount == 0 {
            sender.isSelected = true
            mStatusCount = 1
            married = 0
        } else {
            if sender.isSelected == true {
                sender.isSelected = false
                mStatusCount = 0
                married = 0
            } else {
                sender.isSelected = true
                maritalMarried.isSelected = false
                mStatusCount = 1
                married = 0
            }
        }
    }
    
    @objc func birthdateActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        dateView.isHidden = false
        dateView.center = view.center
        view.addSubview(dateView)
    }
    
    @IBAction func chooseDate(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        birthdateLbl.text = dateFormatter.string(from: datePicker.date)
        dateView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelDate(_ sender: UIButton) {
        dateView.isHidden = true
        removeBlurLayer()
    }
    
    @objc func cityActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        self.cityView.isHidden = false
        cityView.center = view.center
        self.view.addSubview(self.cityView)
    }
    
    @IBAction func chooseCity(_ sender: UIButton) {
        countyLbl.text = ""
        cityView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelCity(_ sender: UIButton) {
        if let city = tempDict["City"] as? String {
            self.cityLbl.text = city
            if let cityid = tempDict["CityId"] as? Int {
                self.cityId = cityid
                self.getCounties(cityid)
            }
        }
        cityView.isHidden = true
        removeBlurLayer()
    }
    
    @objc func countyActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        self.countyView.isHidden = false
        countyView.center = view.center
        self.view.addSubview(self.countyView)
    }
    
    @IBAction func chooseCounty(_ sender: UIButton) {
        countyView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelCounty(_ sender: UIButton) {
        if let county = tempDict["County"] as? String {
            self.countyLbl.text = county
            if let countyid = tempDict["CountyId"] as? Int {
                self.countyId = countyid
            }
        }
        countyView.isHidden = true
        removeBlurLayer()
    }
    
    @objc func petActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        self.petView.isHidden = false
        petView.center = view.center
        self.view.addSubview(self.petView)
    }
    
    @IBAction func choosePet(_ sender: UIButton) {
        petView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelPet(_ sender: UIButton) {
        if let pet = tempDict["Pet"] as? String {
            self.petLbl.text = pet
            if let petid = tempDict["PetId"] as? Int {
                self.petId = petid
            }
        }
        petView.isHidden = true
        removeBlurLayer()
    }
    
    @objc func educationActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        self.educationView.isHidden = false
        educationView.center = view.center
        self.view.addSubview(self.educationView)
    }
    
    @IBAction func chooseEducation(_ sender: UIButton) {
        educationView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelEducation(_ sender: UIButton) {
        if let education = tempDict["Graduation"] as? String {
            self.educationLevelLbl.text = education
            if let edid = tempDict["GraduationId"] as? Int {
                self.educationLevelId = edid
            }
        }
        educationView.isHidden = true
        removeBlurLayer()
    }
    
    @objc func jobActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        self.jobView.isHidden = false
        jobView.center = view.center
        self.view.addSubview(self.jobView)
    }
    
    @IBAction func chooseJob(_ sender: UIButton) {
        jobView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelJob(_ sender: UIButton) {
        if let job = tempDict["Job"] as? String {
            self.jobLbl.text = job
            if let jobid = tempDict["JobId"] as? Int {
                self.jobId = jobid
            }
        }
        jobView.isHidden = true
        removeBlurLayer()
    }
    
    @objc func familySizeActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        self.familySizeView.isHidden = false
        familySizeView.center = view.center
        self.view.addSubview(self.familySizeView)
    }
    
    @IBAction func chooseFamilySize(_ sender: UIButton) {
        familySizeView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelFamilySize(_ sender: UIButton) {
        if let size = tempDict["FamilySize"] as? Int {
            self.familySizeLbl.text = "\(size)"
            self.familySize = size
        }
        familySizeView.isHidden = true
        removeBlurLayer()
    }
    
    @objc func incomeActionTapped(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        self.incomeView.isHidden = false
        incomeView.center = view.center
        self.view.addSubview(self.incomeView)
    }
    
    @IBAction func chooseIncome(_ sender: UIButton) {
        incomeView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelIncome(_ sender: UIButton) {
        if let income = tempDict["Income"] as? String {
            self.incomeLbl.text = income
            if let inid = tempDict["IncomeId"] as? Int {
                self.incomeId = inid
            }
        }
        incomeView.isHidden = true
        removeBlurLayer()
    }
    
    func control() -> Bool {
        var flag = true
        if licensePlate.text!.isValidPlate == false && licensePlate.text! != "" {
            self.showAlert(withTitle: "Uyarı", withMessage: "Plakanız doğru formatta değildir. Lütfen güncelleyiniz.(Örn: 01AB123)", withAction: "")
            flag = false
        }
        if licensePlate.text != "" && address.text == "" {
            self.showAlert(withTitle: "Uyarı", withMessage: "Lütfen adres bilginizi giriniz.", withAction: "")
            flag = false
        }
        return flag
    }
    
    @IBAction func saveProfile(_ sender: UIButton) {
        if control() {
            addBlurLayer()
            view.addSubview(indicator)
            indicator.isHidden = false
            indicator.startAnimating()
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
            ]
            let parameters: [String:AnyObject] = [
                "Name": name.text! as AnyObject,
                "Surname": surname.text! as AnyObject,
                "Email": email.text! as AnyObject,
                "GenderId": gender as AnyObject,
                "BirthDate": birthdateLbl.text?.dateToStandard as AnyObject,
                "CityId": cityId as AnyObject,
                "CountyId": countyId as AnyObject,
                "FamilySize": familySize as AnyObject,
                "GraduationId": educationLevelId as AnyObject,
                "PetId": petId as AnyObject,
                "IncomeId": incomeId as AnyObject,
                "Child": 0 as AnyObject, // TODO - unnecessary
                "Married": married as AnyObject,
                "MemberImageUrl": memberImageUrl as AnyObject,
                "CarPlate": "\(licensePlate.text!)" as AnyObject,
                "Address": "\(address.text!)" as AnyObject,
                "JobId": jobId as AnyObject
            ]
            print(parameters)
            
            Alamofire.request(APIURL.UpdateMember.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                debugPrint(response)
                let result = response.result
                
                switch result {
                case .success:
                    let httpCode = response.response!.statusCode
                    switch httpCode {
                    case 200:
                        //self.showAlert(withTitle: "Başarılı", withMessage: "Bilgileriniz güncellendi.", withAction: "pop")
                        break
                    case 401:
                        self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                        break
                    default:
                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                        break
                    }
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                    self.removeBlurLayer()
                    break
                case .failure:
                    self.showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu an gerçekleştiremiyoruz Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
                    break
                }
            }
        } else {
            showAlert(withTitle: "Uyarı", withMessage: "İşleminizi şu an gerçekleştiremiyoruz Lütfen daha sonra tekrar deneyiniz.", withAction: "pop")
        }
    }
    
    @IBAction func cancelProfile(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updatePicture(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func sendNewPicture(_ image: UIImage) {
        if Reachability.isInternetAvailable() {
            addBlurLayer()
            view.addSubview(indicator)
            indicator.isHidden = false
            indicator.startAnimating()
            
            let imageData = UIImageJPEGRepresentation(image, 0.5)
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            
            Alamofire.upload(multipartFormData: {(multipartFormData) in
                
                multipartFormData.append(imageData!, withName: "file", fileName: "imageFile.jpg", mimeType: "image/jpg")
                
                
            }, to: APIURL.ProfilImage.rawValue,headers: headers, encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        guard let resultValue = response.result.value else {
                            let alert = UIAlertController(title: "Uyarı", message: "Bilinmeyen bir hata oluştu tekrarı halinde lütfen iletişime geçiniz" , preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            return
                        }
                        
                        let result = response.result
                        
                        switch result {
                        case .success:
                            print("success")
                            self.dismiss(animated: false, completion: nil)
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            self.removeBlurLayer()
                            break
                        case .failure:
                            let alert = UIAlertController(title: "Uyarı", message: "Fişiniz okunurken bir hata oluştu, lüften daha sonra tekrar deneyiniz" , preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            break
                        }
                        
                    }
                case .failure:
                    let alert = UIAlertController(title: "Uyarı", message: "Bilinmeyen bir hata oluştu tekrarı halinde lütfen iletişime geçiniz" , preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
            })
            
        } else {
            let alert = UIAlertController(title: "Uyarı", message: "Lütfen internet bağlantınızı kontrol ediniz.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.userImage.image = image
        picker.dismiss(animated: true, completion: nil)
        sendNewPicture(image!)
    }
    
    func numberOfComponents(in cityPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == cityPicker{
            return cityData.count
        } else if pickerView == countyPicker {
            return countyData.count
        } else if pickerView == educationPicker {
            return educationData.count
        } else if pickerView == petPicker {
            return petData.count
        } else if pickerView == incomePicker {
            return incomeData.count
        } else if pickerView == familySizePicker {
            return familySizeData.count
        } else if pickerView == jobPicker {
            return jobData.count
        } else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == cityPicker {
            return cityData[row].name
        } else if pickerView == countyPicker {
            return countyData[row].name
        } else if pickerView == educationPicker {
            return educationData[row].name
        } else if pickerView == petPicker {
            return petData[row].name
        } else if pickerView == incomePicker {
            return incomeData[row].name
        } else if pickerView == familySizePicker {
            return String(familySizeData[row])
        } else if pickerView == jobPicker {
            return jobData[row].name
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == cityPicker {
            if cityLbl.text != "" && cityLbl.text != "Seçiniz" {
                getCounties(cityData[row].id)
            }
            cityId = cityData[row].id
            self.cityLbl.text = cityData[row].name
        } else if pickerView == countyPicker {
            if cityLbl.text != "" && cityLbl.text != "Seçiniz" {
                countyId = countyData[row].id
                self.countyLbl.text = countyData[row].name
            }
        } else if pickerView == petPicker {
            petId = petData[row].id
            self.petLbl.text = petData[row].name
        } else if pickerView == educationPicker {
            educationLevelId = educationData[row].id
            self.educationLevelLbl.text = educationData[row].name
        } else if pickerView == familySizePicker {
            familySize = familySizeData[row]
            self.familySizeLbl.text = String(familySizeData[row])
        } else if pickerView == incomePicker {
            incomeId = incomeData[row].id
            self.incomeLbl.text = incomeData[row].name
        } else if pickerView == jobPicker {
            jobId = jobData[row].id
            self.jobLbl.text = jobData[row].name
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UIViewController {
    func addBorder(label: UILabel) {
        label.layer.borderColor = UIColor.lightGray.cgColor
        label.layer.borderWidth = 0.5
        label.layer.cornerRadius = 5
    }
    
    func stringToImage(_ str: String) -> UIImage {
        let data = Data(base64Encoded: str)
        let image = UIImage(data: data!)!
        return image
    }
}

extension UIImage {
    func imageToBase64(image: UIImage) -> String {
        let imageData: Data = UIImageJPEGRepresentation(image, 0.5)!
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return strBase64
    }
}
