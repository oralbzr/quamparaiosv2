//
//  InviteFriendViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/14/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

class InviteFriendViewController: UIViewController {
    
    @IBOutlet weak var couponTxt: UITextField!
    
    let defaults = UserDefaults.standard
    
    var toShareText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCoupon()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        HHTabBarView.shared.isHidden = true
        
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getCoupon() {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        Alamofire.request(APIURL.InvitationCampaign.rawValue, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            debugPrint(response)
            let httpCode = response.response?.statusCode
            let result = response.result
            
            switch result {
            case .success:
                if let data = result.value as? Dictionary<String, AnyObject> {
                    
                    switch httpCode! {
                    case 200:
                        print("success")
                        if let responseData = data["response"] as? Dictionary<String, AnyObject> {
                            self.toShareText = responseData["ShareText"] as! String
                            let code = responseData["MemberInvitationCode"] as! String
                            DispatchQueue.main.async {
                                self.couponTxt.text = code
                            }
                        }
                        break
                    case 401:
                        self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                        break
                    case 403, 404, 500:
                        self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "")
                        break
                    default: break
                    }
                    
                }
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyiniz.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    @IBAction func copyToClipboard(_ sender: UIButton) {
        UIPasteboard.general.string = couponTxt.text
        let alert = UIAlertController(title: "Bilgi", message: "Kod kopyalandı.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func shareCoupon(_ sender: UIButton) {
        let textToShare = "\(self.couponTxt.text ?? "")" + "\(self.toShareText )"
        
            let objectsToShare = [textToShare] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        
    }
    
    @IBAction func participationTapped(_ sender: UIButton) {
        if let participationView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "ParticipationViewController") as? ParticipationViewController {
            if let navigator = self.navigationController {
                navigator.pushViewController(participationView, animated: true)
            }
        }
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
