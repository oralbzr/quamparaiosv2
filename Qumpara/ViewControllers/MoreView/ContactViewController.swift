//
//  ContactViewController.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/15/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit
import Alamofire

struct Topic {
    var id: Int = 0
    var type: String = ""
}

class ContactViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var topicLbl: UILabel!
    @IBOutlet weak var messageTxt: UITextView!
    @IBOutlet weak var topicView: UIView!
    @IBOutlet weak var topicPicker: UIPickerView!
    
    @IBOutlet weak var sendMessageBtn: UIButton!
    
    var topicData: [Topic] = []
    
    var selectedSubjectID: Int = 0
    
    var tempID: Int = 0
    var tempTopic: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topicView.isHidden = true

        sendMessageBtn.setDisable()
        
        addToolBar(textField: messageTxt)
        messageTxt.keyboardType = UIKeyboardType.alphabet
        
        // Borders
        topicLbl.layer.borderColor = UIColor.lightGray.cgColor
        topicLbl.layer.borderWidth = 0.5
        messageTxt.layer.borderColor = UIColor.lightGray.cgColor
        messageTxt.layer.borderWidth = 0.5
        
        // Gesture recognizer for topic
        let topicTapAction = UITapGestureRecognizer(target: self, action: #selector(self.openTopics(_:)))
        topicLbl.isUserInteractionEnabled = true
        topicLbl.addGestureRecognizer(topicTapAction)
        
        topicPicker.dataSource = self
        topicPicker.delegate = self
        
        messageTxt.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if defaults.object(forKey: "access_token") == nil {
            if let signUpView = UIStoryboard(name: "Views", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(signUpView, animated: false)
                }
            }
        } else {
            getTopics()
        }
    }
    
    func getTopics() {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        Alamofire.request(APIURL.GetSubjectType.rawValue, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let result = response.result
            let httpCode = response.response?.statusCode
            debugPrint(response)
            
            switch result {
            case .success:
                switch httpCode! {
                case 200:
                    if let data = result.value as? Dictionary<String, AnyObject> {
                        if let responseData = data["response"] as? [Dictionary<String, AnyObject>] {
                            for i in 0...responseData.count - 1 {
                                var topic = Topic()
                                topic.id = responseData[i]["SubjectId"] as! Int
                                topic.type = responseData[i]["SubjectType"] as! String
                                self.topicData.append(topic)
                            }
                            self.topicPicker.reloadAllComponents()
                            self.topicLbl.text = self.topicData[0].type
                            self.selectedSubjectID = 1
                        }
                    }
                    break
                case 401:
                    self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                    break
                case 403, 404, 500:
                    self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                    break
                default: break
                }
                
                break
            case .failure:
                let alert = UIAlertController(title: "Uyarı", message: "Şu an isteğinizi gerçekleştiremiyoruz, lütfen daha sonra tekrar deneyin", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    @objc func openTopics(_ sender: UITapGestureRecognizer) {
        addBlurLayer()
        topicView.isHidden = false
        view.addSubview(topicView)
        tempID = selectedSubjectID
        tempTopic = topicData[selectedSubjectID - 1].type
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(self.defaults.object(forKey: "access_token")!)"
        ]
        let parameters = [
            "subjectId": selectedSubjectID as AnyObject,
            "subject": self.messageTxt.text as AnyObject
        ]
        Alamofire.request(APIURL.SendContactForm.rawValue, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let httpCode = response.response?.statusCode
            let result = response.result
            switch httpCode! {
            case 200:
                let alert = UIAlertController(title: "Başarılı", message: "İletişim formunuz gönderildi", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                break
            case 401:
                self.unauthorized(withError: (result.value as? Dictionary<String, AnyObject>)!)
                break
            case 403, 500:
                self.showError(withError: (result.value as? Dictionary<String, AnyObject>)!, withAction: "pop")
                break
            default: break
            }
        }
        //navigationController?.popViewController(animated: true)
        selectedSubjectID = 1
        topicLbl.text = topicData[0].type
        messageTxt.text = ""
    }
    
    @IBAction func chooseTopic(_ sender: UIButton) {
        topicView.isHidden = true
        removeBlurLayer()
    }
    
    @IBAction func cancelTopic(_ sender: UIButton) {
        topicLbl.text = tempTopic
        selectedSubjectID = tempID
        topicView.isHidden = true
        removeBlurLayer()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if !textView.text.isEmpty {
            sendMessageBtn.setEnable()
        } else {
            sendMessageBtn.setDisable()
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return topicData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return topicData[row].type
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedSubjectID = topicData[row].id
        self.topicLbl.text = topicData[row].type
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
