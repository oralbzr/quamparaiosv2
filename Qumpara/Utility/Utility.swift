//
//  Utility.swift
//  Qumpara
//
//  Created by Oral Bozkır on 08/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit


let defaultWidth = 320.0
let defaultHeight = 568.0
private var bounds = CGRect.zero
private var widthCoefficient: Float = 0.0
private var heightCoefficient: Float = 0.0
let textWarning = "Uyarı"
let textAlert = "Bilgi"

class Utility: NSObject {

  
    class func initializeStaticVariables() {
        bounds = UIScreen.main.bounds
        if UIDevice.current.userInterfaceIdiom == .phone {
            if bounds.size.width  > CGFloat(defaultWidth) {
                widthCoefficient = Float(bounds.size.width / CGFloat(defaultWidth))
                heightCoefficient = Float(bounds.size.height / CGFloat(defaultHeight))
            }
            else {
                widthCoefficient = 1.0
                heightCoefficient = 1.0
            }
        }
        else{
            widthCoefficient = Float(bounds.size.width / CGFloat(defaultWidth))
            heightCoefficient = Float(bounds.size.height / ((bounds.size.height * CGFloat(defaultWidth)) / bounds.size.width))
        }
        
    }
  
    class func rect(_ rect: CGRect) -> CGRect {
        var scaledRect: CGRect = rect
        if bounds.size.width > CGFloat(defaultWidth) && UIDevice.current.userInterfaceIdiom == .phone {
            scaledRect.origin.x = CGFloat(self.scaledX(Float(rect.origin.x)))
            scaledRect.origin.y = CGFloat(self.scaledY(Float(rect.origin.y)))
            scaledRect.size.width = CGFloat(self.scaledWidth(Float(rect.size.width)))
            scaledRect.size.height = CGFloat(self.scaledHeight(Float(rect.size.height)))
        }
        return scaledRect
    }

   
    

    class func size(_ size: CGSize) -> CGSize {
        var scaledSize: CGSize = size
        if bounds.size.width > CGFloat(defaultWidth) && UIDevice.current.userInterfaceIdiom == .phone {
            scaledSize.width = CGFloat(self.scaledWidth(Float(size.width)))
            scaledSize.height = CGFloat(self.scaledHeight(Float(size.height)))
        }
        return scaledSize
    }
    

    class func scaledX(_ x: Float) -> Float {
        return x * widthCoefficient
    }
    
    class func scaledY(_ y: Float) -> Float {
        return y * heightCoefficient
    }
    
    class func scaledWidth(_ w: Float) -> Float {
        return w * widthCoefficient
    }
    
    class func scaledHeight(_ h: Float) -> Float {
        return h * heightCoefficient
    }
    
    class func setAnchorPoint(_ anchorPoint: CGPoint, for view: UIView) {
        var newPoint = CGPoint(x: view.bounds.size.width * anchorPoint.x, y: view.bounds.size.height * anchorPoint.y)
        var oldPoint = CGPoint(x: view.bounds.size.width * view.layer.anchorPoint.x, y: view.bounds.size.height * view.layer.anchorPoint.y)
        newPoint = newPoint.applying(view.transform)
        oldPoint = oldPoint.applying(view.transform)
        var position: CGPoint = view.layer.position
        position.x -= oldPoint.x
        position.x += newPoint.x
        position.y -= oldPoint.y
        position.y += newPoint.y
        view.layer.position = position
        view.layer.anchorPoint = anchorPoint
    }
    
    class func isValidEmail(_ checkString: String) -> Bool {
        let stricterFilter = true
        let stricterFilterString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let laxString = ".+@.+\\.[A-Za-z]{2}[A-Za-z]*"
        let emailRegex: String = stricterFilter ? stricterFilterString : laxString
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: checkString)
    }

    class func validateEmail(with email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: email)
    }
    
    class func convertFormat(_ unformattedDate: String) -> String {
       
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "tr_TR") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        var dateString = ""
        if let date = dateFormatter.date(from: unformattedDate) {
            dateFormatter.dateFormat = "dd.MM.yyyy"
            dateFormatter.locale = tempLocale // reset the locale
            dateString = dateFormatter.string(from: date)
        
        return dateString
    }
        return dateString
    }
    
    func VodafoneRgBold(x: Float) -> UIFont {
        return UIFont(name: "Vodafone Rg", size: CGFloat(x))!
    }
    func VodafoneRg(x: Float) -> UIFont {
        return UIFont(name: "Vodafone Lt", size: CGFloat(x))!
    }
    func image(name: String) -> UIImage {
        return UIImage(named: name)!
    }
   
    
    
}
