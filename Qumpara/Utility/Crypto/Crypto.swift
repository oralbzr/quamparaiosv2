//
//  Crypto.swift
//  fisiCek
//
//  Created by Selay on 28.07.2016.
//  Copyright © 2016 Nobium. All rights reserved.
//


import Foundation
import CryptoSwift

//TODO init methodunun ayrıntılı olmasına ve variablelara gerek yok silinecek ve test edilecek.
//Cryption ana methodarı implementasyonu
class Crypto {
    
    var key: String
    var iv: String
    var algorithm: String
    var salt: String
    
    init(key: String, iv: String, algorithm: String, salt: String) {
        self.key = key
        self.iv = iv
        self.algorithm = algorithm
        self.salt = salt
    }
    
    
    func encrypt(key: String, iv: String, text: String) -> String?  {
        if let aes = try? AES(key: key, iv: iv),
            let encrypted = try? aes.encrypt(Array(text.utf8)) {
            return encrypted.toHexString()
        }
        return nil
    }
    
    func aesEncrypt(key: String, iv: String, password: String) throws -> String{
        // let data = password.data(using: String.Encoding.utf8)
        let aes = try AES(key: key, iv: iv)
        let ciphertext = try aes.encrypt(Array<UInt8>(hex: password))
        let result = (String)!(ciphertext.toBase64()!)
        return result!
    }
    

    
    func aesDecrypt(key: String, iv: String,encodedString: String) throws -> String {
         let data =  Data(base64Encoded: encodedString, options: Data.Base64DecodingOptions(rawValue: 0))
      
        let decrypted = try! AES(key: key.bytes, blockMode: .CBC(iv: iv.bytes), padding: .pkcs7).decrypt([UInt8](data!))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "banana"
    }
    
    /*
    func aesEncrypt(key: String, iv: String ,password: String) throws -> String {
        
         let data =  Data(password.utf8)
        
        let data = self.data(using: .utf8)!
        let encrypted = try! AES(key: key.bytes, blockMode: .CBC(iv: iv.bytes), padding: .pkcs7).encrypt([UInt8](data))
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
    }
    */
   
    
   
   
    
}


