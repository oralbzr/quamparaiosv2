//
//  Defines.swift
//  Qumpara
//
//  Created by Oral Bozkır on 08/12/2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import Foundation
import UIKit

enum APIURL:String {
    
    //Campaign
    case GetCampaignList = "https://www.fisicek.com/Application/api/GetCampaigns"
    case GetCampaignDetail = "https://www.fisicek.com/Application/api/GetCampaignDetail"
    
    //CitiesAndCounties
    case GetCities = "https://www.fisicek.com/Application/api/GetCities"
    case GetCounties = "https://www.fisicek.com/Application/api/GetCounties"
    
    //MemberLogin
    case MemberLogin = "https://www.fisicek.com/Application/api/MemberLogin"
    case MemberLogin2 = "https://www.fisicek.com/Application/api/MemberLogin2"
    
    //MemberNew
    case MemberNew = "https://www.fisicek.com/Application/api/MemberNew"
    case MemberNew2 = "https://www.fisicek.com/Application/api/MemberNew2"
    case PhoneVerify = "https://www.fisicek.com/Application/api/PhoneVerify"
    case OTPVerify = "https://www.fisicek.com/Application/api/OTPVerify"
    case OTPVerify2 = "https://www.fisicek.com/Application/api/OTPVerify2"
    case MemberNewSave = "https://www.fisicek.com/Application/api/MemberNewSave" // + id
    case InvitationCodeEntry = "https://www.fisicek.com/Application/api/InvitationCodeEntry"
    
    //Notifications
    case GetNotifications = "https://www.fisicek.com/Application/api/GetNotifications"
    case GetNotificationDetail = "https://www.fisicek.com/Application/api/GetNotificationDetail"
    
    //Survey
    case GetSurveyQuestion = "https://www.fisicek.com/Application/api/GetSurveyQuestion"
    case SurveyAnswer = "https://www.fisicek.com/Application/api/SurveyAnswer"
    case SurveyDenied = "https://www.fisicek.com/Application/api/SurveyDenied"
    
    //MoreInformation
    case InvitationCampaign = "https://www.fisicek.com/Application/api/InvitationCampaign"
    case ChangeNotification = "https://www.fisicek.com/Application/api/ChangeNotification"
    case GetMember = "https://www.fisicek.com/Application/api/GetMember"
    case ProfilImage = "https://www.fisicek.com/Application/api/ProfilImage"
    case GetEducations = "https://www.fisicek.com/Application/api/GetEducations"
    case GetJobs = "https://www.fisicek.com/Application/api/GetJobs"
    case GetPets = "https://www.fisicek.com/Application/api/GetPets"
    case GetIncomes = "https://www.fisicek.com/Application/api/GetIncome"
    case UpdateMember = "https://www.fisicek.com/Application/api/UpdateMember"
    
    //Qumparam
    case Qumparam = "https://www.fisicek.com/Application/api/GetQumparam"
    case BrandCoupon = "https://www.fisicek.com/Application/api/GetBrandCouponQumparam"
    case GetCouponInformation = "https://www.fisicek.com/Application/api/GetCouponInformation"
    case UpdateUsedCoupon = "https://www.fisicek.com/Application/api/UpdateUsedCoupon" // + id

    case GetPrizeListInformation = "https://www.fisicek.com/Application/api/GetPrizesForConvert"
    case ConvertToPrize = "https://www.fisicek.com/Application/api/ConvertToPrize"

    case QumparamCardVerificationSMS = "https://www.fisicek.com/Application/api/QumparamCardVerificationSms"
    case QumparamGetCard = "https://www.fisicek.com/Application/api/GetQumparamCard"
    case QumparamCardChargingMoney = "https://www.fisicek.com/Application/api/QumparamCardChargingMoney"
    case CreateUserByQumparamCard = "https://www.fisicek.com/Application/api/CreateUserByQumparamCard"
    case QumparamCardMemberSave = "https://www.fisicek.com/Application/api/QumparamCardMemberSave"
    
    //Authentication
    case getToken = "https://www.fisicek.com/Application/getToken"
    case MemberVersionUpdate = "https://www.fisicek.com/Application/api/MemberVersionUpdate"
    case UpdateRegistration = "https://www.fisicek.com/Application/api/UpdateRegistration"
    
    //Support
    case GetSupports = "https://www.fisicek.com/Application/api/GetSupports"
    case GetSubjectType = "https://www.fisicek.com/Application/api/GetSubjectType"
    case SendContactForm = "https://www.fisicek.com/Application/api/SendContactForm"
    
    //delete when change requests
    case PhoneMemberLogin = "https://www.fisicek.com/web/api/PhoneMemberLogin2"
    case MainPage = "https://www.fisicek.com/web/api/MainPage"
    //Upload Image
    case UploadImage = "https://www.fisicek.com/Application/api/UploadImage"
    //Promotion
    case GetPromotionList = "https://www.fisicek.com/Application/api/GetPromotionCampaigns"
    case ApplyPromotionCode = "https://www.fisicek.com/Application/api/ApplyPromotionCode"
}

func QFontRg(x: CGFloat) -> UIFont {
    return UIFont(name: "PTSans-Narrow", size: x)!
}

func QFontBd(x: CGFloat) -> UIFont {
    return UIFont(name: "PTSans-Bold", size: x)!
}





