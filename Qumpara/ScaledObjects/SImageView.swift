//
//  SImageView.swift
//  Qumpara
//
//  Created by apple on 24.12.2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit

class SImageView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: Utility.rect(frame))
        
        // Initialization code
        
    }
    
     required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
      //  fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        frame = Utility.rect(frame)
        
    }
    

}
