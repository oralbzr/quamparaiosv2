//
//  SBoldLabel.swift
//  Qumpara
//
//  Created by apple on 24.12.2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit

class SBoldLabel: UILabel {
        
        override init(frame: CGRect) {
            super.init(frame: Utility.rect(frame))
            font = QFontBd(x: font.pointSize)
            // Initialization code
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }
        
        override func awakeFromNib() {
            super.awakeFromNib()
            frame = Utility.rect(frame)
            /*** Custom Fonts ***/
            font = QFontBd(x: font.pointSize)
            
        }
        
        
        
        
    }
   


