//
//  SCollectionView.swift
//  Qumpara
//
//  Created by Oral Bozkır on 21/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class SCollectionView: UICollectionView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //  fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        frame = Utility.rect(frame)
        
    }

}
