//
//  SSearchBar.swift
//  Qumpara
//
//  Created by Mahmut Acar on 2/5/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class SSearchBar: UISearchBar {
    override init(frame: CGRect) {
        super.init(frame: Utility.rect(frame))
        
        // Initialization code
        
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        
        let newSize  = Utility .size(CGSize(width: 280, height: 30))
        return newSize;
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //  fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        frame = Utility.rect(frame)
    }
}
