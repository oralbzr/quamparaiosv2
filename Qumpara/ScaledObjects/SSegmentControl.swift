//
//  SSegmentControl.swift
//  Qumpara
//
//  Created by Oral Bozkır on 23/02/2018.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class SSegmentControl: UISegmentedControl {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        frame = Utility.rect(frame)
        
    }

}
