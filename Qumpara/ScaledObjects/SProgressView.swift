//
//  SProgressView.swift
//  Qumpara
//
//  Created by Mahmut Acar on 1/30/18.
//  Copyright © 2018 digitus. All rights reserved.
//

import UIKit

class SProgressView: UIProgressView {
    
    override init(frame: CGRect) {
        super.init(frame: Utility.rect(frame))
        
        // Initialization code
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //  fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        frame = Utility.rect(frame)
        
    }
    
}
