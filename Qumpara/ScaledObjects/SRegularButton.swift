//
//  SRegularButton.swift
//  Qumpara
//
//  Created by apple on 24.12.2017.
//  Copyright © 2017 digitus. All rights reserved.
//

import UIKit

class SRegularButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: Utility.rect(frame))
        titleLabel?.font = QFontRg(x: (titleLabel?.font.pointSize)!)
        // Initialization code
        
    }
    
     required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
      //  fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        frame = Utility.rect(frame)
        titleLabel?.font = QFontRg(x: (titleLabel?.font.pointSize)!)
        /*** Custom Fonts ***/
       
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
